<?php 
include('security.php');
include('dbconfig.php');
// $connection = mysqli_connect("localhost","root","","gogoshop");
// $connection = mysqli_connect("localhost","gogoempi_gotravelweb","OfKTL6PbD6IP","gogoempi_gotravelweb");

//: Add New hotel
if(isset($_POST['addhotelbtn']))
{
    $hotelname = $_POST['hotel_name'];
    $hotelemail = $_POST['hotel_email'];
    $hoteladd = $_POST['hotel_add'];
    $hotelpassword = $_POST['hotel_password'];
    $hotelcpassword = $_POST['hotel_confirmpassword'];
    $hoteltype = 'food';
    

    $email_query = "SELECT * FROM food_company WHERE company_email='$hotelemail'";
    $email_query_run = mysqli_query($connection, $email_query);
    if(mysqli_num_rows($email_query_run) > 0)
    {
        $_SESSION['status'] = "Email Already Taken. Please Try Another one.";
        $_SESSION['status_code'] = "error";
        header('Location: hotel.php');  
    }
    else
    {
        if($hotelpassword === $hotelcpassword)
        {
            $query = "INSERT INTO food_company (Company_name,company_address,company_openinghours,company_email,company_pass,company_type,company_banner_img) VALUES ('$hotelname','$hoteladd','','$hotelemail','$hotelpassword','$hoteltype','')";
            $query_run = mysqli_query($connection, $query);


            if($query_run)
            {
                // echo "Saved";
                $_SESSION['success'] = "New Hotel Added";
                // $_SESSION['status_code'] = "success";
                header('Location: hotel.php');
            }
            else 
            {   
               
                $_SESSION['status'] = "Hotel Not Added";
                // $_SESSION['status_code'] = "error";
                header('Location: hotel.php');  
            }
        }
        else 
        {
            $_SESSION['status'] = "Password and Confirm Password Does Not Match";
            // $_SESSION['status_code'] = "warning";
            header('Location: hotel.php');  
        }
    }

}


 

//:: Update the hotel data:

 if(isset($_POST['updatehotelbtn']))
{
    $hotelupdateid = $_POST['hotel_updateid'];
    $hotelupdatename = $_POST['hotel_updatename'];
    $hotelupdateemail = $_POST['hotel_updateemail'];
    $hotelupdateadd = $_POST['hotel_updateadd'];
    $hotelupdatepassword = $_POST['hotel_updatepassword'];

    $query = "UPDATE food_company SET Company_name='$hotelupdatename', company_email='$hotelupdateemail', company_address='$hotelupdateadd', company_pass='$hotelupdatepassword' WHERE fcompany_id='$hotelupdateid' ";
    $query_run = mysqli_query($connection, $query);

    if($query_run)
    {
        $_SESSION['success'] = "Your Data is Updated";
        header('Location: hotel.php'); 
    }
    else
    {
        $_SESSION['status'] = "Your Data is NOT Updated";
        header('Location: hotel.php'); 
    }
}


//:: Delete the hotel Data..

if(isset($_POST['deletehotel_btn']))
{
    $hoteldeleteid = $_POST['deletehotel_id'];

    $query = "DELETE FROM food_company WHERE fcompany_id='$hoteldeleteid' ";
    $query_run = mysqli_query($connection, $query);

    if($query_run)
    {
        $_SESSION['success'] = "Your Data is Deleted";
        // $_SESSION['status_code'] = "success";
        header('Location: hotel.php'); 
    }
    else
    {
        $_SESSION['status'] = "Your Data is NOT DELETED";       
        // $_SESSION['status_code'] = "error";
        header('Location: hotel.php'); 
    }    
}




//:: Admin login

if(isset($_POST['adminlogin_btn'])){

    $adminloginemail = $_POST['adminlogin_email'];
    $adminloginpassword = $_POST['adminlogin_password'];

    // $query = "SELECT * FROM admin where admin_email='$adminloginemail' AND admin_pass='$adminloginpassword' ";
    // $query_run = mysqli_query($connection, $query);

    // if(mysqli_fetch_array($query_run)){

    //     $_SESSION['username'] = $adminloginemail;
    //     header('Location: index.php');
    // }else{
    //     $_SESSION['status'] = 'Email id / Password is invalid';
    //     // $_SESSION['status'] = $adminloginemail;
    //     header('Location: login.php');
    // }

    $query = "SELECT * FROM food_company where company_email='$adminloginemail' AND company_pass='$adminloginpassword' ";
    $query_run = mysqli_query($connection, $query);
    $storetype = mysqli_fetch_array($query_run);


    if($storetype['company_type'] == "food"){

        $_SESSION['foodusername'] = $adminloginemail;
        header('Location: foodstore_index.php');

    }else if($storetype['company_type'] == "product"){

        $_SESSION['productusername'] = $adminloginemail;
        header('Location: productstore_index.php');

    }else if($storetype['company_type'] == "admin"){
        
        $_SESSION['username'] = $adminloginemail;
        header('Location: index.php');

    }else{
        $_SESSION['status'] = 'Email id / Password is invalid';
        // $_SESSION['status'] = $adminloginemail;
        header('Location: login.php');
    }

}


// Add new product item

if(isset($_POST['addfooditem'])){
   
    $item_title = $_POST['item_title'];
    $item_cat = $_POST['item_cat'];
    $item_company = $_POST['item_company'];
    $item_price = $_POST['item_price'];
    $item_desc = $_POST['item_desc'];
    $status = "on";
    $item_youtubelink = $_POST['item_youtubelink'];
    $item_discounted_percentage = $_POST['item_discounted_percentage'];
    $item_availability = $_POST['item_availability'];
    $item_type = 'food';
    // //: Image Name
    $item_img = $_FILES['item_img']['name'];

    
    
    if(file_exists("fooditem_images/" .$_FILES["item_img"]["name"])){

        $image_already_exist = $_FILES["item_img"]["name"];
        $_SESSION['status'] = "Image Already exists. $store";
        header('Location: food_item.php'); 

    }else{

        $insert_product ="insert into food_items (fcat_id,fcompany_id,date,item_title,item_img,item_price,item_desc,youtube_link,status,item_discounted_price,item_avalability,item_type,top_product,trending) values 
         ('$item_cat','$item_company',NOW(),'$item_title','$item_img',
         '$item_price','$item_desc','$item_youtubelink','$status','$item_discounted_percentage','$item_availability', '$item_type','','')";

            if(mysqli_query($connection, $insert_product)){
            move_uploaded_file($_FILES["item_img"]["tmp_name"], "fooditem_images/".$_FILES["item_img"]["name"] );
            $_SESSION['success'] = "Item is Added";
            header('Location: food_item.php'); 
        }else{ 

            $_SESSION['status'] = "Item is not Added";       
            header('Location: food_item.php'); 
        }

    }
    
  
}







//: Add Food Category..

if(isset($_POST['addfoodcategorybtn'])){

    $fcat_title = $_POST['foodcategory_name'];
    $fcat_type = 'food';

    $insert_foodcategory = "insert into food_category (food_cat,cat_image,cat_type) values ('$fcat_title','','$fcat_type')";

    if(mysqli_query($connection, $insert_foodcategory)){
        $_SESSION['success'] = "Food Category is Added";
        header('Location: food_category.php'); 
    }else{ 

        $_SESSION['status'] = "Food Category is not Added";       
        header('Location: food_category.php'); 
        // echo "Error insertin data: " . mysqli_error($connection);
    }
}

//:: Update Food Category
if(isset($_POST['updatefoodcatbtn'])){

    $fcatupdateid = $_POST['foodcat_updateid'];
    $fcatupdatename = $_POST['foodcat_updatename'];

    $query = "UPDATE food_category SET food_cat='$fcatupdatename' WHERE fcat_id='$fcatupdateid' ";
    $query_run = mysqli_query($connection, $query);

    if($query_run)
    {
        $_SESSION['success'] = "Your food Category is Updated";
        // $_SESSION['status_code'] = "success";
        header('Location: food_category.php'); 
    }
    else
    {
        $_SESSION['status'] = "Your Data is NOT Updated";
        // $_SESSION['status_code'] = "error";
        header('Location: food_category.php'); 
    }
}

//:: Delete the Food Category Data..

if(isset($_POST['deletefoodcat_btn']))
{
    $fcatdeleteid = $_POST['deletefoodcat_id'];

    $query = "DELETE FROM food_category WHERE fcat_id='$fcatdeleteid' ";
    $query_run = mysqli_query($connection, $query);

    if($query_run)
    {
        $_SESSION['success'] = "Food Category is Deleted";
        // $_SESSION['status_code'] = "success";
        header('Location: food_category.php'); 
    }
    else
    {
        $_SESSION['status'] = "Food Category Data is NOT DELETED";       
        // $_SESSION['status_code'] = "error";
        header('Location: food_category.php'); 
    }    
}


//: Add Product Category..

if(isset($_POST['addcategorybtn'])){

    $cat_title = $_POST['category_title'];  
    $category_img = $_FILES['category_img']['name'];

    if(file_exists("assets/" .$_FILES["category_img"]["name"])){

        $image_already_exist = $_FILES["category_img"]["name"];
        $_SESSION['status'] = "Image Already exists. $store";
        header('Location: product_category.php'); 

    }else{

        $insert_category = "insert into categories (cat_title,cat_image,cat_type) values ('$cat_title', '$category_img', 'food')";

        if(mysqli_query($connection, $insert_category)){
            move_uploaded_file($_FILES["category_img"]["tmp_name"], "assets/".$_FILES["category_img"]["name"] );
            $_SESSION['success'] = "Food Category is Added";
            header('Location: product_category.php'); 
        }else{ 

            $_SESSION['status'] = "Food Category is not Added";       
            header('Location: product_category.php'); 
        }

    }
}


// :: Update Product Category
if(isset($_POST['productcategory_update_btn'])){
    
    $catupdateid = $_POST['category_updateid'];
    $catupdatename = $_POST['updatecategory_title'];
    $category_img = $_FILES['updatecategory_img']['name'];

    //:: UPDATING THE IMAGE::
    $update_img = "SELECT * FROM categories where cat_id= '$catupdateid'";
    $run_update_img = mysqli_query($connection, $update_img);

    foreach($run_update_img as $fa_row){
        if($category_img == NULL){
            $image_data = $fa_row['cat_img'];
        }else{
            //: upload with new image and delete with old image
            if($img_path = "assets/".$fa_row['cat_img']){
                unlink($img_path);
                $image_data = $category_img;
            }
            
        }
    }

    $query = "UPDATE categories SET cat_title='$catupdatename', cat_img='$image_data' WHERE cat_id='$catupdateid' ";
    $query_run = mysqli_query($connection, $query);

    if($query_run)
    {
        if($category_img == NULL){
            $_SESSION['success'] = "Food Category is Updated with existing image";
            header('Location: product_category.php'); 
        }else{
            //: upload with new image and delete with old image
            move_uploaded_file($_FILES["updatecategory_img"]["tmp_name"], "assets/".$_FILES["updatecategory_img"]["name"] );
            $_SESSION['success'] = "Category is Updated";
            header('Location: product_category.php'); 
        }
      
    }
    else
    {
        $_SESSION['status'] = "Category is NOT Updated";
        // $_SESSION['status_code'] = "error";
        header('Location: product_category.php'); 
    }
}

//: Delete Product Category:
if(isset($_POST['deletecat_btn']))
{
    $catdeleteid = $_POST['deletecat_id'];

    $query = "DELETE FROM categories WHERE cat_id='$catdeleteid' ";
    $query_run = mysqli_query($connection, $query);

    if($query_run)
    {
        $_SESSION['success'] = "Category is Deleted";
        // $_SESSION['status_code'] = "success";
        header('Location: product_category.php'); 
    }
    else
    {
        $_SESSION['status'] = "Category Data is NOT DELETED";       
        // $_SESSION['status_code'] = "error";
        header('Location: product_category.php'); 
    }    
}



//: Add New store
if(isset($_POST['addstore_btn']))
{
    $storename = $_POST['store_name'];
    $storeemail = $_POST['store_email'];
    $storeadd = $_POST['store_add'];
    $storepassword = $_POST['store_password'];
    $storecpassword = $_POST['store_confirmpassword'];
    $storetype = 'product';
    

    $email_query = "SELECT * FROM food_company WHERE company_email='$storeemail'";
    $email_query_run = mysqli_query($connection, $email_query);
    if(mysqli_num_rows($email_query_run) > 0)
    {
        $_SESSION['status'] = "Email Already Taken. Please Try Another one.";
        $_SESSION['status_code'] = "error";
        header('Location: store.php');  
    }
    else
    {
        if($storepassword === $storecpassword)
        {
            $query = "INSERT INTO food_company (Company_name,company_address,company_openinghours,company_email,company_pass,company_type,company_banner_img) VALUES ('$storename','$storeadd','','$storeemail','$storepassword','$storetype','')";
            $query_run = mysqli_query($connection, $query);


            if($query_run)
            {
                // echo "Saved";
                $_SESSION['success'] = "New store Added";
                // $_SESSION['status_code'] = "success";
                header('Location: store.php');
            }
            else 
            {   
               
                $_SESSION['status'] = "Store Not Added";
                // $_SESSION['status_code'] = "error";
                header('Location: store.php');  
            }
        }
        else 
        {
            $_SESSION['status'] = "Password and Confirm Password Does Not Match";
            // $_SESSION['status_code'] = "warning";
            header('Location: store.php');  
        }
    }

 }


 //:: Update the store data:

 if(isset($_POST['updatestorebtn']))
{
    $storeupdateid = $_POST['store_updateid'];
    $storeupdatename = $_POST['store_updatename'];
    $storeupdateemail = $_POST['store_updateemail'];
    $storeupdateadd = $_POST['store_updateadd'];
    $storeupdatepassword = $_POST['store_updatepassword'];

    $query = "UPDATE food_company SET Company_name='$storeupdatename', company_email='$storeupdateemail', company_address='$storeupdateadd', company_pass='$storeupdatepassword' WHERE fcompany_id='$storeupdateid' ";
    $query_run = mysqli_query($connection, $query);

    if($query_run)
    {
        $_SESSION['success'] = "Your Data is Updated";
        // $_SESSION['status_code'] = "success";
        header('Location: store.php'); 
    }
    else
    {
        $_SESSION['status'] = "Your Data is NOT Updated";
        // $_SESSION['status_code'] = "error";
        header('Location: store.php'); 
    }
}

// /:: Delete the store Data..
if(isset($_POST['deletestore_btn']))
{
    $storedeleteid = $_POST['deletestore_id'];

    $query = "DELETE FROM food_company WHERE fcompany_id='$storedeleteid' ";
    $query_run = mysqli_query($connection, $query);

    if($query_run)
    {
        $_SESSION['success'] = "Your Data is Deleted";
        // $_SESSION['status_code'] = "success";
        header('Location: store.php'); 
    }
    else
    {
        $_SESSION['status'] = "Your Data is NOT DELETED";       
        // $_SESSION['status_code'] = "error";
        header('Location: store.php'); 
    }    
}


// Add new product item

if(isset($_POST['addproductitem'])){
   
    $item_title = $_POST['product_title'];
    $item_cat = $_POST['product_cat'];
    $item_company = $_POST['product_company'];
    $item_price = $_POST['product_price'];
    $item_desc = $_POST['product_desc'];
    $status = "on";
    $item_youtubelink = $_POST['product_youtubelink'];
    $item_discounted_percentage = $_POST['product_discounted_percentage'];
    $item_availability = $_POST['product_availability'];
    $item_type = 'product';
    // //: Image Name
    $item_img = $_FILES['product_img']['name'];
    
    
    if(file_exists("fooditem_images/" .$_FILES["product_img"]["name"])){

        $image_already_exist = $_FILES["product_img"]["name"];
        $_SESSION['status'] = "Image Already exists. $store";
        header('Location: products.php'); 

    }else{

        $insert_product ="insert into food_items (fcat_id,fcompany_id,date,item_title,item_img,item_price,item_desc,youtube_link,status,item_discounted_price,item_avalability,item_type,top_product,trending) values 
         ('$item_cat','$item_company',NOW(),'$item_title','$item_img',
         '$item_price','$item_desc','$item_youtubelink','$status','$item_discounted_percentage','$item_availability', '$item_type','','')";

            if(mysqli_query($connection, $insert_product)){
            move_uploaded_file($_FILES["product_img"]["tmp_name"], "fooditem_images/".$_FILES["product_img"]["name"] );
            $_SESSION['success'] = "Item is Added";
            header('Location: products.php'); 
        }else{ 

            $_SESSION['status'] = "Item is not Added";       
            header('Location: products.php');
            //  echo "Error insertin data: " . mysqli_error($connection); 
        }

    }
    
  
}


//: Add product Category..

if(isset($_POST['addproductcategorybtn'])){

    $fcat_title = $_POST['productcategory_name'];
    $item_img = $_FILES['productcat_img']['name'];
    $fcat_type = 'product';

    if(file_exists("homepage_images/" .$_FILES["productcat_img"]["name"])){

        $image_already_exist = $_FILES["productcat_img"]["name"];
        $_SESSION['status'] = "Image Already exists. $store";
        header('Location: product_category.php'); 

    }else{

        $insert_foodcategory = "insert into food_category (food_cat,cat_image,cat_type) values ('$fcat_title','$item_img','$fcat_type')";


            if(mysqli_query($connection, $insert_foodcategory)){
            move_uploaded_file($_FILES["productcat_img"]["tmp_name"], "homepage_images/".$_FILES["productcat_img"]["name"] );
            $_SESSION['success'] = "Item is Added";
            header('Location: product_category.php'); 
        }else{ 

            $_SESSION['status'] = "Item is not Added";       
            header('Location: product_category.php'); 
            // echo "Error insertin data: " . mysqli_error($connection);

        }

    }

}

//:: Update Product Category
if(isset($_POST['updateproductcatbtn'])){

    $fcatupdateid = $_POST['productcat_updateid'];
    $fcatupdatename = $_POST['productcat_updatename'];
    $updateitem_img = $_FILES['productcat_updateimg']['name'];


     //:: UPDATING THE IMAGE::
     $update_img = "SELECT * FROM food_category where fcat_id= '$fcatupdateid'";
     $run_update_img = mysqli_query($connection, $update_img);
 
     foreach($run_update_img as $fa_row){
         if($updateitem_img == NULL){
             $image_data = $fa_row['cat_image'];
         }else{
             //: upload with new image and delete with old image
             if($img_path = "homepage_images/".$fa_row['cat_image']){
                 unlink($img_path);
                 $image_data = $updateitem_img;
             }
             
         }
     }

     $query = "UPDATE food_category SET food_cat='$fcatupdatename',cat_image='$image_data' WHERE fcat_id='$fcatupdateid' ";
     $query_run = mysqli_query($connection, $query);
 
     if($query_run)
     {
         if($updateitem_img == NULL){
             $_SESSION['success'] = "Item is Updated with existing image";
             header('Location: product_category.php'); 
         }else{
             //: upload with new image and delete with old image
             move_uploaded_file($_FILES["productcat_updateimg"]["tmp_name"], "homepage_images/".$_FILES["productcat_updateimg"]["name"] );
             $_SESSION['success'] = "Item is Updated";
             header('Location: product_category.php'); 
         }
       
     }
     else
     {
         $_SESSION['status'] = "item is NOT Updated";
         // $_SESSION['status_code'] = "error";
         header('Location: product_category.php'); 
     }

}


//:: Delete the Product Category Data..

if(isset($_POST['deleteproductcat_btn']))
{
    $fcatdeleteid = $_POST['deleteproductcat_id'];

    $query = "DELETE FROM food_category WHERE fcat_id='$fcatdeleteid' ";
    $query_run = mysqli_query($connection, $query);

    if($query_run)
    {
        $_SESSION['success'] = "Product Category is Deleted";
        // $_SESSION['status_code'] = "success";
        header('Location: product_category.php'); 
    }
    else
    {
        $_SESSION['status'] = "product Category Data is NOT DELETED";       
        // $_SESSION['status_code'] = "error";
        header('Location: product_category.php'); 
    }    
}


//: Update Product item

if(isset($_POST['updateproductitem'])){

    $updateitem_id = $_POST['product_updateid'];
    $updateitem_title = $_POST['product_updatetitle'];
    $updateitem_cat = $_POST['product_updatecat'];
    $updateitem_price = $_POST['product_updateprice'];
    $updateitem_desc = $_POST['product_updatedesc'];
    $status = "on";
    $updateitem_youtubelink = $_POST['product_updateyoutubelink'];
    // $item_discounted_percentage = $_POST['item_discounted_percentage'];
    $updateitem_availability = $_POST['product_updateavailability'];
    $item_company = $_POST['product_updatecompany'];
    $item_discounted_percentage = 1;
    $updateitem_img = $_FILES['product_updateimg']['name'];
    

    //:: UPDATING THE IMAGE::
    $update_img = "SELECT * FROM food_items where item_id= '$updateitem_id'";
    $run_update_img = mysqli_query($connection, $update_img);

    foreach($run_update_img as $fa_row){
        if($updateitem_img == NULL){
            $image_data = $fa_row['item_img'];
        }else{
            //: upload with new image and delete with old image
            if($img_path = "fooditem_images/".$fa_row['item_img']){
                unlink($img_path);
                $image_data = $updateitem_img;
            }
            
        }
    }

    $query = "UPDATE food_items SET fcat_id='$updateitem_cat', fcompany_id='$item_company', item_title='$updateitem_title', item_img='$image_data', item_price='$updateitem_price', 
    item_desc='$updateitem_desc', youtube_link='$updateitem_youtubelink', status='$status', item_discounted_price='$item_discounted_percentage', item_avalability='$updateitem_availability'   
    WHERE item_id='$updateitem_id' ";
    $query_run = mysqli_query($connection, $query);

    if($query_run)
    {
        if($updateitem_img == NULL){
            $_SESSION['success'] = "Item is Updated with existing image";
            header('Location: products.php'); 
        }else{
            //: upload with new image and delete with old image
            move_uploaded_file($_FILES["product_updateimg"]["tmp_name"], "fooditem_images/".$_FILES["product_updateimg"]["name"] );
            $_SESSION['success'] = "Item is Updated";
            header('Location: products.php'); 
        }
      
    }
    else
    {
        $_SESSION['status'] = "item is NOT Updated";
        // $_SESSION['status_code'] = "error";
        header('Location: products.php'); 
    }

}


//: Delete Product item
if(isset($_POST['deleteproduct_btn']))
{
    $prodictitemdeleteid = $_POST['deleteproduct_id'];

    $query = "DELETE FROM food_items WHERE item_id='$prodictitemdeleteid' ";
    $query_run = mysqli_query($connection, $query);

    if($query_run)
    {
        $_SESSION['success'] = "item is Deleted";
        // $_SESSION['status_code'] = "success";
        header('Location: products.php'); 
    }
    else
    {
        $_SESSION['status'] = "Item is NOT DELETED";       
        // $_SESSION['status_code'] = "error";
        header('Location: products.php'); 
    }    

    
}

//:: GOGO EMPIRE MALL ADD PRODUCT
if(isset($_POST['addgogoproductitem'])){
   
    $item_title = $_POST['product_title'];
    $item_cat = $_POST['product_cat'];
    $item_company = $_POST['product_company'];
    $item_price = $_POST['product_price'];
    $item_desc = $_POST['product_desc'];
    $status = "on";
    $item_youtubelink = $_POST['product_youtubelink'];
    $item_discounted_percentage = $_POST['product_discounted_percentage'];
    $item_availability = $_POST['product_availability'];
    $item_type = 'gogo';
    // //: Image Name
    $item_img = $_FILES['product_img']['name'];
    
    
    if(file_exists("fooditem_images/" .$_FILES["product_img"]["name"])){

        $image_already_exist = $_FILES["product_img"]["name"];
        $_SESSION['status'] = "Image Already exists. $store";
        header('Location: userwebsite_homepage_gogomall.php'); 

    }else{

        $insert_product ="insert into food_items (fcat_id,fcompany_id,date,item_title,item_img,item_price,item_desc,youtube_link,status,item_discounted_price,item_avalability,item_type,top_product,trending) values 
         ('$item_cat','$item_company',NOW(),'$item_title','$item_img',
         '$item_price','$item_desc','$item_youtubelink','$status','$item_discounted_percentage','$item_availability', '$item_type','','')";

            if(mysqli_query($connection, $insert_product)){
            move_uploaded_file($_FILES["product_img"]["tmp_name"], "fooditem_images/".$_FILES["product_img"]["name"] );
            $_SESSION['success'] = "Item is Added";
            header('Location: userwebsite_homepage_gogomall.php'); 
        }else{ 

            $_SESSION['status'] = "Item is not Added";       
            header('Location: userwebsite_homepage_gogomall.php'); 
        }

    }
    
  
}

//: Update GOGO MALL Product item

if(isset($_POST['updategogoproductitem'])){

    $updateitem_id = $_POST['product_updateid'];
    $updateitem_title = $_POST['product_updatetitle'];
    $updateitem_cat = $_POST['product_updatecat'];
    $updateitem_price = $_POST['product_updateprice'];
    $updateitem_desc = $_POST['product_updatedesc'];
    $status = "on";
    $updateitem_youtubelink = $_POST['product_updateyoutubelink'];
    // $item_discounted_percentage = $_POST['item_discounted_percentage'];
    $updateitem_availability = $_POST['product_updateavailability'];
    $item_company = $_POST['product_updatecompany'];
    $item_discounted_percentage = 1;
    $updateitem_img = $_FILES['product_updateimg']['name'];
    

    //:: UPDATING THE IMAGE::
    $update_img = "SELECT * FROM food_items where item_id= '$updateitem_id'";
    $run_update_img = mysqli_query($connection, $update_img);

    foreach($run_update_img as $fa_row){
        if($updateitem_img == NULL){
            $image_data = $fa_row['item_img'];
        }else{
            //: upload with new image and delete with old image
            if($img_path = "fooditem_images/".$fa_row['item_img']){
                unlink($img_path);
                $image_data = $updateitem_img;
            }
            
        }
    }

    $query = "UPDATE food_items SET fcat_id='$updateitem_cat', fcompany_id='$item_company', item_title='$updateitem_title', item_img='$image_data', item_price='$updateitem_price', 
    item_desc='$updateitem_desc', youtube_link='$updateitem_youtubelink', status='$status', item_discounted_price='$item_discounted_percentage', item_avalability='$updateitem_availability'   
    WHERE item_id='$updateitem_id' ";
    $query_run = mysqli_query($connection, $query);

    if($query_run)
    {
        if($updateitem_img == NULL){
            $_SESSION['success'] = "Item is Updated with existing image";
            header('Location: userwebsite_homepage_gogomall.php'); 
        }else{
            //: upload with new image and delete with old image
            move_uploaded_file($_FILES["product_updateimg"]["tmp_name"], "fooditem_images/".$_FILES["product_updateimg"]["name"] );
            $_SESSION['success'] = "Item is Updated";
            header('Location: userwebsite_homepage_gogomall.php'); 
        }
      
    }
    else
    {
        $_SESSION['status'] = "item is NOT Updated";
        // $_SESSION['status_code'] = "error";
        header('Location: userwebsite_homepage_gogomall.php'); 
    }

}

//: Delete Gogo Empire mall item
if(isset($_POST['deletegogoproduct_btn']))
{
    $productitemdeleteid = $_POST['deleteproduct_id'];

    $query = "DELETE FROM food_items WHERE item_id='$productitemdeleteid' ";
    $query_run = mysqli_query($connection, $query);

    if($query_run)
    {
        $_SESSION['success'] = "item is Deleted";
        // $_SESSION['status_code'] = "success";
        header('Location:  userwebsite_homepage_gogomall.php'); 
    }
    else
    {
        $_SESSION['status'] = "Item is NOT DELETED";       
        // $_SESSION['status_code'] = "error";
        header('Location:  userwebsite_homepage_gogomall.php'); 
    }    

    
}


// USER WEBSITE CODE

//:: ADD OR UPDATE NEW BANNER

if(isset($_POST['addbanner'])){

    $item_company = $_POST['banner_company'];
    $updateitem_img = $_FILES['banner_img']['name'];

    //:: UPDATING THE IMAGE::
    $update_img = "SELECT * FROM food_company where fcompany_id= '$item_company'";
    $run_update_img = mysqli_query($connection, $update_img);

    foreach($run_update_img as $fa_row){
        if($updateitem_img == NULL){
            $image_data = $fa_row['company_banner_img'];
        }else{
            //: upload with new image and delete with old image
            if($img_path = "homepage_images/".$fa_row['company_banner_img']){
                unlink($img_path);
                $image_data = $updateitem_img;
            }
            
        }
    }


    $query = "UPDATE food_company SET company_banner_img='$image_data'    
    WHERE fcompany_id='$item_company' ";
    $query_run = mysqli_query($connection, $query);

    if($query_run)
    {
        if($updateitem_img == NULL){
            $_SESSION['success'] = "Item is Added with existing image";
            header('Location: userwebsite_homepage_banner.php'); 
        }else{
            //: upload with new image and delete with old image
            move_uploaded_file($_FILES["banner_img"]["tmp_name"], "homepage_images/".$_FILES["banner_img"]["name"] );
            $_SESSION['success'] = "Item is Added";
            header('Location: userwebsite_homepage_banner.php'); 
        }
      
    }
    else
    {
        $_SESSION['status'] = "item is NOT Added";
        // $_SESSION['status_code'] = "error";
        header('Location: userwebsite_homepage_banner.php'); 
    } 

}





//: Delete Banner
if(isset($_POST['deletebanner_btn']))
{
    
    $prodictitemdeleteid = $_POST['deletebanner_id'];

    $query = "UPDATE food_company SET company_banner_img='' WHERE fcompany_id='$prodictitemdeleteid' ";
    $query_run = mysqli_query($connection, $query);

    if($query_run)
    {
        $_SESSION['success'] = "item is Deleted";
        // $_SESSION['status_code'] = "success";
        header('Location: userwebsite_homepage_banner.php'); 
    }
    else
    {
        $_SESSION['status'] = "Item is NOT DELETED";       
        // $_SESSION['status_code'] = "error";
        header('Location: userwebsite_homepage_banner.php'); 
    }    

    
}


//: top product yes button

if(isset($_POST['top_product_yes_btn'])){

    $topProduct_yes_id = $_POST['top_product_yes_id'];


    $update_topProduct = "UPDATE food_items SET top_product = 'yes' WHERE item_id='$topProduct_yes_id' ";
    $run_topProduct = mysqli_query($connection,$update_topProduct);

    if($run_topProduct){
        // $_SESSION['success'] = "Product is Added in Top Product";
        // $_SESSION['status_code'] = "success";
        header('Location: userwebsite_homepage_topproduct.php'); 
    }
    else
    {
        // $_SESSION['status'] = "Product is not Added in Top Product";       
        // $_SESSION['status_code'] = "error";
        header('Location:userwebsite_homepage_topproduct.php'); 
    }    
}

//: top product no button

if(isset($_POST['top_product_no_btn'])){

    $topProduct_no_id = $_POST['top_product_no_id'];
   

    $update_topProduct = "UPDATE food_items SET top_product='' WHERE item_id='$topProduct_no_id' ";
    $run_topProduct = mysqli_query($connection,$update_topProduct);

    if($run_topProduct){
        // $_SESSION['success'] = "Product is remove from Top Product";
        // $_SESSION['status_code'] = "success";
        header('Location: userwebsite_homepage_topproduct.php'); 
    }
    else
    {
        // $_SESSION['status'] = "Product is remove from Top Product";       
        // $_SESSION['status_code'] = "error";
        header('Location:userwebsite_homepage_topproduct.php'); 
    }    
}


//: New Arrival button

if(isset($_POST['new_arrival_btn'])){

    $new_arrival_id = $_POST['new_arrival_id'];


    //: Count New Arrival items
    $query = "Select * from food_items where trending='New Arrival'";
    $run_query = mysqli_query($connection, $query);
    $count =  mysqli_num_rows($run_query);


    if($count>2){
        $_SESSION['success'] = "Maximum no of adding the item in new arrival is 3";
        header('Location: userwebsite_homepage_topproduct.php'); 
    }else {

         $update_Product = "UPDATE food_items SET trending = 'New Arrival' WHERE item_id='$new_arrival_id' ";
         $run_Product = mysqli_query($connection,$update_Product);

            if($run_Product){
                // $_SESSION['success'] = "Product is Added in Top Product";
                // // $_SESSION['status_code'] = "success";
                header('Location: userwebsite_homepage_topproduct.php'); 
            }
            else
            {
                // $_SESSION['status'] = "Product is not Added in Top Product";       
                // // $_SESSION['status_code'] = "error";
                header('Location:userwebsite_homepage_topproduct.php'); 
            }    

    }


 
}

//: Daily Discover button

if(isset($_POST['daily_discover_btn'])){

    $daily_discover_id = $_POST['daily_discover_id'];

      //: Count New Arrival items
      $query = "Select * from food_items where trending='Daily Discover'";
      $run_query = mysqli_query($connection, $query);
      $count =  mysqli_num_rows($run_query);
  
  
      if($count>2){

        $_SESSION['success'] = "Maximum no of adding the item in Daily Discover is 3";
        header('Location: userwebsite_homepage_topproduct.php'); 

      }else{

            $update_Product = "UPDATE food_items SET trending = 'Daily Discover' WHERE item_id='$daily_discover_id' ";
            $run_Product = mysqli_query($connection,$update_Product);

            if($run_Product){
                // $_SESSION['success'] = "Product is Added in Top Product";
                // // $_SESSION['status_code'] = "success";
                header('Location: userwebsite_homepage_topproduct.php'); 
            }
            else
            {
                // $_SESSION['status'] = "Product is not Added in Top Product";       
                // // $_SESSION['status_code'] = "error";
                header('Location:userwebsite_homepage_topproduct.php'); 
            }  
    
    }
}

//: Trending Search Button 
if(isset($_POST['trending_search_btn'])){

    $trending_search_id = $_POST['trending_search_id'];

     //: Count New Arrival items
     $query = "Select * from food_items where trending='Trending Search'";
     $run_query = mysqli_query($connection, $query);
     $count =  mysqli_num_rows($run_query);


     if($count>2){

        $_SESSION['success'] = "Maximum no of adding the item in Trending Search is 3";
        header('Location: userwebsite_homepage_topproduct.php'); 

      }else{



            $update_Product = "UPDATE food_items SET trending = 'Trending Search' WHERE item_id='$trending_search_id' ";
            $run_Product = mysqli_query($connection,$update_Product);

            if($run_Product){
                // $_SESSION['success'] = "Product is Added in Trending Search";
                // // $_SESSION['status_code'] = "success";
                header('Location: userwebsite_homepage_topproduct.php'); 
            }
            else
            {
                // $_SESSION['status'] = "Product is not Added in Trending Search";       
                // // $_SESSION['status_code'] = "error";
                header('Location:userwebsite_homepage_topproduct.php'); 
            }   
    
    }
}

//: Null Button 
if(isset($_POST['null_btn'])){

    $null_id = $_POST['null_id'];

    $update_Product = "UPDATE food_items SET trending = '' WHERE item_id='$null_id' ";
    $run_Product = mysqli_query($connection,$update_Product);

    if($run_Product){
        // $_SESSION['success'] = "Product is Added in Top Product";
        // // $_SESSION['status_code'] = "success";
        header('Location: userwebsite_homepage_topproduct.php'); 
    }
    else
    {
        // $_SESSION['status'] = "Product is not Added in Top Product";       
        // // $_SESSION['status_code'] = "error";
        header('Location:userwebsite_homepage_topproduct.php'); 
    }    
}


//:: Contact Us Page: 
if(isset($_POST['updatecontactusbtn'])){

   $update_maplink = $_POST['update_maplink'];
   $update_address = $_POST['update_address'];
   $update_phone = $_POST['update_phone'];
   $update_description = $_POST['update_description'];

   $update_contactusdata = "UPDATE contact_page SET contact_link = '$update_maplink', contact_address = '$update_address', contact_phone = '$update_phone', contact_description = '$update_description' where contactid = '1' ";
   $run_update_contactusdata = mysqli_query($connection, $update_contactusdata);

   if($run_update_contactusdata)
    {
        $_SESSION['success'] = "Data is updated";
        // $_SESSION['status_code'] = "success";
        header('Location:  userwebsite_contactuspage_data.php'); 
    }
    else
    {
        $_SESSION['status'] = "Data is not updated";       
        // $_SESSION['status_code'] = "error";
        header('Location: userwebsite_contactuspage_data.php'); 
    }    
}


//:: About Us page: 

//:: Add new question
if(isset($_POST['addaboutusquestionbtn'])){

    $aboutus_question = $_POST['aboutus_question'];
    $aboutus_answer = $_POST['aboutus_answer'];

    $insert_aboutus = "INSERT INTO about_questions(about_question, about_answer) values('$aboutus_question','$aboutus_answer')";
    $run_aboutus = mysqli_query($connection,$insert_aboutus);

    if($run_aboutus)
    {
        $_SESSION['success'] = "Data is added";
        // $_SESSION['status_code'] = "success";
        header('Location:  userwebsite_aboutuspage_question.php'); 
    }
    else
    {
        $_SESSION['status'] = "Data is not added";       
        // $_SESSION['status_code'] = "error";
        header('Location: userwebsite_aboutuspage_question.php'); 
    }    


    

}

//: Update About us question
if(isset($_POST['updateaboutusbtn']))
{
    $aboutus_update_id = $_POST['aboutus_updateid'];
    $aboutus_update_question = $_POST['aboutus_updatequestion'];
    $aboutus_update_answer = $_POST['aboutus_updateanswer'];
  

    $update_aboutus = "UPDATE about_questions SET about_question='$aboutus_update_question', about_answer='$aboutus_update_answer' WHERE about_id='$aboutus_update_id' ";
    $run_update_aboutus = mysqli_query($connection, $update_aboutus);

    if($run_update_aboutus)
    {
        $_SESSION['success'] = "Data is Updated";
        // $_SESSION['status_code'] = "success";
        header('Location:  userwebsite_aboutuspage_question.php'); 
    }
    else
    {
        $_SESSION['status'] = "Data is not Updated";       
        // $_SESSION['status_code'] = "error";
        header('Location: userwebsite_aboutuspage_question.php'); 
    }   
}

//: Update About us question
if(isset($_POST['aboutus_delete_btn']))
{
    $aboutus_delete_id = $_POST['aboutus_delete_id'];

    $delete_aboutus = "DELETE FROM about_questions WHERE about_id ='$aboutus_delete_id' ";
    $run_delete_aboutus = mysqli_query($connection, $delete_aboutus);

    if($run_delete_aboutus)
    {
        $_SESSION['success'] = "Data is Delete";
        // $_SESSION['status_code'] = "success";
        header('Location:  userwebsite_aboutuspage_question.php'); 
    }
    else
    {
        $_SESSION['status'] = "Data is not Deleted";       
        // $_SESSION['status_code'] = "error";
        header('Location: userwebsite_aboutuspage_question.php'); 
    }   
    
}
















?>
 
<?php

 // if(mysqli_query($connection, $insert_foodcategory)){
    //     echo "data insert successfully";
    // }else{

    //     echo "Error insertin data: " . mysqli_error($connection);
    // }
?>


