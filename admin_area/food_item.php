<?php
include('security.php');
include('includes/header.php'); 
include('includes/navbar.php'); 
?>

<div class="modal fade" id="addadminprofile" tabindex="-1" role="dialog" aria-labelledby="exampleModalScrollableTitle" aria-hidden="true">
  <div class="modal-dialog modal-dialog-scrollable" role="document">
    <div class="modal-content">
      <div class="modal-header">
        <h6 style="text-align: center;  color: black; font-weight: bold; font-size: 16px;"class="modal-title" id="exampleModalScrollableTitle"> ADD NEW FOOD ITEM</h6>
      </div>
        <div class="modal-body">
                <form action="code.php" method="POST" enctype="multipart/form-data">

                    <div class="form-row">
                        <div class="form-group col-md-6">
                            <label> Food Title: </label>
                            <input type="text" name="item_title" class="form-control" placeholder="Enter Food title" required="required">

                        </div>
                        <div class="form-group col-md-6">
                            <label> Food Category:</label>
                            <select name="item_cat" class="form-control" required="required" placeholder="Enter Food title" >
                                        <option>
                                        <!-- Select a Category -->
                                        </option>
                                        <?php
                                            require 'dbconfig.php';
                                            $get_cats = "select * from food_category where cat_type = 'food'";
                                            $run_cats = mysqli_query($connection, $get_cats);
                                            while($row_cats = mysqli_fetch_array($run_cats)){
                                                $cat_id = $row_cats['fcat_id'];
                                                $cat_title = $row_cats['food_cat'];
                                                echo "<option value='$cat_id'>$cat_title</option>";
                                            } 
                                        ?>
                                    </select>
                        </div>
                    </div>

                    <div class="form-row">
                        <div class="form-group col-md-6">
                            <label>Food Company/Hotel</label>
                            <select name="item_company" class="form-control" required="required">
                                        <option>
                                        <!-- Select a Hotel -->
                                        </option>
                                        <?php
                                            require 'dbconfig.php';
                                            $get_brand = "select * from food_company where company_type = 'food'";
                                            $run_brand = mysqli_query($connection, $get_brand);
                                            while($row_brand = mysqli_fetch_array($run_brand)){
                                                $brand_id = $row_brand['fcompany_id'];
                                                $brand_title = $row_brand['Company_name'];
                                                echo "<option value='$brand_id'>$brand_title</option>";
                                            } 
                                        ?>
                            </select>
                        </div>
                        <div class="form-group col-md-6">
                            <label>Food Item Availability:</label>
                            <select name="item_availability" class="form-control" required="required" >
                                        <option>
                                        <!-- Choose -->
                                        </option>
                                        <option>IN STOCK</option>
                                        <option>NEW</option>
                                        <option>TREND</option>
                                    </select>
                        </div>
                    </div>
                    
                    <div class="form-row">
                        <div class="form-group col-md-6">
                            <label>Price</label>
                            <input type="text" name="item_price" class="form-control" required="required" />
                        </div>
                        <div class="form-group col-md-6">
                            <label>Discount:</label>
                            <select name="item_discounted_percentage" class="form-control" required="required" >
                                        <option>
                                        <!-- Choose in Percentage -->
                                        </option>
                                        <option>0</option>
                                        <option>10</option>
                                        <option>20</option>
                                        <option>40</option>
                                        <option>60</option>
                                    </select>
                        
                        </div>
                    </div>
                
                        
                    <div class="form-row">
                        <div class="form-group col-md-12">
                        <label for="inputZip">Food Image</label>
                        <input type="file" class="form-control" name="item_img" required="required">
                        </div>
                    </div>

                    <div class="form-row">
                        <div class="form-group col-md-12">
                        <label for="inputZip">Food Youtube Link</label>
                        <input type="text"  class="form-control" name="item_youtubelink" size="50" />
                        </div>
                    </div>

                    <div class="form-row">
                        <div class="form-group col-md-12">
                        <label for="inputZip">Food-Description</label>
                        <textarea name="item_desc" class="form-control" cols="20" rows="5" required="required"></textarea>
                        </div>
                    </div>
                        
                    <div style=" border-top: 0 none;" class="modal-footer"> 
                        <button type="button" class="btn btn-secondary" data-dismiss="modal">Close</button>
                        <button type="submit"  class="btn btn-primary"  name="addfooditem">Save</button>
                    </div>
                </form> 
        </div>
    </div>
  </div>
</div>



<!-- MODAL -->


<div class="container-fluid">

<!-- DataTales Example -->
<div class="card shadow mb-4">
  <div class="card-header py-3">
    <h6 class="m-0 font-weight-bold text-primary">FoodItem's Data
            <button type="button" class="btn btn-primary" data-toggle="modal" data-target="#addadminprofile">
              Add New Food-Item
            </button>
    </h6>
  </div>

  <div class="card-body">

    <?php
      if(isset($_SESSION['success']) && $_SESSION['success']!=''){
        echo '<h2> '.$_SESSION['success'].' </h2>.';
        unset($_SESSION['success']);
      } 
      if(isset($_SESSION['status']) && $_SESSION['status']!=''){
        echo '<h2> '.$_SESSION['status'].' </h2>.';
        unset($_SESSION['status']);
      } 
      
    ?>

    <div class="table-responsive">

      <?php

      
      require 'dbconfig.php';

      $query = "SELECT * FROM food_items where item_type = 'food'";
      $query_run = mysqli_query($connection, $query);

      ?>

      <table class="table table-bordered" id="dataTable" width="100%" cellspacing="0">
        <thead>
          <tr>
            <th> Image </th>
            <th> ID </th>
            <th> Category </th>
            <th> Food Store </th>
            <th>Date Added</th>
            <th>Title</th>
            <th>Price </th>
            <th>Description </th>
            <th>Youtube link </th>
            <!-- <th>Discount </th> -->
            <th>Availability </th>
          </tr>
        </thead>
        <tbody>

<?php 
  if(mysqli_num_rows($query_run) > 0)        
  {
      while($row = mysqli_fetch_assoc($query_run))
      {

        ?>

        




      <tr>
          <td> <?php echo '<img src="fooditem_images/'.$row['item_img'].'" width="100px;" height="100px;" alt="image" >' ?> </td>
          <td> <?php  echo $row['item_id']; ?></td>
          

          
          
          <!-- Fetch Category -->
          <?php 

        
          ?>

          <td> 
            <?php
           
              $category_id = $row['fcat_id']; 

              $get_cat_data = "Select * from food_category where fcat_id = '$category_id'";
              $run_cat_data = mysqli_query($connection,$get_cat_data);
              $fetch_cat_data = mysqli_fetch_array($run_cat_data);

              $category_name = $fetch_cat_data['food_cat'];

              echo $category_name;
            
            
            ?>
          </td>
          <td> 
            <?php  
                
              $hotel_id  = $row['fcompany_id'];

              $get_hotel_data = "Select * from food_company where fcompany_id = '$hotel_id'";
              $run_hotel_data = mysqli_query($connection,$get_hotel_data);
              $fetch_hotel_data = mysqli_fetch_array($run_hotel_data);

              $hotel_name = $fetch_hotel_data['Company_name'];

              echo "$hotel_name";
          
            ?>
          </td>
          <td> <?php  echo $row['date']; ?> </td>
          <td> <?php  echo $row['item_title']; ?></td>
          <td> <?php  echo $row['item_price'];  ?>RM </td>
          <!-- <td> <?php  echo $row['item_discounted_price']; ?>% </td> -->
          <td> <?php  echo $row['item_desc']; ?></td>
          <td> <?php  echo $row['youtube_link']; ?></td>

          <td> <?php  echo $row['item_avalability']; ?></td>
      </tr>
<?php
      }
    }else {
      echo "No Record Found";
    }
?>



</tbody>
      </table>

    </div>
  </div>
</div>

</div>
<!-- /.container-fluid -->

<?php
include('includes/scripts.php');
include('includes/footer.php');
?>