<?php

include('foodstore_security.php');
include('dbconfig.php');
// // $connection = mysqli_connect("localhost","root","","gogoshop");
// $connection = mysqli_connect("localhost","gogoempi_gotravelweb","OfKTL6PbD6IP","gogoempi_gotravelweb");



//: Getting current foodstore id: 

$store_email = $_SESSION['foodusername'];
                                    
$get_foodstore_details = "Select * from food_company where company_email='$store_email'";
$run_foodstore_details = mysqli_query($connection, $get_foodstore_details);
$fetch_foodstore_details = mysqli_fetch_array($run_foodstore_details);
                                                                                                
$foodstore_id = $fetch_foodstore_details['fcompany_id']; //:: Customer id 
$foodstore_name = $fetch_foodstore_details['Company_name']; //:: customer name


//: Add New Food Item..

if(isset($_POST['addfoodstoreitem'])){
   
    $item_title = $_POST['food_title'];
    $item_cat = $_POST['food_cat'];
    $item_price = $_POST['food_price'];
    $item_desc = $_POST['food_desc'];
    $status = "on";
    $item_youtubelink = $_POST['food_youtubelink'];
    // $item_discounted_percentage = $_POST['item_discounted_percentage'];
    $item_availability = $_POST['food_availability'];
    $item_company = "$foodstore_id";
    $item_discounted_percentage = '1';
    $item_type = 'food';
    $food_img = $_FILES['food_img']['name'];

    if(file_exists("fooditem_images/" .$_FILES["food_img"]["name"])){

        $image_already_exist = $_FILES["food_img"]["name"];
        $_SESSION['status'] = "Image Already exists. $store";
        header('Location: foodstore_fooditems.php'); 

    }else{

        $insert_fooditem = "insert into food_items (fcat_id,fcompany_id,date,item_title,item_img,item_price,item_desc,youtube_link,status,item_discounted_price,item_avalability,item_type,top_product,trending) values 
        ('$item_cat','$item_company',NOW(),'$item_title','$food_img',
        '$item_price','$item_desc','$item_youtubelink','$status','$item_discounted_percentage','$item_availability', '$item_type','','')";

        if(mysqli_query($connection, $insert_fooditem)){
            move_uploaded_file($_FILES["food_img"]["tmp_name"], "fooditem_images/".$_FILES["food_img"]["name"] );
            $_SESSION['success'] = "Food Item is Added";
            header('Location: foodstore_fooditems.php'); 
        }else{ 

            $_SESSION['status'] = "Food Item is not Added";       
            header('Location: foodstore_fooditems.php'); 
        }

    //      if(mysqli_query($connection, $insert_fooditem)){
    //         move_uploaded_file($_FILES["food_img"]["tmp_name"], "fooditem_images/".$_FILES["food_img"]["name"] );
    //             $_SESSION['success'] = "Food Item is Added";
    //             header('Location: foodstore_fooditems.php'); 
    // }else{

    //     echo "Error insertin data: " . mysqli_error($connection);
    // }
    }
}

//: Update Food item

if(isset($_POST['updatefoodstoreitem'])){

    $updateitem_id = $_POST['food_updateid'];
    $updateitem_title = $_POST['food_updatetitle'];
    $updateitem_cat = $_POST['food_updatecat'];
    $updateitem_price = $_POST['food_updateprice'];
    $updateitem_desc = $_POST['food_updatedesc'];
    $status = "on";
    $updateitem_youtubelink = $_POST['food_updateyoutubelink'];
    // $item_discounted_percentage = $_POST['item_discounted_percentage'];
    $updateitem_availability = $_POST['food_updateavailability'];
    $item_company = "$foodstore_id";
    $item_discounted_percentage = 1;
    $updateitem_img = $_FILES['food_updateimg']['name'];
    

    //:: UPDATING THE IMAGE::
    $update_img = "SELECT * FROM food_items where item_id= '$updateitem_id'";
    $run_update_img = mysqli_query($connection, $update_img);

    foreach($run_update_img as $fa_row){
        if($updateitem_img == NULL){
            $image_data = $fa_row['item_img'];
        }else{
            //: upload with new image and delete with old image
            if($img_path = "fooditem_images/".$fa_row['item_img']){
                unlink($img_path);
                $image_data = $updateitem_img;
            }
            
        }
    }

    $query = "UPDATE food_items SET fcat_id='$updateitem_cat', fcompany_id='$item_company', item_title='$updateitem_title', item_img='$image_data', item_price='$updateitem_price', 
    item_desc='$updateitem_desc', youtube_link='$updateitem_youtubelink', status='$status', item_discounted_price='$item_discounted_percentage', item_avalability='$updateitem_availability'   
    WHERE item_id='$updateitem_id' ";
    $query_run = mysqli_query($connection, $query);

    if($query_run)
    {
        if($updateitem_img == NULL){
            $_SESSION['success'] = "Item is Updated with existing image";
            header('Location: foodstore_fooditems.php'); 
        }else{
            //: upload with new image and delete with old image
            move_uploaded_file($_FILES["food_updateimg"]["tmp_name"], "fooditem_images/".$_FILES["food_updateimg"]["name"] );
            $_SESSION['success'] = "Item is Updated";
            header('Location: foodstore_fooditems.php'); 
        }
      
    }
    else
    {
        $_SESSION['status'] = "item is NOT Updated";
        // $_SESSION['status_code'] = "error";
        header('Location: foodstore_fooditems.php'); 
    }

}

//: Delete Food item
if(isset($_POST['deletefoodstore_btn']))
{
    $fooditemdeleteid = $_POST['deletefoodstore_id'];

    $query = "DELETE FROM food_items WHERE item_id='$fooditemdeleteid' ";
    $query_run = mysqli_query($connection, $query);

    if($query_run)
    {
        $_SESSION['success'] = "item is Deleted";
        // $_SESSION['status_code'] = "success";
        header('Location: foodstore_fooditems.php'); 
    }
    else
    {
        $_SESSION['status'] = "Item is NOT DELETED";       
        // $_SESSION['status_code'] = "error";
        header('Location: foodstore_fooditems.php'); 
    }    
}


//: Completed Pickup order button
if(isset($_POST['confirm_pending_order_btn']))
{
    $confirmpickuporderid = $_POST['confirm_pending_order_id'];

    $query = "Select * from customer_pickuporder where porder_id = '$confirmpickuporderid' ";
    $run_query = mysqli_query($connection, $query);
    $fetch_query = mysqli_fetch_array($run_query);

    $getcustomer_id = $fetch_query['customer_id'];
    $getcustomer_order_no = $fetch_query['customer_order_no'];
    $gethotel_id = $fetch_query['hotel_id'];
    $getorder_date = $fetch_query['order_date'];
    $getinvoiceno = $fetch_query['invoiceno'];
    $getorder_desc = $fetch_query['order_description'];
    $getdue_amount = $fetch_query['due_amount'];
    $getpickup_order_date = $fetch_query['pickup_order_date'];
    $getpickup_order_time = $fetch_query['pickup_order_time'];
    $getorder_status = 'Completed with Payment';
    $ordertype = 'FOOD-PICKUP';


    $insert_completed_order = "Insert into completed_orders(customer_id,customer_order_no,hotel_id,order_date,invoice_no,order_description,due_amount,
    pickup_order_date,pickup_order_time,order_status,order_type)values('$getcustomer_id','$getcustomer_order_no','$gethotel_id',
    '$getorder_date','$getinvoiceno','$getorder_desc','$getdue_amount','$getpickup_order_date','$getpickup_order_time','$getorder_status','$ordertype')";

    $run_completed_order = mysqli_query($connection, $insert_completed_order);

    $delete_pickup_order = "DELETE FROM customer_pickuporder where porder_id='$confirmpickuporderid'";
    $run_delete_query = mysqli_query($connection, $delete_pickup_order);

    if($run_delete_query){
        $_SESSION['success'] = "DONE";
        // $_SESSION['status_code'] = "success";
        header('Location: foodstore_pickuporders.php'); 
        
    }else{ 

        $_SESSION['status'] = "NOT DONE";       
        // $_SESSION['status_code'] = "error";
        header('Location: foodstore_pickuporders.php'); 
    
    }
}

//: Cancelled Pickup order button
if(isset($_POST['cancel_pending_order_btn']))
{
    $cancelledickuporderid = $_POST['cancel_pending_order_id'];

    $query = "Select * from customer_pickuporder where porder_id = '$cancelledickuporderid' ";
    $run_query = mysqli_query($connection, $query);
    $fetch_query = mysqli_fetch_array($run_query);

    $getcustomer_id = $fetch_query['customer_id'];
    $getcustomer_order_no = $fetch_query['customer_order_no'];
    $gethotel_id = $fetch_query['hotel_id'];
    $getorder_date = $fetch_query['order_date'];
    $getinvoiceno = $fetch_query['invoiceno'];
    $getorder_desc = $fetch_query['order_description'];
    $getdue_amount = $fetch_query['due_amount'];
    $getpickup_order_date = $fetch_query['pickup_order_date'];
    $getpickup_order_time = $fetch_query['pickup_order_time'];
    $getorder_status = 'Cancelled';
    $ordertype = 'FOOD-PICKUP';


    $insert_cancelled_order = "Insert into cancelled_orders(customer_id,customer_order_no,hotel_id,order_date,invoice_no,order_description,due_amount,
    pickup_order_date,pickup_order_time,order_status,order_type)values('$getcustomer_id','$getcustomer_order_no','$gethotel_id',
    '$getorder_date','$getinvoiceno','$getorder_desc','$getdue_amount','$getpickup_order_date','$getpickup_order_time','$getorder_status','$ordertype')";

    $run_completed_order = mysqli_query($connection, $insert_cancelled_order);

    $delete_pickup_order = "DELETE FROM customer_pickuporder where porder_id='$cancelledickuporderid'";
    $run_delete_query = mysqli_query($connection, $delete_pickup_order);

    if($run_delete_query){
        $_SESSION['success'] = "DONE";
        // $_SESSION['status_code'] = "success";
        header('Location: foodstore_pickuporders.php'); 
        
    }else{ 

        $_SESSION['status'] = "NOT DONE";       
        // $_SESSION['status_code'] = "error";
        header('Location: foodstore_pickuporders.php'); 
    
    }
}





?>


