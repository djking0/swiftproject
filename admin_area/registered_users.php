<?php
include('security.php');
include('includes/header.php'); 
include('includes/navbar.php'); 
?>




<div class="container-fluid">

<!-- DataTales Example -->
<div class="card shadow mb-4">
  <div class="card-header py-3">
    <h6 class="m-0 font-weight-bold text-primary">Registered Users
    </h6>
  </div>

  <div class="card-body">

    <div class="table-responsive">
      <?php

      
      require 'dbconfig.php';

      $query = "SELECT * FROM customers";
      $query_run = mysqli_query($connection, $query);

      ?>

      <table class="table table-bordered" id="dataTable" width="100%" cellspacing="0">
        <thead>
          <tr>
            <th> ID </th>
            <th> Full Name </th>
            <th> Email </th>
            <th>Country</th>
            <th>City</th>
            <th>Contact Number</th>
            <th>Address</th>
          </tr>
        </thead>
        <tbody>

          <?php 
            if(mysqli_num_rows($query_run) > 0)        
            {
                while($row = mysqli_fetch_assoc($query_run))
                {

                  ?>

                <tr>
                   
                    <td> <?php  echo $row['customer_id']; ?></td>
                    <td> <?php  echo $row['customer_name']; ?></td>
                    <td> <?php  echo $row['customer_email']; ?> </td>
                    <td> <?php  echo $row['customer_country']; ?></td>
                    <td> <?php  echo $row['customer_city'];  ?> </td>
                    <td> <?php  echo $row['customer_contact']; ?></td>
                    <td> <?php  echo $row['customer_address']; ?></td>
                </tr>
          <?php
                }
              }else {
                echo "No Record Found";
              }
          ?>
     
          
        
        </tbody>
      </table>

    </div>
  </div>
</div>

</div>
<!-- /.container-fluid -->

<?php
include('includes/scripts.php');
include('includes/footer.php');
?>