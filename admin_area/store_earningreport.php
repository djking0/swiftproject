<?php
include('security.php');
include('includes/header.php'); 
include('includes/navbar.php'); 
?>

<div class="container-fluid">

<!-- DataTales Example -->
<div class="card shadow mb-4">
  <div class="card-header py-3">
    <h6 class="m-0 font-weight-bold text-primary">Store's Earning Report
    </h6>
  </div>

  <div class="card-body">

    <div class="table-responsive">

      <?php

      
      require 'dbconfig.php';

      $query = "SELECT * FROM food_company where company_type = 'product'";
      $query_run = mysqli_query($connection, $query);

      ?>

      <table class="table table-bordered" id="dataTable" width="100%" cellspacing="0">
        <thead>
          <tr>
            <th> ID </th>
            <th> Name </th>
            <th> No of Completed Orders</th>
            <th> No of Canceled Orders</th>
            <th> Earning So for.. </th>
          </tr>
        </thead>
        <tbody>

          <?php 
            if(mysqli_num_rows($query_run) > 0)        
            {
                while($row = mysqli_fetch_assoc($query_run))
                {

                  ?>

                <tr>
                    <td> <?php  echo $row['fcompany_id']; ?></td>
                    <td> <?php echo $row['Company_name']; ?></td>
                    <td> coming soon </td>
                    <td> coming soon </td>
                    <td> coming soon </td>
                 
                </tr>
          <?php
                }
              }else {
                echo "No Record Found";
              }
          ?>
     
          
        
        </tbody>
      </table>

    </div>
  </div>
</div>

</div>
<!-- /.container-fluid -->

<?php
include('includes/scripts.php');
include('includes/footer.php');
?>