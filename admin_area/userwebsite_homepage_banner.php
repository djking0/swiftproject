<?php
include('security.php');
include('includes/header.php'); 
include('includes/navbar.php'); 
?>


<div class="modal fade" id="addadminprofile" tabindex="-1" role="dialog" aria-labelledby="exampleModalScrollableTitle" aria-hidden="true">
  <div class="modal-dialog modal-dialog-scrollable" role="document">
    <div class="modal-content">
      <div class="modal-header">
        <h6 style="text-align: center;  color: black; font-weight: bold; font-size: 16px;"class="modal-title" id="exampleModalScrollableTitle"> ADD NEW BANNER</h6>
      </div>
        <div class="modal-body">
                <form action="code.php" method="POST" enctype="multipart/form-data">

                <div class="form-group col-md-12">
                            <label>Choose Hotel / Store:</label>
                            <select name="banner_company" class="form-control" required="required">
                                        <option>
                                        <!-- Select a Hotel -->
                                        </option>
                                        <?php
                                            require 'dbconfig.php';
                                            $get_brand = "select * from food_company where company_type != 'admin'";
                                            $run_brand = mysqli_query($connection, $get_brand);
                                            while($row_brand = mysqli_fetch_array($run_brand)){
                                                $brand_id = $row_brand['fcompany_id'];
                                                $brand_title = $row_brand['Company_name'];
                                                echo "<option value='$brand_id'>$brand_title</option>";
                                            } 
                                        ?>
                            </select>
                        </div>
                   
                        <div class="form-group col-md-12">
                        <label for="inputZip">Upload Banner image:</label>
                        <input type="file" class="form-control" name="banner_img" required="required">
                        </div>
                        
                    <div style=" border-top: 0 none;" class="modal-footer"> 
                        <button type="button" class="btn btn-secondary" data-dismiss="modal">Close</button>
                        <button type="submit"  class="btn btn-primary"  name="addbanner">Save</button>
                    </div>
                </form> 
        </div>
    </div>
  </div>
</div>




<div class="container-fluid">

<!-- DataTales Example -->
<div class="card shadow mb-4">
  <div class="card-header py-3">
    <h6 class="m-0 font-weight-bold text-primary">Home Page Banner Section
    <button type="button" class="btn btn-primary" data-toggle="modal" data-target="#addadminprofile">
              Add New Banner
            </button>
    </h6>
  </div>

  <div class="card-body">

  <?php
      if(isset($_SESSION['success']) && $_SESSION['success']!=''){
        echo '<h2> '.$_SESSION['success'].' </h2>.';
        unset($_SESSION['success']);
      } 
      if(isset($_SESSION['status']) && $_SESSION['status']!=''){
        echo '<h2> '.$_SESSION['status'].' </h2>.';
        unset($_SESSION['status']);
      } 
      
    ?>

    <div class="table-responsive">
      <?php

      
      require 'dbconfig.php';

      $query = "SELECT * FROM food_company where company_banner_img != ''";
      $query_run = mysqli_query($connection, $query);

      ?>

      <table class="table table-bordered" id="dataTable" width="100%" cellspacing="0">
        <thead>
          <tr>
            <th> ID </th>
            <th> Store Name </th>
            <th> Store Type </th>
            <th> Banner image </th>
            <th>Delte</th>
          </tr>
        </thead>
        <tbody>

          <?php 
            if(mysqli_num_rows($query_run) > 0)        
            {
                while($row = mysqli_fetch_assoc($query_run))
                {

                  ?>

                <tr>
                   
                    <td> <?php  echo $row['fcompany_id']; ?></td>
                    <td> <?php  echo $row['Company_name']; ?></td>
                    <td> <?php  echo $row['company_type']; ?> </td>
                    <td><?php echo '<img src="homepage_images/'.$row['company_banner_img'].'" width="300px;" height="100px;" alt="image" >' ?> </td>
                  <td>
                      <form action="code.php" method="post">
                        <input type="hidden" name="deletebanner_id" value="<?php  echo $row['fcompany_id']; ?>">
                        <button type="submit" name="deletebanner_btn" class="btn btn-danger"> DELETE</button>
                      </form>
                  </td>
                </tr>
          <?php
                }
              }else {
                echo "No Record Found";
              }
          ?>
     
          
        
        </tbody>
      </table>

    </div>
  </div>
</div>

</div>
<!-- /.container-fluid -->

<?php
include('includes/scripts.php');
include('includes/footer.php');
?>