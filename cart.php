<?php
include("includes/db.php");
include("functions/functions.php");
session_start();

?>
<!DOCTYPE html>
<html lang="en">
  <head>
    <meta charset="UTF-8" />
    <meta name="viewport" content="width=device-width, initial-scale=1.0" />
    <meta http-equiv="X-UA-Compatible" content="ie=edge" />
    <title>Gogo Empire Store</title>

    <!-- Bootstrap CDN -->
    <link
      rel="stylesheet"
      href="https://stackpath.bootstrapcdn.com/bootstrap/4.3.1/css/bootstrap.min.css"
      integrity="sha384-ggOyR0iXCbMQv3Xipma34MD+dH/1fQ784/j6cY/iJTQUOhcWr7x9JvoRxT2MZw1T"
      crossorigin="anonymous"
    />

    <!--  Font Awesome CDN -->
    <script src="https://kit.fontawesome.com/23412c6a8d.js"></script>

    <!-- Slick Slider -->
    <link
      rel="stylesheet"
      type="text/css"
      href="//cdn.jsdelivr.net/npm/slick-carousel@1.8.1/slick/slick.css"
    />

    
    <!-- Custom Stylesheet -->
    <link rel="stylesheet" href="./css/style.css" />
    <link rel="stylesheet" href="./css/viewproduct.css" />

  </head>

  <body>
    <!-- header -->

    <header>
      <div class="container">
        <div class="row">
          <div class="col-md-4 col-sm-12 col-12">
            <div class="btn-group">
              <button
                class="btn border dropdown-toggle my-md-4 my-2 text-white"
                data-toggle="dropdown"
                aria-haspopup="true"
                aria-expanded="false"
              >
                ENG
              </button>
              <div class="dropdown-menu">
                <a href="#" class="dropdown-item">Bhasa - Malaysia</a>
              </div>
            </div>
          </div>
          <div class="col-md-4 col-12 text-center">
            <h2 class="my-md-3 site-title text-white">Gogo Empire </h2>
          </div>
          <div class="col-md-4 col-12 text-right">
          <p class="my-md-4 header-links">
              <?php
                if(!isset($_SESSION['customer_email'])){
                echo"";
                }else{
                  
                  echo"<a href='' class='px-1'> $_SESSION[customer_email] </a>";
                  echo"<a class='px-2' style='color:white;'>|</a>";
                }
              ?>
              <?php
                if(!isset($_SESSION['customer_email'])){
                echo"<a href='checkout.php' class='px-2'>login</a>";
                echo"<a class='px-2' style='color:white;'>|</a>";
                echo"<a href='register.php' class='px-2'>Create an Account</a>";
                }else{
                  echo"<a href='logout.php' class='px-2'>logout</a>";
                }
              ?>
            </p>
          </div>
        </div>
      </div>

      <div class="container-fluid p-0">
        <nav class="navbar navbar-expand-lg navbar-light bg-white">
          <button
            class="navbar-toggler"
            type="button"
            data-toggle="collapse"
            data-target="#navbarNav"
            aria-controls="navbarNav"
            aria-expanded="false"
            aria-label="Toggle navigation"
          >
            <span class="navbar-toggler-icon"></span>
          </button>
          <div class="collapse navbar-collapse" id="navbarNav">
            <ul class="navbar-nav">
              <li class="nav-item active">
                <a href="index.php" class="nav-link" >HOME <span class="sr-only">(current)</span></a>
              </li>
              <li class="nav-item">
                <a href="index.php" class="nav-link" > FEATURES </a>
              </li>
              <li class="nav-item">
                <a href="index.php" class="nav-link" > COLLECTION</a>
              </li>
              <li class="nav-item">
                <a href="index.php" class="nav-link" > SHOP</a>
              </li>
              <li class="nav-item">
                <a href="index.php" class="nav-link" > ABOUT US</a>
              </li>
              <li class="nav-item">
                <a href="cart.php" class="nav-link" > Your Cart <b class="badge badge-pill badge-light float-right"> <?php total_items();?></b></a>
              </li>
            </ul>
          </div>
          <div class="navbar-nav">
            <form class="form-inline my-2 my-lg-0" method="get" action="results.php" enctype="multipart/form-data">
                  <input class="form-control mr-sm-2" type="search" placeholder="Search a Product" aria-label="Search" name="user_query"/>
                  <button class="btn btn-outline-info my-2 my-sm-0" type="submit">Search</button>
                  </form>
          </div>
        </nav>
      </div>

        <!-- BreadCrumbs -->
        <div class="container-fluid p-0">
          <nav class="navbar navbar-expand-lg navbar-light bg-white">
            <ul class="breadcrumbs">
              <li class="breadcrumbs__item">
                <a href="#" class="breadcrumbs__link">Home</a>
              </li>
              <li class="breadcrumbs__item">
                <a href="#" class="breadcrumbs__link breadcrumbs__link--active"> Your Cart <br></a>
              </li>
          </nav>
            </ul>
        </div>
      <!-- /BreadCrumbs -->

    </header>

    <!-- /header -->
    
    <!-- Main Section   -->

<!-- ========================= SECTION PAGETOP ========================= -->
<section class="section-pagetop bg">
<div class="container">
	<h2 class="title-page">Shopping cart</h2>
</div> <!-- container //  -->
</section>
<!-- ========================= SECTION INTRO END// ========================= -->


<!-- ========================= SECTION CONTENT ========================= -->
<section class="section-content padding-y">
<div class="container">

<div class="row">
	<main class="col-md-9">
<div class="card">
<form action="cart.php" method="post" enctype="multipart/form-data">
<table class="table table-borderless table-shopping-cart">
<thead class="text-muted">
<tr class="small text-uppercase">
  <th scope="col">Product</th>
  <th scope="col" width="120">Quantity</th>
  <th scope="col" width="120">Price</th>
  <th scope="col" class="text-right" width="200"> </th>
</tr>
</thead>

<?php

$total = 0;
$ip = getIp(); 
$sel_price = "select * from cart where ip_add='$ip'";
$product_quant =['qty'];
$run_price = mysqli_query($con, $sel_price);
$product_quant =['qty'];
while($p_price=mysqli_fetch_array($run_price)){
    $pro_id = $p_price['p_id']; 
    $pro_price = "select * from products where product_id='$pro_id'";

    $run_pro_price = mysqli_query($con,$pro_price); 
    while ($pp_price = mysqli_fetch_array($run_pro_price)){
    $product_price = array($pp_price['product_price']);
    $product_title = $pp_price['product_title'];
    $product_image = $pp_price['product_img1'];
    $productsingle_price = $pp_price['product_price'];
    $values = array_sum($product_price);
    $total +=$values;

    // $fixquantity = '1';

    // foreach($pp_price as $row){
   

?>
<tbody>
<tr>
	<td>
		<figure class="itemside">
			<figcaption class="info">
				<a href="#" class="title text-dark"><?php echo"$product_title"?></a>
				<p class="text-muted small">Size: XL, Color: blue, <br> Brand: Gucci</p>
			</figcaption>
		</figure>
	</td>
	<td> 
		<select class="form-control" name= "qty">
			<option>1</option>
			<option>2</option>	
			<option>3</option>	
			<option>4</option>	
      </select> 
    </td>

	<td> 
		<div class="price-wrap"> 
    <var class="price">RM <?php echo"$productsingle_price"?></var> 
            <small class="text-muted"> RM<?php echo"$productsingle_price"?> each </small> 
		</div>
    </td>

    <td class="text-right"> 
	  <a data-original-title="Save to Wishlist" title="" href="cart.php?removeitem=<?php echo"$pro_id"?>" class="btn btn-light" data-toggle="tooltip"> <i class="fa fa-trash-alt"></i></a> 
    
    <!-- <button type="button" class="btn btn-light updateitembtn" data-button="Hello" >Update Cart</button> -->

    <a type="submit" value="submit" name="submit" href="#my_modal" class="btn btn-light" data-toggle="modal" data-book-id="<?php echo"$pro_id"?>">Updart Cart</a>
    <!-- <button data-toggle="modal" class="btn btn-light" data-target="edit">Update Cart</button> -->

    </td>

</tr>


<?php
}

}

?>  
</tbody>


</table>

<!-- MODAL -->
<div class="modal fade" id="my_modal" tabindex="-1" role="dialog" aria-labelledby="exampleModalLabel" aria-hidden="true">
        <div class="modal-dialog" role="document">
            <div class="modal-content">
                <div class="modal-header">
                    <h5 class="modal-title" id="exampleModalLabel">Update Item</h5>
                    <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                    <span aria-hidden="true">&times;</span>
                    </button>
                </div>
                <div class="modal-body">
                    <form action="" method="POST">
                            <div class="form-group col-md-12">
                                
                              <label>Product Name</label>
                                
                                <input class="form-control" id="producta"  name="producta" value="bookId">
                            </div>

                            <div class="form-group col-md-12">
                                <label>Product Name</label>
                                <input type="text" class="form-control" value="" >
                            </div>

                            <div class="form-group col-md-12">
                                <label>Product Price</label>
                                <input type="text" class="form-control" id="fixquantity" >
                            </div>

                            <div class="form-group col-md-12">
                                <label>Quantity</label>
                                <input type="text" class="form-control" id="productsingle_price">
                            </div>
                      
                    </form>

                    
                </div>
                <div class="modal-footer">
                    <button type="button" class="btn btn-secondary" data-dismiss="modal">Close</button>
                    <button type="button" class="btn btn-primary">Save changes</button>
                </div>
            </div>
        </div>
</div>
  

    <!-- /MODAL -->


<!-- <div class="card-body" align="center">
        <input type="submit" name="update" value="Update Cart" class="btn btn-secondary"/>
    </div> -->

<div class="card-body border-top">

    <!-- <input type="submit" name="update" value="Checkout >>" class="btn btn-primary float-md-right"/>  -->
    <input type="submit" name="continue" value=" << Continue Shopping" class="btn btn-light"> </input>


        <a href="checkout.php" class="btn btn-primary float-md-right"> Checkout <i class="fa fa-chevron-right"></i> </a>
        <!-- <a href="viewproduct.php?cat=1" class="btn btn-light"> <i class="fa fa-chevron-left"></i> Continue shopping </a> -->
</div>	
</form>

</div> <!-- card.// -->

<div class="alert alert-success mt-3">
	<p class="icontext"><i class="icon text-success fa fa-truck"></i> Free Delivery within 1-2 weeks</p>
</div>

	</main> <!-- col.// -->
	<aside class="col-md-3">
		<div class="card mb-3">
			<div class="card-body">
			<form>
				<div class="form-group">
					<label>Have coupon?</label>
					<div class="input-group">
						<input type="text" class="form-control" name="" placeholder="Coupon code">
						<span class="input-group-append"> 
							<button class="btn btn-primary">Apply</button>
						</span>
					</div>
				</div>
			</form>
			</div> <!-- card-body.// -->
		</div>  <!-- card .// -->
		<div class="card">
			<div class="card-body">
					<dl class="dlist-align">
					  <dt>Total price:</dt>
					  <dd class="text-right">RM <?php echo $total ?></dd>
					</dl>
					<dl class="dlist-align">
					  <dt>Discount:</dt>
					  <dd class="text-right">RM 0</dd>
					</dl>
					<dl class="dlist-align">
					  <dt>Total:</dt>
					  <dd class="text-right  h5"><strong>RM <?php echo $total ?> </strong></dd>
					</dl>
					<hr>
					<p class="text-center mb-3">
						<img src="images/misc/payments.png" height="26">
					</p>
					
			</div> <!-- card-body.// -->
		</div>  <!-- card .// -->
	</aside> <!-- col.// -->
</div>

</div> <!-- container .//  -->
</section>
<!-- ========================= SECTION CONTENT END// ========================= -->
    
    <!-- /Main Section   -->


    <canvas class="my-4 w-100" id="myChart" width="900" height="180"></canvas>
   
    <hr> </hr>
    <!-- Footer -->

    <footer>
        <div class="container-fluid px-5">
          <div class="row">
            <div class="col-md-4 col-sm-12">
              <h4>Contact Us</h4>
              <div class="row pl-md-1 text-secondary">
                <div class="col-12">
                  <i class="fa fa-home px-md-2"></i>
                  <small>S0-7-01, Menara 1, No 3, Jalan Bangsar, KL Eco City</small>
                </div>
              </div>
              <div class="row pl-md-1 text-secondary py-4">
                  <div class="col-12">
                      <i class="fa fa-paper-plane px-md-2"></i>
                      <small>www.gogoempire.com</small>
                    </div>
              </div>
               <div class="row pl-md-1 text-secondary">
                  <div class="col-12">
                      <i class="fa fa-phone-volume px-md-2"></i>
                      <small>(+60) 123817908</small>
                    </div>
               </div>

              <div class="row social text-secondary">
                <div class="col-12 py-3">
                  <i class="fab fa-twitter"></i>
                  <i class="fab fa-facebook-f"></i>
                  <i class="fab fa-google-plus-g"></i>
                  <i class="fab fa-skype"></i>
                  <i class="fab fa-pinterest-p"></i>
                  <i class="fab fa-youtube"></i>
                  <i class="fab fa-linkedin-in"></i>
                </div>
              </div>
            </div>
            <div class="col-md-2 col-sm-12">
              <h4>Our Services</h4>
              <div class="d-flex flex-column pl-md-3">
                <small class="pt-0">Kuala Lumpur</small>
                <small>Selangor</small>
                <small>KL Centeral</small>
              </div>
            </div>
            <div class="col-md-2 col-sm-12">
              <h4>Extras</h4>
              <div class="d-flex flex-column pl-md-3">
                  <small class="pt-0">About GogoEmpire</small>
                  <small>Collection</small>
                  <small>Contact Us</small>
                </div>
            </div>
            <div class="col-md-4 follow-us col-sm-12">
              <h4>Follow Instagram</h4>
              <div class="d-flex flex-row">
                <img src="./assets/256_n.jpg" alt="Instagram 1" class="img-fluid">
                <img src="./assets/792_n.jpg" alt="Instagram 2" class="img-fluid">
                <img src="./assets/392_n.jpg" alt="Instagram 3" class="img-fluid">
              </div>
              <div class="d-flex flex-row">
                  <img src="./assets/664_n.jpg" alt="Instagram 1" class="img-fluid">
                  <img src="./assets/088_n.jpg" alt="Instagram 2" class="img-fluid">
                  <img src="./assets/896_n.jpg" alt="Instagram 3" class="img-fluid">
                </div>
            </div>
          </div>
        </div>

        <div class="container-fluid news pt-5">
          <div class="row">
            <div class="col-md-6 col-12 pl-5">
              <h4 class="primary-color font-roboto m-0 p-0">
                Need Help? Call Our Award-Warning
              </h4>
              <p class="m-0 p-0 text-secondary">
                Support Team 24/7 At (+60) 176844365
              </p>
            </div>
            <div class="col-md-4 col-12 my-md-0 my-3 pl-md-0 pl-5">
              <input type="text" class="form-control border-0 bg-light" placeholder="Enter Your email Address">
            </div>
            <div class="col-md-2 col-12 my-md-0 my-3 pl-md-0 pl-5">
              <button class="btn bg-primary-color text-white">Subscribe</button>
            </div>
          </div>
        </div>

        <div class="container text-center">
          <p class="pt-5">
            <img src="./assets/payment.png" alt="payment image" class="img-fluid">
          </p>
          <small class="text-secondary py-4">GogoEmpire © 2020 All Rights Reserved.</small>
        </div>

    </footer>

    <!-- /Footer -->

    <script src="https://cdnjs.cloudflare.com/ajax/libs/jquery/3.5.0/jquery.min.js"></script>
    <script
      src="https://code.jquery.com/jquery-3.3.1.slim.min.js"
      integrity="sha384-q8i/X+965DzO0rT7abK41JStQIAqVgRVzpbzo5smXKp4YfRvH+8abtTE1Pi6jizo"
      crossorigin="anonymous"
    ></script>
    <script
      src="https://cdnjs.cloudflare.com/ajax/libs/popper.js/1.14.7/umd/popper.min.js"
      integrity="sha384-UO2eT0CpHqdSJQ6hJty5KVphtPhzWj9WO1clHTMGa3JDZwrnQq4sF86dIHNDz0W1"
      crossorigin="anonymous"
    ></script>
    <script
      src="https://stackpath.bootstrapcdn.com/bootstrap/4.3.1/js/bootstrap.min.js"
      integrity="sha384-JjSmVgyd0p3pXB1rRibZUAYoIIy6OrQ6VrjIEaFf/nJGzIxFDsf4x0xIM+B07jRM"
      crossorigin="anonymous"
    ></script>
    <script
      type="text/javascript"
      src="//cdn.jsdelivr.net/npm/slick-carousel@1.8.1/slick/slick.min.js"
    ></script>
    <script src="./js/main.js"></script>

   

    

<script>
$('#my_modal').on('show.bs.modal', function(e) {
    var bookId = $(e.relatedTarget).data('book-id');
    $(e.currentTarget).find('input[value="bookId"]').val(bookId);
});
</script>



    
</body>
  
</html>


<?php

if(isset($_GET['removeitem'])){

    $remove_id = $_GET['removeitem'];

    $delete_product = "delete from cart where p_id='$remove_id'";

    $run_delete = mysqli_query($con, $delete_product);

    if($run_delete){
        echo "<script>window.open('cart.php','_self')</script>";
    }else {

    }

}

// function updatecart(){
//     global $con;
//     $ip = getIp();
//     if(isset($_POST['update'])){
//         foreach($_POST['remove'] as $remove_id){
//             $delete_product = "delete from cart where p_id='$remove_id'";
//             $run_delete = mysqli_query($con, $delete_product);
//             if($run_delete){
//                 echo "<script>window.open('cart.php','_self')</script>";
//             }
//         }
//     }
//     if(isset($_POST['continue'])){
//         echo "<script>window.open('viewproduct.php?cat=1','_self')</script>";
//     }
// }
// echo @$up_cart = updatecart();



            // if(isset($_GET['updateitem'])){
                
            //     $updateitemid = $_GET['updateitem'];

            //     $displaydatainmodal = "SELECT * FROM cart WHERE p_id = '$updateitemid'";

            //     // $data = mysqli_query($con, $displaydatainmodal);

            //     // $result = mysqli_fetch_assoc($data);

            //     // $quantity = $result['qty'];

            //     // echo $quantity;

            //     $result = mysqli_query($con, $displaydatainmodal);

            //         if (mysqli_num_rows($result) > 0) {
            //         // output data of each row
            //         while($row = mysqli_fetch_assoc($result)) {
                        
            //             $quantity = $row["qty"];

            //         }
            //         } else {
            //             echo "0 results";
            //         }


                
            // }


        
                
            ?>



