<?php
session_start();
include("includes/db.php");
include("functions/functions.php");


?>
<!DOCTYPE html>
<html lang="en">
<head>

<!-- Basic page needs
============================================ -->
<!-- <title>GOGO EMPIRE STORE</title>
<meta charset="utf-8">
<meta name="keywords"
    content="Gogo Empire Sdn Bhd" />
<meta name="description"
    content="Gogo Empire Sdn Bhd" />
<meta name="author" content="Gogo Empire">
<meta name="robots" content="index, follow" /> -->
<title>Swift Commerce</title>
<!-- Mobile specific metas
============================================ -->
<meta name="viewport" content="width=device-width, initial-scale=1.0, maximum-scale=1.0, user-scalable=no">

<!-- Favicon
============================================ -->

<link rel="shortcut icon" type="image/png" href="ico/favicon-16x16.png" />


<!-- Libs CSS
============================================ -->
<link rel="stylesheet" href="css/bootstrap/css/bootstrap.min.css">
<link href="css/font-awesome/css/font-awesome.min.css" rel="stylesheet">
<link href="js/datetimepicker/bootstrap-datetimepicker.min.css" rel="stylesheet">
<link href="js/owl-carousel/owl.carousel.css" rel="stylesheet">
<link href="css/themecss/lib.css" rel="stylesheet">
<link href="js/jquery-ui/jquery-ui.min.css" rel="stylesheet">
<link href="js/minicolors/miniColors.css" rel="stylesheet">

<!-- Theme CSS
============================================ -->
<link href="css/themecss/so_searchpro.css" rel="stylesheet">
<link href="css/themecss/so_megamenu.css" rel="stylesheet">
<link href="css/themecss/so-categories.css" rel="stylesheet">
<link href="css/themecss/so-listing-tabs.css" rel="stylesheet">
<link href="css/themecss/so-newletter-popup.css" rel="stylesheet">

<link href="css/footer/footer1.css" rel="stylesheet">
<link href="css/header/header1.css" rel="stylesheet">
<link id="color_scheme" href="css/theme.css" rel="stylesheet">
<link href="css/responsive.css" rel="stylesheet">

<!-- Google web fonts
============================================ -->
<link href='https://fonts.googleapis.com/css?family=Roboto:300,400,400i,500,500i,700' rel='stylesheet'
    type='text/css'>
<style type="text/css">
    body {
        font-family: 'Roboto', sans-serif
    }
</style>

</head>

  <body>
    

      <!-- Header Container  -->
      <header id="header" class=" typeheader-1">
            <!-- Header Top -->
            <div class="header-top hidden-compact">
                <div class="container">
                    <div class="row">
                        <div class="header-top-left col-lg-7 col-md-8 col-sm-6 col-xs-4">
                            <div class="hidden-sm hidden-xs welcome-msg"><b>Welcome to GogoFood Store !</b> ! Wrap new offers /
                                gift every single day on Weekends </div>
                            <ul class="top-link list-inline hidden-lg hidden-md">
                                <li class="account" id="my_account">
                                    <?php
                                    if(!isset($_SESSION['customer_email'])){
                                       ?>
                                        <ul class="dropdown-menu ">
                                                <li><a href="register.php"><i class="fa fa-user"></i> Register</a></li>
                                                <li><a href="login.php"><i class="fa fa-pencil-square-o"></i> Login</a></li>
                                            </ul>
                                       <?php
                                    }else{
                                       ?>
                                            <?php
                                            
                                            $customer_email = $_SESSION['customer_email'];
                                    
                                            $get_customer_details = "Select * from customers where customer_email='$customer_email'";
                                            $run_customer_details = mysqli_query($con, $get_customer_details);
                                            $fetch_customer_details = mysqli_fetch_array($run_customer_details);
                                                                                                
                                            $customer_id = $fetch_customer_details['customer_id']; //:: Customer id 
                                            $customer_name = $fetch_customer_details['customer_name']; //:: customer name

                                            ?>
                                             <?php echo" 
                                               $customer_name"?>
                                             <a href="#" title="My Account " class="btn-xs dropdown-toggle"
                                            data-toggle="dropdown"> <span class="hidden-xs">My Account </span> <span
                                            class="fa fa-caret-down"></span>
                                            </a>
                                            <ul class="dropdown-menu ">
                                                <li><a href="food.php"><i class="fa fa-user"></i> <?php echo" 
                                                $_SESSION[customer_email]"?></a></li>
                                                <li><a href="logout.php"><i class="fa fa-pencil-square-o"></i> Logout</a></li>
                                                
                                            </ul>

                                       <?php
                                    }

                                    ?>
                                    
                                    <!-- <ul class="dropdown-menu ">
                                        <li><a href="register.html"><i class="fa fa-user"></i> Register</a></li>
                                        <li><a href="login.html"><i class="fa fa-pencil-square-o"></i> Login</a></li>
                                    </ul> -->
                                </li>
                            </ul>
                        </div>
                        <div class="header-top-right collapsed-block col-lg-5 col-md-4 col-sm-6 col-xs-8">
                            <ul class="top-link list-inline lang-curr">
                                <li class="language">
                                    <div class="btn-group languages-block ">
                                        <form action="http://demo.smartaddons.com/templates/html/emarket/index.html"
                                            method="post" enctype="multipart/form-data" id="bt-language">
                                            <a class="btn btn-link dropdown-toggle" data-toggle="dropdown">
                                                <span class="">English</span>
                                                <span class="fa fa-angle-down"></span>
                                            </a>
                                            <ul class="dropdown-menu">
                                                <li><a href="index.html"> English </a></li>
                                                <li> <a href="index.html">  Bhasa Malayu </a> </li>
                                            </ul>
                                        </form>
                                    </div>

                                </li>
                            </ul>



                        </div>
                    </div>
                </div>
            </div>
            <!-- //Header Top -->

            <!-- Header center -->
            <div class="header-middle">
                <div class="container">
                    <div class="row">
                        <!-- Logo -->
                        <div class="navbar-logo col-lg-2 col-md-2 col-sm-12 col-xs-12">
                            <div class="logo"><a href="index.html">
								<!--<h1 style="color: white;"> GOGO EMPIRE</h1>-->
                                <img src="image/catalog/CBMC-logo-800x132.png" title="Your Store"
                                        alt="Your Store" />
                                </a></div>
                        </div>
                        <!-- //end Logo -->

                        <!-- Main menu -->
                        <div class="main-menu col-lg-6 col-md-7 ">
                            <div class="responsive so-megamenu megamenu-style-dev">
                                <nav class="navbar-default">
                                    <div class=" container-megamenu  horizontal open ">
                                        <div class="navbar-header">
                                            <button type="button" id="show-megamenu" data-toggle="collapse"
                                                class="navbar-toggle">
                                                <span class="icon-bar"></span>
                                                <span class="icon-bar"></span>
                                                <span class="icon-bar"></span>
                                            </button>
                                        </div>

                                        <div class="megamenu-wrapper">
                                            <span id="remove-megamenu" class="fa fa-times"></span>
                                            <div class="megamenu-pattern">
                                                <div class="container-mega">
                                                <ul class="megamenu" 
                                                        data-animationtime="250">
                                                        <li class="home hover">
                                                            <a href="index.php">HOME </a>
                                                        </li>
                                                        <li class="home hover">
                                                        <a href="hotels.php">STORES </a>
                                                        </li>
                                                        <li class="home hover">
                                                            <a href="">COLLECTION </a>
                                                        </li>
                                                        <li class="home hover">
                                                            <a href="aboutus.php">ABOUT US </a>
                                                        </li>
                                                        <li class="home hover">
                                                            <a href="contact.php">CONTACT US </a>
                                                        </li>
                                                    </ul>
                                                </div>
                                            </div>
                                        </div>
                                    </div>
                                </nav>
                            </div>
                        </div>
                        <!-- //end Main menu -->

                        <div class="middle-right col-lg-4 col-md-3 col-sm-6 col-xs-8">
                            <div class="signin-w hidden-sm hidden-xs">
                                <ul class="signin-link blank">
                                <?php 
                                if(!isset($_SESSION['customer_email'])){
                                    echo"";
                                }else{
                                    echo" <li class='log login'> 
                                    <a class='link-lg'>$_SESSION[customer_email]</a>
                                 </li>";
                                }
                                ?>
                                </ul>
                            </div>
                            <div class="signin-w hidden-sm hidden-xs">
                                <ul class="signin-link blank">
                                <?php
                            if(!isset($_SESSION['customer_email'])){
                                echo" <li class='log login'></i> 
                                <a class='link-lg' href='checkout.php'>Login</a>
                             </li>";
                             echo" <li class='log login'></i> 
                             <a class='link-lg' href='checkout.php'>|</a>
                          </li>";
                             echo" <li class='log login'></i> 
                             <a class='link-lg' href='login.html'>Create an Account</a>
                          </li>";
                            }else{
                                echo"<li class='log login'></i> 
                                <a class='link-lg' href='logout.php'>Logout</a>
                             </li>";
                            }
                            ?>
                                </ul>
                            </div>

                        </div>

                    </div>

                </div>
            </div>
            <!-- //Header center -->

            <!-- Header Bottom -->
            <div class="header-bottom hidden-compact">
                <div class="container">
                    <div class="row">

                        <div class="bottom1 menu-vertical col-lg-2 col-md-3 col-sm-3">
                            <div class="responsive so-megamenu megamenu-style-dev ">
                                <div class="so-vertical-menu ">
                                    <nav class="navbar-default">

                                        <div class="container-megamenu vertical">
                                            <div id="menuHeading">
                                                <div class="megamenuToogle-wrapper">
                                                    <div class="megamenuToogle-pattern">
                                                        <div class="container">
                                                            <div>
                                                                <span></span>
                                                                <span></span>
                                                                <span></span>
                                                            </div>
                                                            All Categories
                                                        </div>
                                                    </div>
                                                </div>
                                            </div>

                                            <div class="navbar-header">
                                                <button type="button" id="show-verticalmenu" data-toggle="collapse"
                                                    class="navbar-toggle">
                                                    <i class="fa fa-bars"></i>
                                                    <span> All Categories </span>
                                                </button>
                                            </div>
                                            <div class="vertical-wrapper">
                                                <span id="remove-verticalmenu" class="fa fa-times"></span>
                                                <div class="megamenu-pattern">
                                                    <div class="container-mega">
                                                        <ul class="megamenu">
                                                            
                                                            
                                                             <li class="item-vertical">
                                                                <p class="close-menu"></p>
                                                                <a href="" class="clearfix">
                                                                    <span>FOOD</span>

                                                                </a>
                                                            </li>
                                                            <!-- <li class="item-vertical  style1 with-sub-menu hover">
                                                                <p class="close-menu"></p>
                                                                <a href="#" class="clearfix">
                                                                    <span class="label"></span>
                                                                    <img src="image/catalog/menu/icons/ico9.png"
                                                                        alt="icon">
                                                                    <span>Electronic</span>

                                                                    <b class="caret"></b>
                                                                </a>
                                                                <div class="sub-menu" data-subwidth="40">
                                                                    <div class="content">
                                                                        <div class="row">
                                                                            <div class="col-md-6">
                                                                                <div class="row">
                                                                                    <div class="col-md-12 static-menu">
                                                                                        <div class="menu">
                                                                                            <ul>
                                                                                                <li><a href="#"
                                                                                                        class="main-menu">Smartphone</a>
                                                                                                    <ul>
                                                                                                        <li><a
                                                                                                                href="#">Esdipiscing</a>
                                                                                                        </li>
                                                                                                        <li><a
                                                                                                                href="#">Scanners</a>
                                                                                                        </li>
                                                                                                        <li><a
                                                                                                                href="#">Apple</a>
                                                                                                        </li>
                                                                                                        <li><a
                                                                                                                href="#">Dell</a>
                                                                                                        </li>
                                                                                                        <li><a
                                                                                                                href="#">Scanners</a>
                                                                                                        </li>
                                                                                                    </ul>
                                                                                                </li>
                                                                                                <li><a href="#"
                                                                                                        class="main-menu">Electronics</a>
                                                                                                    <ul>
                                                                                                        <li><a
                                                                                                                href="#">Asdipiscing</a>
                                                                                                        </li>
                                                                                                        <li><a href="#">Diam
                                                                                                                sit</a>
                                                                                                        </li>
                                                                                                        <li><a href="#">Labore
                                                                                                                et</a>
                                                                                                        </li>
                                                                                                        <li><a
                                                                                                                href="#">Monitors</a>
                                                                                                        </li>
                                                                                                    </ul>
                                                                                                </li>
                                                                                            </ul>
                                                                                        </div>
                                                                                    </div>
                                                                                </div>
                                                                            </div>
                                                                            <div class="col-md-6">
                                                                                <div class="row banner">
                                                                                    <a href="#">
                                                                                        <img src="image/catalog/menu/megabanner/vbanner1.jpg"
                                                                                            alt="banner1">
                                                                                    </a>
                                                                                </div>
                                                                            </div>
                                                                        </div>
                                                                    </div>
                                                                </div>
                                                            </li>
                                                            <li class="item-vertical with-sub-menu hover">
                                                                <p class="close-menu"></p>
                                                                <a href="#" class="clearfix">
                                                                    <img src="image/catalog/menu/icons/ico7.png"
                                                                        alt="icon">
                                                                    <span>Health &amp; Beauty</span>
                                                                    <b class="caret"></b>
                                                                </a>
                                                                <div class="sub-menu" data-subwidth="60">
                                                                    <div class="content">
                                                                        <div class="row">
                                                                            <div class="col-md-12">
                                                                                <div class="row">
                                                                                    <div class="col-md-4 static-menu">
                                                                                        <div class="menu">
                                                                                            <ul>
                                                                                                <li>
                                                                                                    <a href="#"
                                                                                                        class="main-menu">Car
                                                                                                        Alarms and
                                                                                                        Security</a>
                                                                                                    <ul>
                                                                                                        <li><a href="#">Car
                                                                                                                Audio
                                                                                                                &amp;
                                                                                                                Speakers</a>
                                                                                                        </li>
                                                                                                        <li><a href="#">Gadgets
                                                                                                                &amp;
                                                                                                                Auto
                                                                                                                Parts</a>
                                                                                                        </li>
                                                                                                        <li><a href="#">Gadgets
                                                                                                                &amp;
                                                                                                                Auto
                                                                                                                Parts</a>
                                                                                                        </li>
                                                                                                        <li><a href="#">Headphones,
                                                                                                                Headsets</a>
                                                                                                        </li>
                                                                                                    </ul>
                                                                                                </li>
                                                                                                <li>
                                                                                                    <a href="24.html"
                                                                                                        onclick="window.location = '24.html';"
                                                                                                        class="main-menu">Health
                                                                                                        &amp; Beauty</a>
                                                                                                    <ul>
                                                                                                        <li>
                                                                                                            <a href="#">Home
                                                                                                                Audio</a>
                                                                                                        </li>
                                                                                                        <li>
                                                                                                            <a href="#">Helicopters
                                                                                                                &amp;
                                                                                                                Parts</a>
                                                                                                        </li>
                                                                                                        <li>
                                                                                                            <a href="#">Outdoor
                                                                                                                &amp;
                                                                                                                Traveling</a>
                                                                                                        </li>
                                                                                                        <li>
                                                                                                            <a href="#">Toys
                                                                                                                &amp;
                                                                                                                Hobbies</a>
                                                                                                        </li>
                                                                                                    </ul>
                                                                                                </li>
                                                                                            </ul>
                                                                                        </div>
                                                                                    </div>
                                                                                    <div class="col-md-4 static-menu">
                                                                                        <div class="menu">
                                                                                            <ul>
                                                                                                <li>
                                                                                                    <a href="#"
                                                                                                        class="main-menu">Electronics</a>
                                                                                                    <ul>
                                                                                                        <li>
                                                                                                            <a
                                                                                                                href="#">Earings</a>
                                                                                                        </li>
                                                                                                        <li>
                                                                                                            <a href="#">Salon
                                                                                                                &amp;
                                                                                                                Spa
                                                                                                                Equipment</a>
                                                                                                        </li>
                                                                                                        <li>
                                                                                                            <a href="#">Shaving
                                                                                                                &amp;
                                                                                                                Hair
                                                                                                                Removal</a>
                                                                                                        </li>
                                                                                                        <li>
                                                                                                            <a href="#">Smartphone
                                                                                                                &amp;
                                                                                                                Tablets</a>
                                                                                                        </li>
                                                                                                    </ul>
                                                                                                </li>
                                                                                                <li>
                                                                                                    <a href="#"
                                                                                                        class="main-menu">Sports
                                                                                                        &amp;
                                                                                                        Outdoors</a>
                                                                                                    <ul>
                                                                                                        <li>
                                                                                                            <a href="#">Flashlights
                                                                                                                &amp;
                                                                                                                Lamps</a>
                                                                                                        </li>
                                                                                                        <li>
                                                                                                            <a
                                                                                                                href="#">Fragrances</a>
                                                                                                        </li>
                                                                                                        <li>
                                                                                                            <a
                                                                                                                href="#">Fishing</a>
                                                                                                        </li>
                                                                                                        <li>
                                                                                                            <a href="#">FPV
                                                                                                                System
                                                                                                                &amp;
                                                                                                                Parts</a>
                                                                                                        </li>
                                                                                                    </ul>
                                                                                                </li>
                                                                                            </ul>
                                                                                        </div>
                                                                                    </div>
                                                                                    <div class="col-md-4 static-menu">
                                                                                        <div class="menu">
                                                                                            <ul>
                                                                                                <li>
                                                                                                    <a href="#"
                                                                                                        class="main-menu">More
                                                                                                        Car
                                                                                                        Accessories</a>
                                                                                                    <ul>
                                                                                                        <li>
                                                                                                            <a href="#">Lighter
                                                                                                                &amp;
                                                                                                                Cigar
                                                                                                                Supplies</a>
                                                                                                        </li>
                                                                                                        <li>
                                                                                                            <a href="#">Mp3
                                                                                                                Players
                                                                                                                &amp;
                                                                                                                Accessories</a>
                                                                                                        </li>
                                                                                                        <li>
                                                                                                            <a href="#">Men
                                                                                                                Watches</a>
                                                                                                        </li>
                                                                                                        <li>
                                                                                                            <a href="#">Mobile
                                                                                                                Accessories</a>
                                                                                                        </li>
                                                                                                    </ul>
                                                                                                </li>
                                                                                                <li>
                                                                                                    <a href="#"
                                                                                                        class="main-menu">Gadgets
                                                                                                        &amp; Auto
                                                                                                        Parts</a>
                                                                                                    <ul>
                                                                                                        <li>
                                                                                                            <a href="#">Gift
                                                                                                                &amp;
                                                                                                                Lifestyle
                                                                                                                Gadgets</a>
                                                                                                        </li>
                                                                                                        <li>
                                                                                                            <a href="#">Gift
                                                                                                                for
                                                                                                                Man</a>
                                                                                                        </li>
                                                                                                        <li>
                                                                                                            <a href="#">Gift
                                                                                                                for
                                                                                                                Woman</a>
                                                                                                        </li>
                                                                                                        <li>
                                                                                                            <a href="#">Gift
                                                                                                                for
                                                                                                                Woman</a>
                                                                                                        </li>
                                                                                                    </ul>
                                                                                                </li>
                                                                                            </ul>
                                                                                        </div>
                                                                                    </div>
                                                                                </div>
                                                                            </div>
                                                                        </div>
                                                                    </div>
                                                                </div>
                                                            </li>
                                                            <li class="item-vertical css-menu with-sub-menu hover">
                                                                <p class="close-menu"></p>
                                                                <a href="#" class="clearfix">

                                                                    <img src="image/catalog/menu/icons/ico6.png"
                                                                        alt="icon">
                                                                    <span>Smartphone &amp; Tablets</span>
                                                                    <b class="caret"></b>
                                                                </a>
                                                                <div class="sub-menu" data-subwidth="20">
                                                                    <div class="content">
                                                                        <div class="row">
                                                                            <div class="col-sm-12">
                                                                                <div class="row">
                                                                                    <div class="col-sm-12 hover-menu">
                                                                                        <div class="menu">
                                                                                            <ul>
                                                                                                <li>
                                                                                                    <a href="#"
                                                                                                        class="main-menu">Headphones,
                                                                                                        Headsets</a>
                                                                                                </li>
                                                                                                <li>
                                                                                                    <a href="#"
                                                                                                        class="main-menu">Home
                                                                                                        Audio</a>
                                                                                                </li>
                                                                                                <li>
                                                                                                    <a href="#"
                                                                                                        class="main-menu">Health
                                                                                                        &amp; Beauty</a>
                                                                                                </li>
                                                                                                <li>
                                                                                                    <a href="#"
                                                                                                        class="main-menu">Helicopters
                                                                                                        &amp; Parts</a>
                                                                                                </li>
                                                                                                <li>
                                                                                                    <a href="#"
                                                                                                        class="main-menu">Helicopters
                                                                                                        &amp; Parts</a>
                                                                                                </li>
                                                                                            </ul>
                                                                                        </div>
                                                                                    </div>
                                                                                </div>
                                                                            </div>
                                                                        </div>
                                                                    </div>
                                                                </div>
                                                            </li>
                                                            <li class="item-vertical">
                                                                <p class="close-menu"></p>
                                                                <a href="#" class="clearfix">
                                                                    <img src="image/catalog/menu/icons/ico5.png"
                                                                        alt="icon">
                                                                    <span>Health & Beauty</span>

                                                                </a>
                                                            </li>
                                                            <li class="item-vertical">
                                                                <p class="close-menu"></p>
                                                                <a href="#" class="clearfix">
                                                                    <img src="image/catalog/menu/icons/ico4.png"
                                                                        alt="icon">
                                                                    <span>Bathroom</span>
                                                                </a>
                                                            </li>
                                                            <li class="item-vertical">
                                                                <p class="close-menu"></p>
                                                                <a href="#" class="clearfix">
                                                                    <img src="image/catalog/menu/icons/ico3.png"
                                                                        alt="icon">
                                                                    <span>Metallurgy</span>
                                                                </a>
                                                            </li>
                                                            <li class="item-vertical">
                                                                <p class="close-menu"></p>
                                                                <a href="#" class="clearfix">
                                                                    <img src="image/catalog/menu/icons/ico2.png"
                                                                        alt="icon">
                                                                    <span>Bedroom</span>
                                                                </a>
                                                            </li>
                                                            <li class="item-vertical">
                                                                <p class="close-menu"></p>

                                                                <a href="#" class="clearfix">
                                                                    <img src="image/catalog/menu/icons/ico1.png"
                                                                        alt="icon">
                                                                    <span>Health &amp; Beauty</span>
                                                                </a>
                                                            </li>
                                                            <li class="item-vertical" style="display: none;">
                                                                <p class="close-menu"></p>
                                                                <a href="#" class="clearfix">
                                                                    <img src="image/catalog/menu/icons/ico11.png"
                                                                        alt="icon">
                                                                    <span>Toys &amp; Hobbies </span>
                                                                </a>
                                                            </li>
                                                            <li class="item-vertical" style="display: none;">
                                                                <p class="close-menu"></p>
                                                                <a href="#" class="clearfix">
                                                                    <img src="image/catalog/menu/icons/ico12.png"
                                                                        alt="icon">
                                                                    <span>Jewelry &amp; Watches</span>
                                                                </a>
                                                            </li>
                                                            <li class="item-vertical" style="display: none;">
                                                                <p class="close-menu"></p>
                                                                <a href="#" class="clearfix">
                                                                    <img src="image/catalog/menu/icons/ico9.png"
                                                                        alt="icon">
                                                                    <span>Home &amp; Lights</span>
                                                                </a>
                                                            </li>
                                                            <li class="item-vertical" style="display: none;">
                                                                <p class="close-menu"></p>
                                                                <a href="#" class="clearfix">
                                                                    <img src="image/catalog/menu/icons/ico6.png"
                                                                        alt="icon">
                                                                    <span>Metallurgy</span>
                                                                </a>
                                                            </li>

                                                            <li class="loadmore">
                                                                <i class="fa fa-plus-square-o"></i>
                                                                <span class="more-view">More Categories</span>
                                                            </li> -->

                                                        </ul>
                                                    </div>
                                                </div>
                                            </div>
                                        </div>
                                    </nav>
                                </div>
                            </div>

                        </div>

                        <!-- Search -->
                        <div class="bottom2 col-lg-7 col-md-6 col-sm-6">
                            <div class="search-header-w">
                                <div class="icon-search hidden-lg hidden-md hidden-sm"><i class="fa fa-search"></i>
                                </div>

                                <div id="sosearchpro" class="sosearchpro-wrapper so-search ">
                                    <form method="GET"
                                        action="food.php">
                                        <div id="search0" class="search input-group form-group">
                                            <div class="select_category filter_type  icon-select hidden-sm hidden-xs">
                                                <select class="no-border" name="category_id">
                                                    <option value="0">All Categories</option>
                                                    <option value="78">FOOD</option>
                                                    <!-- <option value="77">Cables &amp; Connectors</option>
                                                    <option value="82">Cameras &amp; Photo</option>
                                                    <option value="80">Flashlights &amp; Lamps</option>
                                                    <option value="81">Mobile Accessories</option>
                                                    <option value="79">Video Games</option>
                                                    <option value="20">Jewelry &amp; Watches</option>
                                                    <option value="76">&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;Earings
                                                    </option>
                                                    <option value="26">&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;Wedding Rings
                                                    </option>
                                                    <option value="27">&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;Men Watches
                                                    </option> -->
                                                </select>
                                            </div>

                                            <input class="autosearch-input form-control" type="text" value="" size="50"
                                                autocomplete="off" placeholder="Keyword here..." name="search">
                                            <span class="input-group-btn">
                                                <button type="submit" class="button-search btn btn-primary"
                                                    name=""><i class="fa fa-search"></i></button>
                                            </span>
                                        </div>
                                        <input type="hidden" name="route" value="product/search" />
                                    </form>
                                </div>
                            </div>
                        </div>
                        <!-- //end Search -->

                        <!-- Secondary menu -->
                        <div class="bottom3 col-lg-3 col-md-3 col-sm-3">


                          <!--cart-->
                       

                    </div>
                </div>

            </div>
        </header>
        <!-- //Header Container  -->

    <!-- /header -->

    <!-- Main Section   -->
    <?php 

        if(!isset($_SESSION['customer_email'])){

            include("login.php");

        }else{
            // include("payment_options.php");
            include("ordersummary.php");
        }


        ?>

    <!-- /Main Section   -->

   
    <hr> </hr>
   
     <!-- Footer Container -->
     <footer class="footer-container typefooter-1">

<hr>

<div class="footer-middle ">
    <div class="container">
        <div class="row">
            <div class="col-lg-3 col-md-3 col-sm-12 col-xs-12 col-style">
                <div class="infos-footer">
                    <h3 class="modtitle">Contact Us</h3>
                    <ul class="menu">
                        <li class="adres">
                            Swift Commerce Sdn Bhd Lot 23 , Lebuh Sultan Mohamed 1 42000 Klang
                        </li>
                        <li class="phone">
                            Tel: 03-3169 6700 / Fax: 03-3176 1271
                        </li>
                        <li class="mail">
                            askme@swiftcommerce.my 
                        </li>
                        <li class="time">
                            Open time: 07:00AM - 5:30PM
                        </li>
                    </ul>
                </div>


            </div>
            <div class="col-lg-2 col-md-2 col-sm-4 col-xs-12 col-style">
                <div class="box-information box-footer">
                    <div class="module clearfix">
                        <h3 class="modtitle">Information</h3>
                        <div class="modcontent">
                            <ul class="menu">
                                <li><a href="aboutus.php">ABOUT US </a></li>
                                <li><a href="#">FAQ</a></li>
                                <li><a href="#">Warranty And Services</a></li>
                                <li><a href="#">Support 24/7 page</a></li>
                                <li><a href="#">Product Registration</a></li>
                                <li><a href="#">Product Support</a></li>
                            </ul>
                        </div>
                    </div>
                </div>

            </div>
            <div class="col-lg-2 col-md-2 col-sm-4 col-xs-12 col-style">
                <div class="box-account box-footer">
                    <div class="module clearfix">
                        <h3 class="modtitle">Extras</h3>
                        <div class="modcontent">
                            <ul class="menu">
                                <li> <a href="aboutus.php"> Gogo Empire </a></li>
                                <li><a href="#">Collection</a></li>
                                <li><a href="contact.php">Contact Us</a></li>
                            </ul>
                        </div>
                    </div>
                </div>


            </div>
            <div class="col-lg-2 col-md-2 col-sm-4 col-xs-12 col-style">
                <div class="box-service box-footer">
                    <div class="module clearfix">
                        <h3 class="modtitle">Services</h3>
                        <div class="modcontent">
                            <ul class="menu">
                                <li><a href="#">Kuala Lumpur</a></li>
                                <li><a href="#">Selangor</a></li>
                                <li><a href="#">KL Central</a></li>
                            </ul>
                        </div>
                    </div>
                </div>

            </div>
            <div class="col-lg-3 col-md-3 col-sm-12 col-xs-12 col-style">


            </div>

            <div class="col-lg-12 col-xs-12 text-center">
                <img src="image/catalog/demo/payment/payment.png" alt="imgpayment">
            </div>
        </div>
    </div>
</div>

<!-- Footer Bottom Container -->
<div class="footer-bottom ">
    <div class="container">
        <div class="copyright">
            gogoempire © 2020. All Rights Reserved. 
        </div>
    </div>
</div>
<!-- /Footer Bottom Container -->


<!--Back To Top-->
<div class="back-to-top"><i class="fa fa-angle-up"></i></div>
</footer>
<!-- //end Footer Container -->


    <script
      src="https://code.jquery.com/jquery-3.3.1.slim.min.js"
      integrity="sha384-q8i/X+965DzO0rT7abK41JStQIAqVgRVzpbzo5smXKp4YfRvH+8abtTE1Pi6jizo"
      crossorigin="anonymous"
    ></script>
    <script
      src="https://cdnjs.cloudflare.com/ajax/libs/popper.js/1.14.7/umd/popper.min.js"
      integrity="sha384-UO2eT0CpHqdSJQ6hJty5KVphtPhzWj9WO1clHTMGa3JDZwrnQq4sF86dIHNDz0W1"
      crossorigin="anonymous"
    ></script>
    <script
      src="https://stackpath.bootstrapcdn.com/bootstrap/4.3.1/js/bootstrap.min.js"
      integrity="sha384-JjSmVgyd0p3pXB1rRibZUAYoIIy6OrQ6VrjIEaFf/nJGzIxFDsf4x0xIM+B07jRM"
      crossorigin="anonymous"
    ></script>
    <script
      type="text/javascript"
      src="//cdn.jsdelivr.net/npm/slick-carousel@1.8.1/slick/slick.min.js"
    ></script>
    <script src="./js/main.js"></script>
  </body>



<script src="https://code.jquery.com/jquery-3.4.1.slim.min.js" integrity="sha384-J6qa4849blE2+poT4WnyKhv5vZF5SrPo0iEjwBvKU7imGFAV0wwj1yYfoRSJoZ+n" crossorigin="anonymous"></script>
      <script>window.jQuery || document.write('<script src="/docs/4.4/assets/js/vendor/jquery.slim.min.js"><\/script>')</script><script src="/docs/4.4/dist/js/bootstrap.bundle.min.js" integrity="sha384-6khuMg9gaYr5AxOqhkVIODVIvm9ynTT5J4V1cfthmT+emCG6yVmEZsRHdxlotUnm" crossorigin="anonymous"></script>
        <script src="https://cdnjs.cloudflare.com/ajax/libs/feather-icons/4.9.0/feather.min.js"></script>
        <script src="https://cdnjs.cloudflare.com/ajax/libs/Chart.js/2.7.3/Chart.min.js"></script>
        <script src="dashboard.js"></script></body>
  
</html>