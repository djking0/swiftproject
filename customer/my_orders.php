<?php
include ("includes/db.php");


$c = $_SESSION ['customer_email'];

$get_c = "select * from customers where customer_email = '$c'";

$run_c = mysqli_query($con , $get_c);

$row_c = mysqli_fetch_array($run_c);

$customer_id = $row_c['customer_id'];
?>

<div class="col-sm-9">
                <h2>Your orders</h2>
                <div class="cart-page">
                    <form method="post">
                        <table class="table">
                            <thead>
                                <tr>
                                    <th>Order No  </th>
                                    <th>Due Amount</th>
                                    <th>Invoice No</th>
                                    <th>Total Products</th>
                                    <th>Order Date</th>
                                    <th>Paid/Unpaid</th>
                                    <th>Status </th>

                                </tr>
                            </thead>

                            <?php
                            
                            $get_orders = "select * from customer_orders where customer_id = '$customer_id'";
                            $run_orders = mysqli_query($con, $get_orders);

                            $i = 0;
                            while($row_orders=mysqli_fetch_array($run_orders)){

                                $order_id = $row_orders['order_id'];
                                $due_amout = $row_orders['due_amount'];
                                $invoice_no = $row_orders['invoice_no'];
                                $products = $row_orders['total_product'];
                                $date = $row_orders['order_date'];
                                $status = $row_orders['order_status'];

                                $i++;

                                if($status=='Pending'){
                                    $status = 'Unpaid';
                                }else{
                                    $status = 'Paid';
                                }
                            
                            
                            ?>

                            <tbody>

                            <tr>
                                <td><?php echo "$i" ?></td>
                                <td><?php echo "$due_amout" ?></td>
                                <td><?php echo "$invoice_no" ?></td>
                                <td><?php echo "$products" ?></td>
                                <td><?php echo "$date" ?></td>
                                <td><?php echo "$status" ?></td>
                                <td><a href='confirm.php?order_id=<?php echo "$order_id" ?>'>Go for Payment</td>
                                
                            </tr>

                             
                                </tbody>

                            <?php
                            } // While loop end
                            ?>

                        </table>
                    </form>
                </div> <!--End Cart page-->

</div>
