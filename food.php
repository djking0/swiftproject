<?php
session_start();
include("includes/db.php");
include("functions/functions.php");

?>
<!DOCTYPE html>
<html lang="en">

<!--  demo.smartaddons.com/templates/html/emarket/category.html by AkrAm, Sat, 20 Apr 2019 19:59:32 GMT -->

<head>

    <!-- Basic page needs
    ============================================ -->
    <!-- <title>GOGO EMPIRE STORE</title>
    <meta charset="utf-8">
    <meta name="keywords"
        content="Gogo Empire Sdn Bhd" />
    <meta name="description"
        content="Gogo Empire Sdn Bhd" />
    <meta name="author" content="Gogo Empire">
    <meta name="robots" content="index, follow" /> -->
	<title>Swift Commerce</title>
    <!-- Mobile specific metas
    ============================================ -->
    <meta name="viewport" content="width=device-width, initial-scale=1.0, maximum-scale=1.0, user-scalable=no">

    <!-- Favicon
    ============================================ -->

    <link rel="shortcut icon" type="image/png" href="ico/favicon-16x16.png" />

     <!--  jQuery -->
     <script type="text/javascript" src="https://code.jquery.com/jquery-1.11.3.min.js"></script>

<!-- Bootstrap Date-Picker Plugin -->
<script type="text/javascript" src="https://cdnjs.cloudflare.com/ajax/libs/bootstrap-datepicker/1.4.1/js/bootstrap-datepicker.min.js"></script>
<link rel="stylesheet" href="https://stackpath.bootstrapcdn.com/bootstrap/3.4.1/css/bootstrap.min.css" integrity="sha384-HSMxcRTRxnN+Bdg0JdbxYKrThecOKuH5zCYotlSAcp1+c8xmyTe9GYg1l9a69psu" crossorigin="anonymous">




    <!-- Libs CSS
    ============================================ -->
    <link rel="stylesheet" href="css/bootstrap/css/bootstrap.min.css">
    <link href="css/font-awesome/css/font-awesome.min.css" rel="stylesheet">
    <link href="js/datetimepicker/bootstrap-datetimepicker.min.css" rel="stylesheet">
    <link href="js/owl-carousel/owl.carousel.css" rel="stylesheet">
    <link href="css/themecss/lib.css" rel="stylesheet">
    <link href="js/jquery-ui/jquery-ui.min.css" rel="stylesheet">
    <link href="js/minicolors/miniColors.css" rel="stylesheet">

    <!-- Theme CSS
    ============================================ -->
    <link href="css/themecss/so_searchpro.css" rel="stylesheet">
    <link href="css/themecss/so_megamenu.css" rel="stylesheet">
    <link href="css/themecss/so-categories.css" rel="stylesheet">
    <link href="css/themecss/so-listing-tabs.css" rel="stylesheet">
    <link href="css/themecss/so-newletter-popup.css" rel="stylesheet">

    <link href="css/footer/footer1.css" rel="stylesheet">
    <link href="css/header/header1.css" rel="stylesheet">
    <link id="color_scheme" href="css/theme.css" rel="stylesheet">
    <link href="css/responsive.css" rel="stylesheet">

    <!-- Google web fonts
    ============================================ -->
    <link href='https://fonts.googleapis.com/css?family=Roboto:300,400,400i,500,500i,700' rel='stylesheet'
        type='text/css'>
    <style type="text/css">
        body {
            font-family: 'Roboto', sans-serif
        }
    </style>

</head>

<body class="res layout-1">

    <div id="wrapper" class="wrapper-fluid banners-effect-5">


        <!-- Header Container  -->
          <header id="header" class=" typeheader-1">
            <!-- Header Top -->
            <div class="header-top hidden-compact">
                <div class="container">
                    <div class="row">
                        <div class="header-top-left col-lg-7 col-md-8 col-sm-6 col-xs-4">
                            <div class="hidden-sm hidden-xs welcome-msg"><b>Welcome to GogoFood Store !</b> ! Wrap new offers /
                                gift every single day on Weekends </div>
                            <ul class="top-link list-inline hidden-lg hidden-md">
                                <li class="account" id="my_account">
                                    <?php
                                    if(!isset($_SESSION['customer_email'])){
                                       ?>
                                        <ul class="dropdown-menu ">
                                                <li><a href="register.php"><i class="fa fa-user"></i> Register</a></li>
                                                <li><a href="checkout.php"><i class="fa fa-pencil-square-o"></i> Login</a></li>
                                            </ul>
                                       <?php
                                    }else{
                                       ?>
                                            <?php
                                            
                                            $customer_email = $_SESSION['customer_email'];
                                    
                                            $get_customer_details = "Select * from customers where customer_email='$customer_email'";
                                            $run_customer_details = mysqli_query($con, $get_customer_details);
                                            $fetch_customer_details = mysqli_fetch_array($run_customer_details);
                                                                                                
                                            $customer_id = $fetch_customer_details['customer_id']; //:: Customer id 
                                            $customer_name = $fetch_customer_details['customer_name']; //:: customer name

                                            ?>
                                             <?php echo" 
                                               $customer_name"?>
                                             <a href="#" title="My Account " class="btn-xs dropdown-toggle"
                                            data-toggle="dropdown"> <span class="hidden-xs">My Account </span> <span
                                            class="fa fa-caret-down"></span>
                                            </a>
                                            <ul class="dropdown-menu ">
                                                <li><a href="food.php"><i class="fa fa-user"></i> <?php echo" 
                                                $_SESSION[customer_email]"?></a></li>
                                                <li><a href="logout.php"><i class="fa fa-pencil-square-o"></i> Logout</a></li>
                                                
                                            </ul>

                                       <?php
                                    }

                                    ?>
                                    
                                    <!-- <ul class="dropdown-menu ">
                                        <li><a href="register.html"><i class="fa fa-user"></i> Register</a></li>
                                        <li><a href="login.html"><i class="fa fa-pencil-square-o"></i> Login</a></li>
                                    </ul> -->
                                </li>
                            </ul>
                        </div>
                        <div class="header-top-right collapsed-block col-lg-5 col-md-4 col-sm-6 col-xs-8">
                            <ul class="top-link list-inline lang-curr">
                                <li class="language">
                                    <div class="btn-group languages-block ">
                                        <form action="http://demo.smartaddons.com/templates/html/emarket/index.html"
                                            method="post" enctype="multipart/form-data" id="bt-language">
                                            <a class="btn btn-link dropdown-toggle" data-toggle="dropdown">
                                                <span class="">English</span>
                                                <span class="fa fa-angle-down"></span>
                                            </a>
                                            <ul class="dropdown-menu">
                                                <li><a href="index.html"> English </a></li>
                                                <li> <a href="index.html">  Bhasa Malayu </a> </li>
                                            </ul>
                                        </form>
                                    </div>

                                </li>
                            </ul>



                        </div>
                    </div>
                </div>
            </div>
            <!-- //Header Top -->

            <!-- Header center -->
            <div class="header-middle">
                <div class="container">
                    <div class="row">
                        <!-- Logo -->
                        <div class="navbar-logo col-lg-2 col-md-2 col-sm-12 col-xs-12">
                            <div class="logo"><a href="index.html">
							<!--<h1 style="color: white;"> GOGO EMPIRE</h1>-->
                                <img src="image/catalog/CBMC-logo-800x132.png" title="Your Store"
                                        alt="Your Store" />
                                </a></div>
                        </div>
                        <!-- //end Logo -->

                        <!-- Main menu -->
                        <div class="main-menu col-lg-6 col-md-7 ">
                            <div class="responsive so-megamenu megamenu-style-dev">
                                <nav class="navbar-default">
                                    <div class=" container-megamenu  horizontal open ">
                                        <div class="navbar-header">
                                            <button type="button" id="show-megamenu" data-toggle="collapse"
                                                class="navbar-toggle">
                                                <span class="icon-bar"></span>
                                                <span class="icon-bar"></span>
                                                <span class="icon-bar"></span>
                                            </button>
                                        </div>

                                        <div class="megamenu-wrapper">
                                            <span id="remove-megamenu" class="fa fa-times"></span>
                                            <div class="megamenu-pattern">
                                                <div class="container-mega">
                                                <ul class="megamenu" 
                                                        data-animationtime="250">
                                                        <li class="home hover">
                                                            <a href="index.php">HOME </a>
                                                        </li>
                                                        <li class="home hover">
                                                        <a href="hotels.php">STORES </a>
                                                        </li>
                                                        <li class="home hover">
                                                            <a href="">COLLECTION </a>
                                                        </li>
                                                        <li class="home hover">
                                                            <a href="aboutus.php">ABOUT US </a>
                                                        </li>
                                                        <li class="home hover">
                                                            <a href="contact.php">CONTACT US </a>
                                                        </li>
                                                    </ul>
                                                </div>
                                            </div>
                                        </div>
                                    </div>
                                </nav>
                            </div>
                        </div>
                        <!-- //end Main menu -->

                        <div class="middle-right col-lg-4 col-md-3 col-sm-6 col-xs-8">
                            <div class="signin-w hidden-sm hidden-xs">
                                <ul class="signin-link blank">
                                <?php 
                                if(!isset($_SESSION['customer_email'])){
                                    echo"";
                                }else{
                                    echo" <li class='log login'> 
                                    <a class='link-lg'>$_SESSION[customer_email]</a>
                                 </li>";
                                }
                                ?>
                                </ul>
                            </div>



                           

                            <div class="signin-w hidden-sm hidden-xs">
                                <ul class="signin-link blank">
                                <?php
                            if(!isset($_SESSION['customer_email'])){
                                echo" <li class='log login'></i> 
                                <a class='link-lg' href='checkout.php'>Login</a>
                             </li>";
                             echo" <li class='log login'></i> 
                             <a class='link-lg' href='checkout.php'>|</a>
                          </li>";
                             echo" <li class='log login'></i> 
                             <a class='link-lg' href='register.php'>Create an Account</a>
                          </li>";
                            }else{
                                echo"<li class='log login'></i> 
                                <a class='link-lg' href='logout.php'>Logout</a>
                             </li>";
                            }
                            ?>
                                </ul>
                            </div>

                        </div>

                    </div>

                </div>
            </div>
            <!-- //Header center -->

            <!-- Header Bottom -->
            <div class="header-bottom hidden-compact">
                <div class="container">
                    <div class="row">

                        <div class="bottom1 menu-vertical col-lg-2 col-md-3 col-sm-3">
                            <div class="responsive so-megamenu megamenu-style-dev ">
                                <div class="so-vertical-menu ">
                                    <nav class="navbar-default">

                                        <div class="container-megamenu vertical">
                                            <div id="menuHeading">
                                                <div class="megamenuToogle-wrapper">
                                                    <div class="megamenuToogle-pattern">
                                                        <div class="container">
                                                            <div>
                                                                <span></span>
                                                                <span></span>
                                                                <span></span>
                                                            </div>
                                                            All Categories
                                                        </div>
                                                    </div>
                                                </div>
                                            </div>

                                            <div class="navbar-header">
                                                <button type="button" id="show-verticalmenu" data-toggle="collapse"
                                                    class="navbar-toggle">
                                                    <i class="fa fa-bars"></i>
                                                    <span> All Categories </span>
                                                </button>
                                            </div>
                                            <div class="vertical-wrapper">
                                                <span id="remove-verticalmenu" class="fa fa-times"></span>
                                                <div class="megamenu-pattern">
                                                    <div class="container-mega">
                                                        <ul class="megamenu">
                                                            
                                                            
                                                             <li class="item-vertical">
                                                                <p class="close-menu"></p>
                                                                <a href="" class="clearfix">
                                                                    <span>FOOD</span>

                                                                </a>
                                                            </li>
                                                            <!-- <li class="item-vertical  style1 with-sub-menu hover">
                                                                <p class="close-menu"></p>
                                                                <a href="#" class="clearfix">
                                                                    <span class="label"></span>
                                                                    <img src="image/catalog/menu/icons/ico9.png"
                                                                        alt="icon">
                                                                    <span>Electronic</span>

                                                                    <b class="caret"></b>
                                                                </a>
                                                                <div class="sub-menu" data-subwidth="40">
                                                                    <div class="content">
                                                                        <div class="row">
                                                                            <div class="col-md-6">
                                                                                <div class="row">
                                                                                    <div class="col-md-12 static-menu">
                                                                                        <div class="menu">
                                                                                            <ul>
                                                                                                <li><a href="#"
                                                                                                        class="main-menu">Smartphone</a>
                                                                                                    <ul>
                                                                                                        <li><a
                                                                                                                href="#">Esdipiscing</a>
                                                                                                        </li>
                                                                                                        <li><a
                                                                                                                href="#">Scanners</a>
                                                                                                        </li>
                                                                                                        <li><a
                                                                                                                href="#">Apple</a>
                                                                                                        </li>
                                                                                                        <li><a
                                                                                                                href="#">Dell</a>
                                                                                                        </li>
                                                                                                        <li><a
                                                                                                                href="#">Scanners</a>
                                                                                                        </li>
                                                                                                    </ul>
                                                                                                </li>
                                                                                                <li><a href="#"
                                                                                                        class="main-menu">Electronics</a>
                                                                                                    <ul>
                                                                                                        <li><a
                                                                                                                href="#">Asdipiscing</a>
                                                                                                        </li>
                                                                                                        <li><a href="#">Diam
                                                                                                                sit</a>
                                                                                                        </li>
                                                                                                        <li><a href="#">Labore
                                                                                                                et</a>
                                                                                                        </li>
                                                                                                        <li><a
                                                                                                                href="#">Monitors</a>
                                                                                                        </li>
                                                                                                    </ul>
                                                                                                </li>
                                                                                            </ul>
                                                                                        </div>
                                                                                    </div>
                                                                                </div>
                                                                            </div>
                                                                            <div class="col-md-6">
                                                                                <div class="row banner">
                                                                                    <a href="#">
                                                                                        <img src="image/catalog/menu/megabanner/vbanner1.jpg"
                                                                                            alt="banner1">
                                                                                    </a>
                                                                                </div>
                                                                            </div>
                                                                        </div>
                                                                    </div>
                                                                </div>
                                                            </li>
                                                            <li class="item-vertical with-sub-menu hover">
                                                                <p class="close-menu"></p>
                                                                <a href="#" class="clearfix">
                                                                    <img src="image/catalog/menu/icons/ico7.png"
                                                                        alt="icon">
                                                                    <span>Health &amp; Beauty</span>
                                                                    <b class="caret"></b>
                                                                </a>
                                                                <div class="sub-menu" data-subwidth="60">
                                                                    <div class="content">
                                                                        <div class="row">
                                                                            <div class="col-md-12">
                                                                                <div class="row">
                                                                                    <div class="col-md-4 static-menu">
                                                                                        <div class="menu">
                                                                                            <ul>
                                                                                                <li>
                                                                                                    <a href="#"
                                                                                                        class="main-menu">Car
                                                                                                        Alarms and
                                                                                                        Security</a>
                                                                                                    <ul>
                                                                                                        <li><a href="#">Car
                                                                                                                Audio
                                                                                                                &amp;
                                                                                                                Speakers</a>
                                                                                                        </li>
                                                                                                        <li><a href="#">Gadgets
                                                                                                                &amp;
                                                                                                                Auto
                                                                                                                Parts</a>
                                                                                                        </li>
                                                                                                        <li><a href="#">Gadgets
                                                                                                                &amp;
                                                                                                                Auto
                                                                                                                Parts</a>
                                                                                                        </li>
                                                                                                        <li><a href="#">Headphones,
                                                                                                                Headsets</a>
                                                                                                        </li>
                                                                                                    </ul>
                                                                                                </li>
                                                                                                <li>
                                                                                                    <a href="24.html"
                                                                                                        onclick="window.location = '24.html';"
                                                                                                        class="main-menu">Health
                                                                                                        &amp; Beauty</a>
                                                                                                    <ul>
                                                                                                        <li>
                                                                                                            <a href="#">Home
                                                                                                                Audio</a>
                                                                                                        </li>
                                                                                                        <li>
                                                                                                            <a href="#">Helicopters
                                                                                                                &amp;
                                                                                                                Parts</a>
                                                                                                        </li>
                                                                                                        <li>
                                                                                                            <a href="#">Outdoor
                                                                                                                &amp;
                                                                                                                Traveling</a>
                                                                                                        </li>
                                                                                                        <li>
                                                                                                            <a href="#">Toys
                                                                                                                &amp;
                                                                                                                Hobbies</a>
                                                                                                        </li>
                                                                                                    </ul>
                                                                                                </li>
                                                                                            </ul>
                                                                                        </div>
                                                                                    </div>
                                                                                    <div class="col-md-4 static-menu">
                                                                                        <div class="menu">
                                                                                            <ul>
                                                                                                <li>
                                                                                                    <a href="#"
                                                                                                        class="main-menu">Electronics</a>
                                                                                                    <ul>
                                                                                                        <li>
                                                                                                            <a
                                                                                                                href="#">Earings</a>
                                                                                                        </li>
                                                                                                        <li>
                                                                                                            <a href="#">Salon
                                                                                                                &amp;
                                                                                                                Spa
                                                                                                                Equipment</a>
                                                                                                        </li>
                                                                                                        <li>
                                                                                                            <a href="#">Shaving
                                                                                                                &amp;
                                                                                                                Hair
                                                                                                                Removal</a>
                                                                                                        </li>
                                                                                                        <li>
                                                                                                            <a href="#">Smartphone
                                                                                                                &amp;
                                                                                                                Tablets</a>
                                                                                                        </li>
                                                                                                    </ul>
                                                                                                </li>
                                                                                                <li>
                                                                                                    <a href="#"
                                                                                                        class="main-menu">Sports
                                                                                                        &amp;
                                                                                                        Outdoors</a>
                                                                                                    <ul>
                                                                                                        <li>
                                                                                                            <a href="#">Flashlights
                                                                                                                &amp;
                                                                                                                Lamps</a>
                                                                                                        </li>
                                                                                                        <li>
                                                                                                            <a
                                                                                                                href="#">Fragrances</a>
                                                                                                        </li>
                                                                                                        <li>
                                                                                                            <a
                                                                                                                href="#">Fishing</a>
                                                                                                        </li>
                                                                                                        <li>
                                                                                                            <a href="#">FPV
                                                                                                                System
                                                                                                                &amp;
                                                                                                                Parts</a>
                                                                                                        </li>
                                                                                                    </ul>
                                                                                                </li>
                                                                                            </ul>
                                                                                        </div>
                                                                                    </div>
                                                                                    <div class="col-md-4 static-menu">
                                                                                        <div class="menu">
                                                                                            <ul>
                                                                                                <li>
                                                                                                    <a href="#"
                                                                                                        class="main-menu">More
                                                                                                        Car
                                                                                                        Accessories</a>
                                                                                                    <ul>
                                                                                                        <li>
                                                                                                            <a href="#">Lighter
                                                                                                                &amp;
                                                                                                                Cigar
                                                                                                                Supplies</a>
                                                                                                        </li>
                                                                                                        <li>
                                                                                                            <a href="#">Mp3
                                                                                                                Players
                                                                                                                &amp;
                                                                                                                Accessories</a>
                                                                                                        </li>
                                                                                                        <li>
                                                                                                            <a href="#">Men
                                                                                                                Watches</a>
                                                                                                        </li>
                                                                                                        <li>
                                                                                                            <a href="#">Mobile
                                                                                                                Accessories</a>
                                                                                                        </li>
                                                                                                    </ul>
                                                                                                </li>
                                                                                                <li>
                                                                                                    <a href="#"
                                                                                                        class="main-menu">Gadgets
                                                                                                        &amp; Auto
                                                                                                        Parts</a>
                                                                                                    <ul>
                                                                                                        <li>
                                                                                                            <a href="#">Gift
                                                                                                                &amp;
                                                                                                                Lifestyle
                                                                                                                Gadgets</a>
                                                                                                        </li>
                                                                                                        <li>
                                                                                                            <a href="#">Gift
                                                                                                                for
                                                                                                                Man</a>
                                                                                                        </li>
                                                                                                        <li>
                                                                                                            <a href="#">Gift
                                                                                                                for
                                                                                                                Woman</a>
                                                                                                        </li>
                                                                                                        <li>
                                                                                                            <a href="#">Gift
                                                                                                                for
                                                                                                                Woman</a>
                                                                                                        </li>
                                                                                                    </ul>
                                                                                                </li>
                                                                                            </ul>
                                                                                        </div>
                                                                                    </div>
                                                                                </div>
                                                                            </div>
                                                                        </div>
                                                                    </div>
                                                                </div>
                                                            </li>
                                                            <li class="item-vertical css-menu with-sub-menu hover">
                                                                <p class="close-menu"></p>
                                                                <a href="#" class="clearfix">

                                                                    <img src="image/catalog/menu/icons/ico6.png"
                                                                        alt="icon">
                                                                    <span>Smartphone &amp; Tablets</span>
                                                                    <b class="caret"></b>
                                                                </a>
                                                                <div class="sub-menu" data-subwidth="20">
                                                                    <div class="content">
                                                                        <div class="row">
                                                                            <div class="col-sm-12">
                                                                                <div class="row">
                                                                                    <div class="col-sm-12 hover-menu">
                                                                                        <div class="menu">
                                                                                            <ul>
                                                                                                <li>
                                                                                                    <a href="#"
                                                                                                        class="main-menu">Headphones,
                                                                                                        Headsets</a>
                                                                                                </li>
                                                                                                <li>
                                                                                                    <a href="#"
                                                                                                        class="main-menu">Home
                                                                                                        Audio</a>
                                                                                                </li>
                                                                                                <li>
                                                                                                    <a href="#"
                                                                                                        class="main-menu">Health
                                                                                                        &amp; Beauty</a>
                                                                                                </li>
                                                                                                <li>
                                                                                                    <a href="#"
                                                                                                        class="main-menu">Helicopters
                                                                                                        &amp; Parts</a>
                                                                                                </li>
                                                                                                <li>
                                                                                                    <a href="#"
                                                                                                        class="main-menu">Helicopters
                                                                                                        &amp; Parts</a>
                                                                                                </li>
                                                                                            </ul>
                                                                                        </div>
                                                                                    </div>
                                                                                </div>
                                                                            </div>
                                                                        </div>
                                                                    </div>
                                                                </div>
                                                            </li>
                                                            <li class="item-vertical">
                                                                <p class="close-menu"></p>
                                                                <a href="#" class="clearfix">
                                                                    <img src="image/catalog/menu/icons/ico5.png"
                                                                        alt="icon">
                                                                    <span>Health & Beauty</span>

                                                                </a>
                                                            </li>
                                                            <li class="item-vertical">
                                                                <p class="close-menu"></p>
                                                                <a href="#" class="clearfix">
                                                                    <img src="image/catalog/menu/icons/ico4.png"
                                                                        alt="icon">
                                                                    <span>Bathroom</span>
                                                                </a>
                                                            </li>
                                                            <li class="item-vertical">
                                                                <p class="close-menu"></p>
                                                                <a href="#" class="clearfix">
                                                                    <img src="image/catalog/menu/icons/ico3.png"
                                                                        alt="icon">
                                                                    <span>Metallurgy</span>
                                                                </a>
                                                            </li>
                                                            <li class="item-vertical">
                                                                <p class="close-menu"></p>
                                                                <a href="#" class="clearfix">
                                                                    <img src="image/catalog/menu/icons/ico2.png"
                                                                        alt="icon">
                                                                    <span>Bedroom</span>
                                                                </a>
                                                            </li>
                                                            <li class="item-vertical">
                                                                <p class="close-menu"></p>

                                                                <a href="#" class="clearfix">
                                                                    <img src="image/catalog/menu/icons/ico1.png"
                                                                        alt="icon">
                                                                    <span>Health &amp; Beauty</span>
                                                                </a>
                                                            </li>
                                                            <li class="item-vertical" style="display: none;">
                                                                <p class="close-menu"></p>
                                                                <a href="#" class="clearfix">
                                                                    <img src="image/catalog/menu/icons/ico11.png"
                                                                        alt="icon">
                                                                    <span>Toys &amp; Hobbies </span>
                                                                </a>
                                                            </li>
                                                            <li class="item-vertical" style="display: none;">
                                                                <p class="close-menu"></p>
                                                                <a href="#" class="clearfix">
                                                                    <img src="image/catalog/menu/icons/ico12.png"
                                                                        alt="icon">
                                                                    <span>Jewelry &amp; Watches</span>
                                                                </a>
                                                            </li>
                                                            <li class="item-vertical" style="display: none;">
                                                                <p class="close-menu"></p>
                                                                <a href="#" class="clearfix">
                                                                    <img src="image/catalog/menu/icons/ico9.png"
                                                                        alt="icon">
                                                                    <span>Home &amp; Lights</span>
                                                                </a>
                                                            </li>
                                                            <li class="item-vertical" style="display: none;">
                                                                <p class="close-menu"></p>
                                                                <a href="#" class="clearfix">
                                                                    <img src="image/catalog/menu/icons/ico6.png"
                                                                        alt="icon">
                                                                    <span>Metallurgy</span>
                                                                </a>
                                                            </li>

                                                            <li class="loadmore">
                                                                <i class="fa fa-plus-square-o"></i>
                                                                <span class="more-view">More Categories</span>
                                                            </li> -->

                                                        </ul>
                                                    </div>
                                                </div>
                                            </div>
                                        </div>
                                    </nav>
                                </div>
                            </div>

                        </div>

                        <!-- Search -->
                        <div class="bottom2 col-lg-7 col-md-6 col-sm-6">
                            <div class="search-header-w">
                                <div class="icon-search hidden-lg hidden-md hidden-sm"><i class="fa fa-search"></i>
                                </div>

                                <div id="sosearchpro" class="sosearchpro-wrapper so-search ">
                                    <form method="GET"
                                        action="food.php">
                                        <div id="search0" class="search input-group form-group">
                                            <div class="select_category filter_type  icon-select hidden-sm hidden-xs">
                                                <select class="no-border" name="category_id">
                                                    <option value="0">All Categories</option>
                                                    <option value="78">FOOD</option>
                                                    <!-- <option value="77">Cables &amp; Connectors</option>
                                                    <option value="82">Cameras &amp; Photo</option>
                                                    <option value="80">Flashlights &amp; Lamps</option>
                                                    <option value="81">Mobile Accessories</option>
                                                    <option value="79">Video Games</option>
                                                    <option value="20">Jewelry &amp; Watches</option>
                                                    <option value="76">&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;Earings
                                                    </option>
                                                    <option value="26">&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;Wedding Rings
                                                    </option>
                                                    <option value="27">&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;Men Watches
                                                    </option> -->
                                                </select>
                                            </div>

                                            <input class="autosearch-input form-control" type="text" value="" size="50"
                                                autocomplete="off" placeholder="Keyword here..." name="search">
                                            <span class="input-group-btn">
                                                <button type="submit" class="button-search btn btn-primary"
                                                    name=""><i class="fa fa-search"></i></button>
                                            </span>
                                        </div>
                                        <input type="hidden" name="route" value="product/search" />
                                    </form>
                                </div>
                            </div>
                        </div>
                        <!-- //end Search -->

                        <!-- Secondary menu -->
                        <div class="bottom3 col-lg-3 col-md-3 col-sm-3">


                          <!--cart-->
                          <div class="shopping_cart">
                                <div id="cart" class="btn-shopping-cart">

                                    <a data-loading-text="Loading... " class="btn-group top_cart dropdown-toggle"
                                        data-toggle="dropdown" aria-expanded="true">
                                        <div class="shopcart">
                                            <span class="icon-c">
                                                <i class="fa fa-shopping-bag"></i>
                                            </span>
                                            <div class="shopcart-inner">
                                                <p class="text-shopping-cart">
                                                    My cart
                                                </p>

                                                <?php

                                                if(!isset($_SESSION['customer_email'])){

                                                    ?>

                                                        <span class="total-shopping-cart cart-total-full">
                                                    <span class="items_cart">0</span><span class="items_cart2">
                                                        item(s)</span><span class="items_carts"> + RM -0 </span>
                                                </span>

                                                <?php
                                                  

                                                }else{

                                                    //: Getting total number of cart_items
                                                    $customer_email = $_SESSION['customer_email'];
                                    
                                                    $get_customer_details = "Select * from customers where customer_email='$customer_email'";
                                                    $run_customer_details = mysqli_query($con, $get_customer_details);
                                                    $fetch_customer_details = mysqli_fetch_array($run_customer_details);
                                                                                                        
                                                    $customer_id = $fetch_customer_details['customer_id']; //:: Customer id 
                                                    $customer_name = $fetch_customer_details['customer_name']; //:: customer name

                                                    $get_items = "select * from customer_foodcart where customer_id='$customer_id'";
                                                    $run_items = mysqli_query($con, $get_items);
                                                    $count_items = mysqli_num_rows($run_items);

                                                    //: Getting the total of customer cart items

                                                    //: Getting the total of prize..

                                                    $query = "select SUM(total_price) as 'sumtotalprice' from customer_foodcart where customer_id='$customer_id' ";
                                                    $res = mysqli_query($con, $query);
                                                    $data = mysqli_fetch_array($res);

                                                    $total = $data['sumtotalprice'];

                                                        //: Getting GST...
                                                        $gst_rate = 6;
                                                        $gst = ($gst_rate / 100) * $total;

                                                        //: Delivery Charges...
                                                        $delivery = 5;

                                                        //: Grand Total..
                                                        $grand_total = ($total + $gst);

                                                        ?>

                                                        <span class="total-shopping-cart cart-total-full">
                                                    <span class="items_cart"><?php echo"$count_items"?></span><span class="items_cart2">
                                                        item(s)</span><span class="items_carts"> + RM <?php echo "$grand_total"?> </span>
                                                </span>

                                                <?php
                                                    
                                                }

                                                        

                                                ?>

                                                
                                            </div>
                                        </div>
                                    </a>

                                    <?php

                                        if(!isset($_SESSION['customer_email'])){

                                            ?>
                                            <ul class="dropdown-menu pull-right shoppingcart-box">
                                            <li>
                                                <p class="text-center empty">Your shopping cart is empty!</p>
                                            </li>
                                        </ul>
                                            <?php

                                        }else{

                                            ?>

                                        <ul class="dropdown-menu pull-right shoppingcart-box" role="menu">
                                            <li>
                                                <table class="table table-striped">
                                                    <tbody>
                                                        <?php

                                                            //: Getting Customer ID..
                                                            $customer_email = $_SESSION['customer_email'];
                                
                                                            $get_customer_details = "Select * from customers where customer_email='$customer_email'";
                                                            $run_customer_details = mysqli_query($con, $get_customer_details);
                                                            $fetch_customer_details = mysqli_fetch_array($run_customer_details);
                                                            
                                                            $customer_id = $fetch_customer_details['customer_id']; //:: Customer id 
                                                            $customer_name = $fetch_customer_details['customer_name']; //:: customer name

                                                            //: Cart Script..
                                                            $get_cart_data = "Select * from customer_foodcart where customer_id='$customer_id'";
                                                            $run_cart_data = mysqli_query($con, $get_cart_data);
                                                            $count_brands = mysqli_num_rows($run_cart_data);
                                                            if($count_brands==0){
                                                            echo "<li>
                                                            <p class='text-center empty'>Your shopping cart is empty!</p>
                                                        </li>";
                                                            }


                                                            //:: delete item from cart..
                                                            if(isset($_GET['removeitem'])){

                                                                $removeitem_id = $_GET['removeitem'];
                                                                $foodcompanya_id = $_GET['foodcompany'];

                                                                $delete_item = "delete from customer_foodcart where fooditem_id='$removeitem_id' AND customer_id='$customer_id'";

                                                                $run_delete = mysqli_query($con, $delete_item);

                                                                if($run_delete){
                                                                    echo "<script>window.open('food.php?foodcompany=$foodcompanya_id','_self')</script>";
                                                                    
                                                                }
                                                            
                                                            }
                                                            
                                                            while($row_cart_data=mysqli_fetch_array($run_cart_data)){
                                                                
                                                                $fcompanya_id = $_GET['foodcompany'];
                                                                $cart_id=$row_cart_data['foodcart_id'];
                                                                $cart_quantity=$row_cart_data['quantity'];
                                                                $cart_itemid=$row_cart_data['fooditem_id'];

                                                              

                                                                // Getting Product data from food_items table.
                                                                
                                                                $get_cartitem_data = "Select * from food_items where item_id='$cart_itemid'";
                                                                $run_cartitem_data = mysqli_query($con, $get_cartitem_data);
                                                                $fetch_cartitem_data = mysqli_fetch_array($run_cartitem_data);

                                                                $cartitem_id = $fetch_cartitem_data['item_id'];
                                                                $cartitem_image = $fetch_cartitem_data['item_img'];
                                                                $cartitem_price = $fetch_cartitem_data['item_price'];
                                                                $cartitem_name = $fetch_cartitem_data['item_title'];

                                                                //:: 
                                                                $totalunitprice = ($cartitem_price * $cart_quantity);
                                                                
                                                                echo " <tr>
                                                                <td class='text-center' style='width:70px'>
                                                                    <a href='product.html'>
                                                                        <img src='./admin_area/fooditem_images/$cartitem_image'
                                                                            style='width:70px' alt='Yutculpa ullamcon'
                                                                            title='Yutculpa ullamco' class='preview'>
                                                                    </a>
                                                                </td>
                                                                <td class='text-left'> <a class='cart_product_name'
                                                                        href='product.html'>$cartitem_name</a>
                                                                </td>
                                                                <td class='text-center'>$cart_quantity</td>
                                                                <td class='text-center'>RM$totalunitprice</td>
                                                                <td class='text-right'>
                                                                    <a href='product.html' class='fa fa-edit'></a>
                                                                </td>
                                                                <td class='text-right'>
                                                                    
                                                                    <a href='food.php?removeitem=$cartitem_id&foodcompany=$fcompanya_id'class='fa fa-times fa-delete'></a>
                                                                   
                                                                    </td>
                                                            </tr>";

                                                            }


                                                        ?>
                                                    </tbody>
                                                </table>
                                            </li>
                                            <li>
                                                <div>
                                                    <table class="table table-bordered">
                                                        <tbody>
                                                            <tr>
                                                                <td class="text-left"><strong>Sub-Total</strong>
                                                                </td>
                                                                <td class="text-right">RM <?Php echo "$total"?></td>
                                                            </tr>
                                                            <tr>
                                                                <td class="text-left"><strong>GST Tax (-6.00)%</strong>
                                                                </td>
                                                                <td class="text-right">RM<?php echo"$gst"?></td>
                                                            </tr>
                                                            <tr>
                                                                <td class="text-left"><strong>Total</strong>
                                                                </td>
                                                                <td class="text-right">RM <?php echo "$grand_total"?></td>
                                                            </tr>
                                                        </tbody>
                                                    </table>
                                                    <p class="text-right"> 
                                                    <a class="btn view-cart" href="foodcart.php"><i class="fa fa-shopping-cart"></i>Update Cart</a>&nbsp;&nbsp;&nbsp; 

                                                            <a type="submit" value="submit" name="submit" href="#my_modal" class="btn btn-light" data-toggle="modal" ><i class="fa fa-share"></i>Checkout</a>    
                                                    </p>
                                                </div>
                                            </li>
                                        </ul>
                                            
                                            <?php

                                        }
                                    ?>

                                </div>

                            </div>
                            <!--//cart-->

                    
                        </div>

                    </div>
                </div>

            </div>
          </header>
        <!-- //Header Container  -->


        

         <!-- Main Container  -->
         <div class="main-container container">
            <ul class="breadcrumb">
                <li><a href="index.php"><i class="fa fa-home"></i></a></li>
                <li><a href="#">FOOD</a></li>
            </ul>

            <div class="row">

                <!--Middle Part Start-->
                <div id="content" class="col-md-12 col-sm-12">
                    <div class="products-category">
                                        
                    <?php

                    

                    $hotel_id = $_GET['foodcompany'];

                    $get_hotel_data = "select * from food_company where fcompany_id='$hotel_id'";

                    $run_hotel_data = mysqli_query($con, $get_hotel_data);

                    $row_hotel_data=mysqli_fetch_array($run_hotel_data);

                    
                    $hotel_title = $row_hotel_data['Company_name'];
                    $hotel_type = $row_hotel_data['company_type'];
                   

                    ?>
                        <h3 class="title-category "><?php echo "$hotel_title"?></h3>
                        <div class="category-derc">
                        </div>

                        
                        <!-- Filters -->
                        <div class="product-filter product-filter-top filters-panel">
                            <div class="row">
                                <div class="col-md-5 col-sm-3 col-xs-12 view-mode">

                                    <div class="list-view">
                                        <button class="btn btn-default grid active" data-view="grid"
                                            data-toggle="tooltip" data-original-title="Grid"><i
                                                class="fa fa-th"></i></button>
                                        <button class="btn btn-default list" data-view="list" data-toggle="tooltip"
                                            data-original-title="List"><i class="fa fa-th-list"></i></button>
                                    </div>

                                </div>
                                <div class="short-by-show form-inline text-right col-md-7 col-sm-9 col-xs-12">
                                    <div class="form-group short-by">
                                        <label class="control-label" for="input-sort">Sort By:</label>
                                        <select id="input-sort" class="form-control" onchange="location = this.value;">
                                            <option value="" selected="selected">Default</option>
                                            <option value="">Name (A - Z)</option>
                                            <option value="">Name (Z - A)</option>
                                            <option value="">Price (Low &gt; High)</option>
                                            <option value="">Price (High &gt; Low)</option>
                                            <option value="">Rating (Highest)</option>
                                            <option value="">Rating (Lowest)</option>
                                            <option value="">Model (A - Z)</option>
                                            <option value="">Model (Z - A)</option>
                                        </select>
                                    </div>
                                    <div class="form-group">
                                        <label class="control-label" for="input-limit">Show:</label>
                                        <select id="input-limit" class="form-control" onchange="location = this.value;">
                                            <option value="" selected="selected">15</option>
                                            <option value="">25</option>
                                            <option value="">50</option>
                                            <option value="">75</option>
                                            <option value="">100</option>
                                        </select>
                                    </div>
                                </div>
                                <!-- <div class="box-pagination col-md-3 col-sm-4 col-xs-12 text-right">
                                <ul class="pagination">
                                    <li class="active"><span>1</span></li>
                                    <li><a href="">2</a></li><li><a href="">&gt;</a></li>
                                    <li><a href="">&gt;|</a></li>
                                </ul>
                            </div> -->
                            </div>
                        </div>
                        <!-- //end Filters -->
                        <!--changed listings-->




                    <!-- Main Part -->
                        
                    <div class="so-onepagecheckout row">

                        <?php

                            if($hotel_type == 'food'){

                                ?>

                                

                                <!-- here. -->

                                
                                <?php

                                $get_foodcategory = "select * from food_category where cat_type = 'food' ";
                                $run_foodcategory = mysqli_query($con, $get_foodcategory);
                                while($row_foodcategory = mysqli_fetch_array($run_foodcategory)){
                                    $foodcategory_id = $row_foodcategory['fcat_id'];
                                    $foodcategory_title = $row_foodcategory['food_cat'];

                                ?>

                                <div class="col-left col-sm-12">
                                    <div class="panel panel-default">
                                        <div class="panel-heading">
                                            <h4 class="panel-title"><i class="fa fa-sign-in"></i> <?php echo "$foodcategory_title"; ?>
                                            </h4>
                                        </div>
                                        <div class="panel-body">
                                            <div class="products-list row nopadding-xs so-filter-gird">
                                                        <?php

                                                            if(isset($_GET['foodcompany'])){

                                                                $fcompany_id = $_GET['foodcompany'];
                                                                $get_company_pro = "select * from food_items where fcompany_id='$fcompany_id' AND fcat_id='$foodcategory_id'";
                                                                $run_company_pro = mysqli_query($con, $get_company_pro); 
                                                                $count_company_pro = mysqli_num_rows($run_company_pro);

                                                                if($count_company_pro==0){
                                                                    echo "<p class='text-center empty'>this hotel doesn't have any fooditem relates with this category!</p>";

                                                                }else{

                                                                while($row_company_pro=mysqli_fetch_array($run_company_pro)){
                                                                    $citem_id = $row_company_pro['item_id'];
                                                                    $citem_cat = $row_company_pro['fcat_id'];
                                                                    $citem_company = $row_company_pro['fcompany_id'];
                                                                    $citem_title = $row_company_pro['item_title'];
                                                                    $citem_price = $row_company_pro['item_price'];
                                                                    $citem_img = $row_company_pro['item_img'];
                                                                    $citem_desc = $row_company_pro['item_desc'];
                                                                    $citem_discounted_price = $row_company_pro['item_discounted_price'];
                                                                    $citem_availability = $row_company_pro['item_avalability'];
                                                                    $citem_youtubelink = $row_company_pro['youtube_link'];
                    
                    
                                                                    //:: Getting Category.. 
                    
                                                                    $get_cat_item = "Select * from food_category where fcat_id = '$citem_cat' ";
                                                                    $run_cat_item = mysqli_query($con,$get_cat_item);
                                                                    $row_cat_item = mysqli_fetch_array($run_cat_item);
                                                                    $item_catname= $row_cat_item['food_cat'];

                                                                    echo"<div class='product-layout col-lg-15 col-md-4 col-sm-6 col-xs-12'>
                                                                    <div class='product-item-container'>
                                                                        <div class='left-block'>
                                                                            <div class='product-image-container second_img'>
                                                                                <a href='singleitems.php?itemid=$citem_id' target='_self' title=' $citem_title'>
                                                                                    <img src='./admin_area/fooditem_images/$citem_img'
                                                                                        class='img-1 img-responsive' alt='image'>
                                                                                    <img src='./admin_area/fooditem_images/$citem_img'
                                                                                        class='img-2 img-responsive' alt='image'>
                                                                                </a>
                                                                            </div>
                                                                            <div class='box-label'><span class='label-product label-new'> $citem_availability</span></div>
                                                                            <div class='button-group so-quickview cartinfo--left'>


                                                                            <form action='main_code.php' method='post'>

                                                                                <input type='hidden' name='add_cart' value='$citem_id'>
                                                                                <input type='hidden' name='foodcompany' value='$fcompany_id'>
                                                                            
                                                                                <button type='submit' name='addtocart_btn' class='addToCart btn-button' title='Add to cart'
                                                                                ><i class='fa fa-shopping-basket'></i>
                                                                                <span>Add to cart </span>
                                                                                </button>

                                                                            </form>
  
                                                                     
                                                                                <a href='$citem_youtubelink' class='addToCart btn-button' title='Youtube Link'
                                                                                > <i class='fa fa-youtube-play'></i>
                                                                                <span>Youtube Link</span>
                                                                            </a>
                                                                            </div>
                                                                        </div>
                                                                        <div class='right-block'>
                                                                            <div class='caption'>
                                                                                <span class='price-old'>$item_catname</span>
                                                                                <h4><a title='Doenpuis consuat ' target='_self'> $citem_title </a></h4>
                                                                                <div class='price'> <span class='price-new'>RM $citem_price</span>
                                                                        
                                                                                </div>
                                                                                <div class='description item-desc'>
                                                                                $citem_desc
                                                                                </div>
                                                                                <div class='list-block'>
                                                                                   <form action='main_code.php' method='post'>
                                                                            
                                                                                <input type='hidden' name='add_cart' value='$citem_id'>
                                                                                <input type='hidden' name='foodcompany' value='$fcompany_id'>
                        
                                                                                <button class='addToCart btn-button' name='addtocart_btn' type='submit' title='Add to Cart'
                                                                                ><i
                                                                                class='fa fa-shopping-basket'></i>
                                                                                </button>

                                                                            </form>
  
                                                                                    <a href='$citem_youtubelink' class='compare btn-button' type='button' title='Youtube Link'
                                                                                    ><i
                                                                                            class='fa fa-youtube-play'></i>
                                                                                    </a>
                                                                        
                                                                                </div>
                                                                                
                                                                            </div>
                                                                        </div>
                                                                    </div>
                                                                </div>
                                                                    ";

                                                                }

                                                            } //:: if class

                                                                }else {

                                                                    echo "<script>window.open('hotels.php','_self')</script>";

                                                                    
                                                                }
                                                                                
                                                        ?>
                                                        
                                                    
                                            </div>
                                            
                                        </div>
                                    </div>
                                    
                                </div> 

                                <?php
                                }
                                ?>
                            


                                <!-- here -->




                        <?php


                            
                        }else if($hotel_type == 'product') {


                            ?>

                                

                                <!-- here. -->

                                
                                <?php

                                $get_foodcategory = "select * from food_category where cat_type = 'product'";
                                $run_foodcategory = mysqli_query($con, $get_foodcategory);
                                while($row_foodcategory = mysqli_fetch_array($run_foodcategory)){
                                    $foodcategory_id = $row_foodcategory['fcat_id'];
                                    $foodcategory_title = $row_foodcategory['food_cat'];

                                ?>

                                <div class="col-left col-sm-12">
                                    <div class="panel panel-default">
                                        <div class="panel-heading">
                                            <h4 class="panel-title"><i class="fa fa-sign-in"></i> <?php echo "$foodcategory_title"; ?>
                                            </h4>
                                        </div>
                                        <div class="panel-body">
                                            <div class="products-list row nopadding-xs so-filter-gird">
                                                        <?php

                                                            if(isset($_GET['foodcompany'])){

                                                                $fcompany_id = $_GET['foodcompany'];
                                                                $get_company_pro = "select * from food_items where fcompany_id='$fcompany_id' AND fcat_id='$foodcategory_id'";
                                                                $run_company_pro = mysqli_query($con, $get_company_pro); 
                                                                $count_company_pro = mysqli_num_rows($run_company_pro);

                                                                if($count_company_pro==0){
                                                                    echo "<p class='text-center empty'>this hotel doesn't have any fooditem relates with this category!</p>";

                                                                }else{

                                                                while($row_company_pro=mysqli_fetch_array($run_company_pro)){
                                                                    $citem_id = $row_company_pro['item_id'];
                                                                    $citem_cat = $row_company_pro['fcat_id'];
                                                                    $citem_company = $row_company_pro['fcompany_id'];
                                                                    $citem_title = $row_company_pro['item_title'];
                                                                    $citem_price = $row_company_pro['item_price'];
                                                                    $citem_img = $row_company_pro['item_img'];
                                                                    $citem_desc = $row_company_pro['item_desc'];
                                                                    $citem_discounted_price = $row_company_pro['item_discounted_price'];
                                                                    $citem_availability = $row_company_pro['item_avalability'];
                                                                    $citem_youtubelink = $row_company_pro['youtube_link'];
                    
                    
                                                                    //:: Getting Category.. 
                    
                                                                    $get_cat_item = "Select * from food_category where fcat_id = '$citem_cat' ";
                                                                    $run_cat_item = mysqli_query($con,$get_cat_item);
                                                                    $row_cat_item = mysqli_fetch_array($run_cat_item);
                                                                    $item_catname= $row_cat_item['food_cat'];

                                                                    echo"<div class='product-layout col-lg-15 col-md-4 col-sm-6 col-xs-12'>
                                                                    <div class='product-item-container'>
                                                                        <div class='left-block'>
                                                                            <div class='product-image-container second_img'>
                                                                                <a href='singleitems.php?itemid=$citem_id' target='_self' title=' $citem_title'>
                                                                                    <img src='./admin_area/fooditem_images/$citem_img'
                                                                                        class='img-1 img-responsive' alt='image'>
                                                                                    <img src='./admin_area/fooditem_images/$citem_img'
                                                                                        class='img-2 img-responsive' alt='image'>
                                                                                </a>
                                                                            </div>
                                                                            <div class='box-label'> <span class='label-product label-new'> $citem_availability</span></div>
                                                                            <div class='button-group so-quickview cartinfo--left'>
                                                                                <a href='fcheckout.php?add_cart=$citem_id&foodcompany=$fcompany_id' class='addToCart btn-button' title='Add to cart'
                                                                                    > <i class='fa fa-shopping-basket'></i>
                                                                                    <span>Add to cart </span>
                                                                                </a>
                                                                                <a href='$citem_youtubelink' class='addToCart btn-button' title='Youtube Link'
                                                                                > <i class='fa fa-youtube-play'></i>
                                                                                <span>Youtube Link</span>
                                                                            </a>
                                                                            </div>
                                                                        </div>
                                                                        <div class='right-block'>
                                                                            <div class='caption'>
                                                                                <span class='price-old'>$item_catname</span>
                                                                                <h4><a title='Doenpuis consuat ' target='_self'> $citem_title </a></h4>
                                                                                <div class='price'> <span class='price-new'>RM $citem_price</span>
                                                                        
                                                                                </div>
                                                                                <div class='description item-desc'>
                                                                                $citem_desc
                                                                                </div>
                                                                                <div class='list-block'>
                                                                                    <a href='fcheckout.php?add_cart=$citem_id&foodcompany=$fcompany_id' class='addToCart btn-button' type='button' title='Add to Cart'
                                                                                    ><i
                                                                                            class='fa fa-shopping-basket'></i>
                                                                                    </a>
                                                                                    <a href='$citem_youtubelink' class='compare btn-button' type='button' title='Youtube Link'
                                                                                    ><i
                                                                                            class='fa fa-youtube-play'></i>
                                                                                    </a>
                                                                                </div>
                                                                                
                                                                            </div>
                                                                        </div>
                                                                    </div>
                                                                </div>
                                                                    ";

                                                                }

                                                            } //:: if class

                                                                }else {

                                                                    echo "<script>window.open('hotels.php','_self')</script>";

                                                                    
                                                                }
                                                                                
                                                        ?>
                                                        
                                                    
                                            </div>
                                            
                                        </div>
                                    </div>
                                    
                                </div> 

                                <?php
                                }
                                ?>
                            


                                <!-- here -->




                        <?php

                            

                            
                           
                        }



                        ?>


                        


                           

                        
                    </div>



<!-- Main Part -->





             
                        <!--// End Changed listings-->
                        <!-- Filters -->
                        <div class="product-filter product-filter-bottom filters-panel">
                            <div class="row">
                                <div class="col-sm-6 text-left"></div>
                                <div class="col-sm-6 text-right">Showing 1 to 15 of 15 (1 Pages)</div>
                            </div>
                        </div>
                        <!-- //end Filters -->

                    </div>

                </div>

                <!--Middle Part End-->
            </div>
        </div>
        <!-- //Main Container -->


        <!-- Footer Container -->
        <footer class="footer-container typefooter-1">

            <hr>

            <div class="footer-middle ">
                <div class="container">
                    <div class="row">
                        <div class="col-lg-3 col-md-3 col-sm-12 col-xs-12 col-style">
                            <div class="infos-footer">
                                <h3 class="modtitle">Contact Us</h3>
                                <ul class="menu">
                                    <li class="adres">
                                        Swift Commerce Sdn Bhd Lot 23 , Lebuh Sultan Mohamed 1 42000 Klang
                                    </li>
                                    <li class="phone">
                                        Tel: 03-3169 6700 / Fax: 03-3176 1271
                                    </li>
                                    <li class="mail">
                                        askme@swiftcommerce.my 
                                    </li>
                                    <li class="time">
                                        Open time: 07:00AM - 5:30PM
                                    </li>
                                </ul>
                            </div>


                        </div>
                        <div class="col-lg-2 col-md-2 col-sm-4 col-xs-12 col-style">
                            <div class="box-information box-footer">
                                <div class="module clearfix">
                                    <h3 class="modtitle">Information</h3>
                                    <div class="modcontent">
                                        <ul class="menu">
                                            <li><a href="aboutus.php">ABOUT US </a></li>
                                            <li><a href="#">FAQ</a></li>
                                            <li><a href="#">Warranty And Services</a></li>
                                            <li><a href="#">Support 24/7 page</a></li>
                                            <li><a href="#">Product Registration</a></li>
                                            <li><a href="#">Product Support</a></li>
                                        </ul>
                                    </div>
                                </div>
                            </div>

                        </div>
                        <div class="col-lg-2 col-md-2 col-sm-4 col-xs-12 col-style">
                            <div class="box-account box-footer">
                                <div class="module clearfix">
                                    <h3 class="modtitle">Extras</h3>
                                    <div class="modcontent">
                                        <ul class="menu">
                                            <li> <a href="aboutus.php"> Gogo Empire </a></li>
                                            <li><a href="#">Collection</a></li>
                                            <li><a href="contact.php">Contact Us</a></li>
                                        </ul>
                                    </div>
                                </div>
                            </div>


                        </div>
                        <div class="col-lg-2 col-md-2 col-sm-4 col-xs-12 col-style">
                            <div class="box-service box-footer">
                                <div class="module clearfix">
                                    <h3 class="modtitle">Services</h3>
                                    <div class="modcontent">
                                        <ul class="menu">
                                            <li><a href="#">Kuala Lumpur</a></li>
                                            <li><a href="#">Selangor</a></li>
                                            <li><a href="#">KL Central</a></li>
                                        </ul>
                                    </div>
                                </div>
                            </div>

                        </div>
                        <div class="col-lg-3 col-md-3 col-sm-12 col-xs-12 col-style">


                        </div>

                        <div class="col-lg-12 col-xs-12 text-center">
                            <img src="image/catalog/demo/payment/payment.png" alt="imgpayment">
                        </div>
                    </div>
                </div>
            </div>

            <!-- Footer Bottom Container -->
            <div class="footer-bottom ">
                <div class="container">
                    <div class="copyright">
                        gogoempire © 2020. All Rights Reserved. 
                    </div>
                </div>
            </div>
            <!-- /Footer Bottom Container -->


            <!--Back To Top-->
            <div class="back-to-top"><i class="fa fa-angle-up"></i></div>
        </footer>
        <!-- //end Footer Container -->

    </div>

   
    

    <!-- Checkout Modal -->
    <div class="modal fade" id="my_modal" tabindex="-1" role="dialog" aria-labelledby="exampleModalLabel" aria-hidden="true">
        <div class="modal-dialog" role="document">
                                                        <div class="modal-content">
                                                        <div class="modal-header">
                                                        <h6 style="text-align: center;  color: black; font-weight: bold; font-size: 16px;"
                                                                class="modal-title" id="exampleModalLongTitle">SELECT YOUR DINNING PREFERENCE </h6>
                                                        
                                                            
                                                        </div>
                                                        <div class="modal-body">
                                                            <!-- <form action="addcustomerorder.php" method="POST" enctype="multipart/form-data"> -->

                                                          
                                                            <div class="form-group col-md-6 ">
                                                            <div class="card" style="width: 18rem;">
                                                                <img class="card-img-top" src="./assets/delivery.png" alt="Card image cap">
                                                                <div class="card-body">
                                                                    <h2 class="card-title" style="align: center;">Pickup</h5>
                                                                    <p class="card-text">Self Collect and beat the queue</p>

                                                                    <a id="button-comment" href="#datetimeModal" data-toggle="modal"
                                                                    class="btn buttonGray" ><span>SELECT</span></a>
                                                                    
                                                                    
                                                                   
                                                                   
                                                                   
                                                                </div>
                                                                </div>
                                                            </div>

                                                            <div class="form-group col-md-6 ">
                                                            
                                                            <div class="card" style="width: 18rem;">
                                                                <img class="card-img-top" src="./assets/delivery.png" alt="Card image cap">
                                                                <div class="card-body">
                                                                <h2 class="card-title">Delivery</h5>
                                                                <p class="card-text">We will deliver your food at your doorstep</p>
                                                                <a id="button-comment" href="fcheckout.php?delivery"
                                                                class="btn buttonGray"><span>SELECT</span></a>
                                                                </div>
                                                            </div>
                                                            </div>


                                                            <div style=" border-top: 0 none;" class="modal-footer"> 
                                                            
                                                            </div>
                                                        
                                                            <!-- </form> -->
                                                        
                                                        </div>
                                                        
                                                        </div>
                                </div>
    </div>
    <!-- /Checkout Modal -->



   
    <?php 

       

            //  if(!isset($_SESSION['customer_email'])){

            // }else {


            $get_carttable_rows = "Select * from customer_foodcart";
            $run_query = mysqli_query($con, $get_carttable_rows);
            $count =  mysqli_num_rows($run_query);

            if($count == 0){

            }else {



            $gethotelid = "Select * from customer_foodcart where customer_id='$customer_id'";
            $runhotelid = mysqli_query($con, $gethotelid);
            $fetch_hotelid = mysqli_fetch_array($runhotelid);
            $hotelid = $fetch_hotelid['foodhotel_id'];

            $gethotelAddress = "Select * from food_company where fcompany_id='$hotelid'";
            $runhotelAddress = mysqli_query($con, $gethotelAddress);
            $fetch_hotelAddress = mysqli_fetch_array($runhotelAddress);
            $hotelAddress = $fetch_hotelAddress['company_address'];


            }

        // }


    ?>


     <!-- DATE & TIME MODAL -->
        
     <div class="modal fade" id="datetimeModal" tabindex="-1" role="dialog" aria-labelledby="exampleModalLabel" aria-hidden="true">

        <div class="modal-dialog" role="document">
            <div class="modal-content">
                <div class="modal-header">
                    <h1 style="text-align: center;  color: black; font-weight: bold; font-size: 16px;"
                    class="modal-title" id="exampleModalLongTitle">PICK UP</h1>                              
                </div>
                <div class="modal-body">
                    <form action="addcustomerorder.php" method="POST" enctype="multipart/form-data">
                        
                    <div class="form-group col-md-12 ">
                            <label for="inputAddress"><h3><strong>Pickup Store: </strong> </h3> </label>
                            <br>
                            <?php echo "$hotelAddress" ?>
                            
                        </div>
                        
                        <div class="form-group col-md-12 ">
                            <label for="inputAddress"><h3><strong>Select Pickup Date: </strong> </h3> </label>
                            <input class="form-control" id="date" placeholder="DD-MM-YYYY" name="date" required="required">
                        </div>

                                        
                        <div class="form-group col-md-12 ">
                            <label for="inputAddress"><h3><strong>Select Pickup Time: </strong> </h3> </label>
                            <select name="picktime" class="form-control" id="sel1">
                                <option>11:00AM to 11:30AM</option>
                                <option>11:30AM to 12:00PM</option>
                                <option>12:00PM to 12:30PM</option>
                                <option>12:30PM to 01:00PM</option>
                                <option>01:00PM to 01:30PM</option>
                                <option>01:30PM to 02:00PM</option>
                                <option>02:00PM to 02:30PM</option>
                                <option>02:30PM to 03:00PM</option>
                                <option>03:00PM to 03:30PM</option>
                                <option>03:30PM to 04:00PM</option>
                                <option>04:00PM to 04:30PM</option>
                                <option>04:30PM to 05:00PM</option>
                            </select>
                        </div>

                        <div class="form-group col-md-12 ">
                            <input type="hidden" class="form-control"  value= "<?php echo $hotelid ?>" name="hotelid">
                        </div>


                        <div style=" border-top: 0 none;" class="modal-footer"> 
                            <button type="button" class="btn btn-secondary" data-dismiss="modal">Close</button>
                            <button type="submit" class="btn btn-primary" type="submit" id="registeruser"  name="submit1">Proceed to Order</button>
                        </div>
                                                            
                    </form>
                                                                
                </div>
                                                                
            </div>
        </div>

    </div>


    <!-- /DATE & TIME MODAL -->


    <!-- Include Libs & Plugins
	============================================ -->
    <!-- Placed at the end of the document so the pages load faster -->
    <!-- <script type="text/javascript" src="js/jquery-2.2.4.min.js"></script> -->
    <script type="text/javascript" src="js/bootstrap.min.js"></script>
    <script type="text/javascript" src="js/owl-carousel/owl.carousel.js"></script>
    <script type="text/javascript" src="js/themejs/libs.js"></script>
    <script type="text/javascript" src="js/unveil/jquery.unveil.js"></script>
    <script type="text/javascript" src="js/countdown/jquery.countdown.min.js"></script>
    <script type="text/javascript" src="js/dcjqaccordion/jquery.dcjqaccordion.2.8.min.js"></script>
    <script type="text/javascript" src="js/datetimepicker/moment.js"></script>
    <script type="text/javascript" src="js/datetimepicker/bootstrap-datetimepicker.min.js"></script>
    <script type="text/javascript" src="js/jquery-ui/jquery-ui.min.js"></script>



     <!-- Datetimepicker Script -->
     <script>
                $(document).ready(function(){
                var date_input=$('input[name="date"]'); //our date input has the name "date"
                var container=$('.bootstrap-iso form').length>0 ? $('.bootstrap-iso form').parent() : "body";
                var options={
                    format: 'dd-mm-yyyy',
                    timepicker: true,
                    
                };
                date_input.datepicker(options);
                })
                </script>
    
    <!-- Getting date id Script -->



   


    <!-- Theme files
	============================================ -->


    <script type="text/javascript" src="js/themejs/so_megamenu.js"></script>
    <script type="text/javascript" src="js/themejs/addtocart.js"></script>
    <script type="text/javascript" src="js/themejs/application.js"></script>
    <script type="text/javascript">
     
        // Check if Cookie exists
        if ($.cookie('display')) {
            view = $.cookie('display');
        } else {
            view = 'grid';
        }
        if (view) display(view);
        
    </script>
</body>

<!--  demo.smartaddons.com/templates/html/emarket/category.html by AkrAm, Sat, 20 Apr 2019 20:00:15 GMT -->

</html>




