<?php
session_start();
include("includes/db.php");
include("functions/functions.php");

?>
<!DOCTYPE html>
<html lang="en">

<!--  demo.smartaddons.com/templates/html/emarket/category.html by AkrAm, Sat, 20 Apr 2019 19:59:32 GMT -->

<head>

    <!-- Basic page needs
    ============================================ -->
    <!-- <title>GOGO EMPIRE STORE</title>
    <meta charset="utf-8">
    <meta name="keywords"
        content="Gogo Empire Sdn Bhd" />
    <meta name="description"
        content="Gogo Empire Sdn Bhd" />
    <meta name="author" content="Gogo Empire">
    <meta name="robots" content="index, follow" /> -->
	<title>Swift Commerce</title>
    <!-- Mobile specific metas
    ============================================ -->
    <meta name="viewport" content="width=device-width, initial-scale=1.0, maximum-scale=1.0, user-scalable=no">

    <!-- Favicon
    ============================================ -->

    <link rel="shortcut icon" type="image/png" href="ico/favicon-16x16.png" />


    <!-- Libs CSS
    ============================================ -->
    <link rel="stylesheet" href="css/bootstrap/css/bootstrap.min.css">
    <link href="css/font-awesome/css/font-awesome.min.css" rel="stylesheet">
    <link href="js/datetimepicker/bootstrap-datetimepicker.min.css" rel="stylesheet">
    <link href="js/owl-carousel/owl.carousel.css" rel="stylesheet">
    <link href="css/themecss/lib.css" rel="stylesheet">
    <link href="js/jquery-ui/jquery-ui.min.css" rel="stylesheet">
    <link href="js/minicolors/miniColors.css" rel="stylesheet">

    <!-- Theme CSS
    ============================================ -->
    <link href="css/themecss/so_searchpro.css" rel="stylesheet">
    <link href="css/themecss/so_megamenu.css" rel="stylesheet">
    <link href="css/themecss/so-categories.css" rel="stylesheet">
    <link href="css/themecss/so-listing-tabs.css" rel="stylesheet">
    <link href="css/themecss/so-newletter-popup.css" rel="stylesheet">

    <link href="css/footer/footer1.css" rel="stylesheet">
    <link href="css/header/header1.css" rel="stylesheet">
    <link id="color_scheme" href="css/theme.css" rel="stylesheet">
    <link href="css/responsive.css" rel="stylesheet">

    <!-- Google web fonts
    ============================================ -->
    <link href='https://fonts.googleapis.com/css?family=Roboto:300,400,400i,500,500i,700' rel='stylesheet'
        type='text/css'>
    <style type="text/css">
        body {
            font-family: 'Roboto', sans-serif
        }
    </style>

</head>

<body class="res layout-1">

    <div id="wrapper" class="wrapper-fluid banners-effect-5">


        <!-- Header Container  -->
        <header id="header" class=" typeheader-1">

            <!-- Header center -->
            <div class="header-middle">
                <div class="container">
                    <div class="row">
                        <!-- Logo -->
                        <div class="navbar-logo col-lg-2 col-md-2 col-sm-12 col-xs-12">
                            <<div class="logo"><a href="index.php">
                                <!--<h1 style="color: white;"> GOGO EMPIRE</h1>-->
                                <img src="image/catalog/CBMC-logo-800x132.png" title="Your Store"
                                        alt="Your Store" />
                                </a></div>
                        </div>
                        <!-- //end Logo -->
                        <div class="middle-right col-lg-4 col-md-3 col-sm-6 col-xs-8">
                            <div class="signin-w">
                                <ul class="signin-link blank">
                                <?php 
                                if(!isset($_SESSION['customer_email'])){
                                    echo"";
                                }else{
                                    echo" <li class='log login'> 
                                    <a class='link-lg'>$_SESSION[customer_email]</a>
                                 </li>";
                                }
                                ?>
                                </ul>
                            </div>



                           

                            <div class="signin-w">
                                <ul class="signin-link blank">
                                <?php
                            if(!isset($_SESSION['customer_email'])){
                                echo" <li class='log login'></i> 
                                <a class='link-lg' href='checkout.php'>Login</a>
                             </li>";
                             echo" <li class='log login'></i> 
                             <a class='link-lg' href='checkout.php'>|</a>
                          </li>";
                             echo" <li class='log login'></i> 
                             <a class='link-lg' href='login.html'>Create an Account</a>
                          </li>";
                            }else{
                                echo"<li class='log login'></i> 
                                <a class='link-lg' href='logout.php'>Logout</a>
                             </li>";
                            }
                            ?>
                                </ul>
                            </div>

                        </div>

                    </div>

                </div>
            </div>
            <!-- //Header center -->

            <!-- Header Bottom -->
            <div class="header-bottom hidden-compact">
                <div class="container">
                    <div class="row">

                         <!-- Main menu -->
                         <div class="main-menu col-lg-6 col-md-7 ">
                            <div class="responsive so-megamenu megamenu-style-dev">
                                <nav class="navbar-default">
                                    <div class=" container-megamenu  horizontal open ">
                                        <div class="navbar-header">
                                            <button type="button" id="show-megamenu" data-toggle="collapse"
                                                class="navbar-toggle">
                                                <span class="icon-bar"></span>
                                                <span class="icon-bar"></span>
                                                <span class="icon-bar"></span>
                                            </button>
                                        </div>

                                        <div class="megamenu-wrapper">
                                            <span id="remove-megamenu" class="fa fa-times"></span>
                                            <div class="megamenu-pattern">
                                                <div class="container-mega">
                                                <ul class="megamenu" 
                                                        data-animationtime="250">
                                                        <li class="home hover">
                                                            <a href="index.php">HOME </a>
                                                        </li>
                                                        <li class="home hover">
                                                        <a href="hotels.php">STORES </a>
                                                        </li>
                                                        <li class="home hover">
                                                            <a href="">COLLECTION </a>
                                                        </li>
                                                        <li class="home hover">
                                                            <a href="aboutus.php">ABOUT US </a>
                                                        </li>
                                                        <li class="home hover">
                                                            <a href="contact.php">CONTACT US </a>
                                                        </li>
                                                    </ul>

                                                </div>
                                            </div>
                                        </div>
                                    </div>
                                </nav>
                            </div>
                        </div>
                        <!-- //end Main menu -->

                        <div class="bottom1 menu-vertical col-lg-2 col-md-3 col-sm-3">
                            <div class="responsive so-megamenu megamenu-style-dev ">
                               
                            </div>

                        </div>

                        <!-- Search -->
                        <div class="bottom2 col-lg-7 col-md-6 col-sm-6">
                            <div class="search-header-w">
                               
                            </div>
                        </div>
                        <!-- //end Search -->

                        <!-- Secondary menu -->
                        <div class="bottom3 col-lg-3 col-md-3 col-sm-3">

                            <ul class="wishlist-comp hidden-md hidden-sm hidden-xs">
                               
                                
                            </ul>
                        </div>

                    </div>
                </div>

            </div>
        </header>
        <!-- //Header Container  -->


           <!-- Main Container  -->
           <div class="main-container container">
            <ul class="breadcrumb">
                <li><a href="#"><i class="fa fa-home"></i></a></li>
                <li><a href="#">Checkout</a></li>

            </ul>

            <div class="row">
                <!--Middle Part Start-->
                <div id="content" class="col-sm-12">
                    <h2 class="title">Checkout</h2>
                    <div class="so-onepagecheckout row">
                        <div class="col-left col-sm-12">
                            <div class="panel panel-default">
                                <div class="panel-heading">
                                    <h4 class="panel-title"><i class="fa fa-truck"></i> Select Your dinning Preference
                                    </h4>
                                </div>
                                <div class="panel-body">
                                        <div class="radio">
                                            <form action='foodcheckout.php' method='get'>
                                                <label>
                                                    <input name="pickup" type="radio" value="Pickup" onchange="this.form.submit()"/>Pickup
                                                </label>
                                            </form>
                                        </div>
                                        <div class="radio">
                                            <form>
                                                <label>
                                                    <input name="delivery" type="radio" value="delivery" onchange="this.form.submit()"/>Delivery
                                                </label>
                                            </form>
                                        </div>
                                   

                                </div>
                            </div>
                        </div>
                    </div>
                    <?php

                    if(isset($_GET['pickup'])){
                        
                        echo "<script>window.open('pickuporder.php','_self')</script>";

                    }else if(isset($_GET['delivery'])) {
                        echo "<script>window.open('deliverorder.php','_self')</script>";
                    }else{
                        echo "select dinning preference first";
                    }
                    ?>
                </div>
                <!--Middle Part End -->

            </div>
        </div>
        <!-- //Main Container -->
        

       

        <!-- Footer Container -->
        <footer class="footer-container typefooter-1">

            <hr>

            <div class="footer-middle ">
                <div class="container">
                    <div class="row">
                        <div class="col-lg-3 col-md-3 col-sm-12 col-xs-12 col-style">
                            <div class="infos-footer">
                                <h3 class="modtitle">Contact Us</h3>
                                <ul class="menu">
                                    <li class="adres">
                                        Swift Commerce Sdn Bhd Lot 23 , Lebuh Sultan Mohamed 1 42000 Klang
                                    </li>
                                    <li class="phone">
                                        Tel: 03-3169 6700 / Fax: 03-3176 1271
                                    </li>
                                    <li class="mail">
                                        askme@swiftcommerce.my 
                                    </li>
                                    <li class="time">
                                        Open time: 07:00AM - 5:30PM
                                    </li>
                                </ul>
                            </div>


                        </div>
                        <div class="col-lg-2 col-md-2 col-sm-4 col-xs-12 col-style">
                            <div class="box-information box-footer">
                                <div class="module clearfix">
                                    <h3 class="modtitle">Information</h3>
                                    <div class="modcontent">
                                        <ul class="menu">
                                            <li><a href="aboutus.php">ABOUT US </a></li>
                                            <li><a href="#">FAQ</a></li>
                                            <li><a href="#">Warranty And Services</a></li>
                                            <li><a href="#">Support 24/7 page</a></li>
                                            <li><a href="#">Product Registration</a></li>
                                            <li><a href="#">Product Support</a></li>
                                        </ul>
                                    </div>
                                </div>
                            </div>

                        </div>
                        <div class="col-lg-2 col-md-2 col-sm-4 col-xs-12 col-style">
                            <div class="box-account box-footer">
                                <div class="module clearfix">
                                    <h3 class="modtitle">Extras</h3>
                                    <div class="modcontent">
                                        <ul class="menu">
                                            <li> <a href="aboutus.php"> Gogo Empire </a></li>
                                            <li><a href="#">Collection</a></li>
                                            <li><a href="contact.php">Contact Us</a></li>
                                        </ul>
                                    </div>
                                </div>
                            </div>


                        </div>
                        <div class="col-lg-2 col-md-2 col-sm-4 col-xs-12 col-style">
                            <div class="box-service box-footer">
                                <div class="module clearfix">
                                    <h3 class="modtitle">Services</h3>
                                    <div class="modcontent">
                                        <ul class="menu">
                                            <li><a href="#">Kuala Lumpur</a></li>
                                            <li><a href="#">Selangor</a></li>
                                            <li><a href="#">KL Central</a></li>
                                        </ul>
                                    </div>
                                </div>
                            </div>

                        </div>
                        <div class="col-lg-3 col-md-3 col-sm-12 col-xs-12 col-style">


                        </div>

                        <div class="col-lg-12 col-xs-12 text-center">
                            <img src="image/catalog/demo/payment/payment.png" alt="imgpayment">
                        </div>
                    </div>
                </div>
            </div>

            <!-- Footer Bottom Container -->
            <div class="footer-bottom ">
                <div class="container">
                    <div class="copyright">
                        gogoempire © 2020. All Rights Reserved. 
                    </div>
                </div>
            </div>
            <!-- /Footer Bottom Container -->


            <!--Back To Top-->
            <div class="back-to-top"><i class="fa fa-angle-up"></i></div>
        </footer>
        <!-- //end Footer Container -->

    </div>


    <!-- Cpanel Block -->
    <div id="sp-cpanel_btn" class="isDown visible-lg">
        <i class="fa fa-cog"></i>
    </div>

    <!-- Include Libs & Plugins
	============================================ -->
    <!-- Placed at the end of the document so the pages load faster -->
    <script type="text/javascript" src="js/jquery-2.2.4.min.js"></script>
    <script type="text/javascript" src="js/bootstrap.min.js"></script>
    <script type="text/javascript" src="js/owl-carousel/owl.carousel.js"></script>
    <script type="text/javascript" src="js/themejs/libs.js"></script>
    <script type="text/javascript" src="js/unveil/jquery.unveil.js"></script>
    <script type="text/javascript" src="js/countdown/jquery.countdown.min.js"></script>
    <script type="text/javascript" src="js/dcjqaccordion/jquery.dcjqaccordion.2.8.min.js"></script>
    <script type="text/javascript" src="js/datetimepicker/moment.js"></script>
    <script type="text/javascript" src="js/datetimepicker/bootstrap-datetimepicker.min.js"></script>
    <script type="text/javascript" src="js/jquery-ui/jquery-ui.min.js"></script>


    <!-- Theme files
	============================================ -->


    <script type="text/javascript" src="js/themejs/so_megamenu.js"></script>
    <script type="text/javascript" src="js/themejs/addtocart.js"></script>
    <script type="text/javascript" src="js/themejs/application.js"></script>
    <script type="text/javascript">
     
        // Check if Cookie exists
        if ($.cookie('display')) {
            view = $.cookie('display');
        } else {
            view = 'grid';
        }
        if (view) display(view);
        
    </script>
</body>

<!--  demo.smartaddons.com/templates/html/emarket/category.html by AkrAm, Sat, 20 Apr 2019 20:00:15 GMT -->

</html>

<?php
// function total_fooditems(){

    //: Getting Customer ID...

    

// }


?>



