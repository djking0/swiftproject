-- phpMyAdmin SQL Dump
-- version 4.8.4
-- https://www.phpmyadmin.net/
--
-- Host: 127.0.0.1:3306
-- Generation Time: Jun 03, 2020 at 01:53 PM
-- Server version: 5.7.24
-- PHP Version: 7.2.14

SET SQL_MODE = "NO_AUTO_VALUE_ON_ZERO";
SET AUTOCOMMIT = 0;
START TRANSACTION;
SET time_zone = "+00:00";


/*!40101 SET @OLD_CHARACTER_SET_CLIENT=@@CHARACTER_SET_CLIENT */;
/*!40101 SET @OLD_CHARACTER_SET_RESULTS=@@CHARACTER_SET_RESULTS */;
/*!40101 SET @OLD_COLLATION_CONNECTION=@@COLLATION_CONNECTION */;
/*!40101 SET NAMES utf8mb4 */;

--
-- Database: `gogoshop`
--

-- --------------------------------------------------------

--
-- Table structure for table `about_questions`
--

DROP TABLE IF EXISTS `about_questions`;
CREATE TABLE IF NOT EXISTS `about_questions` (
  `about_id` int(11) NOT NULL AUTO_INCREMENT,
  `about_question` varchar(233) NOT NULL,
  `about_answer` varchar(555) NOT NULL,
  PRIMARY KEY (`about_id`)
) ENGINE=InnoDB AUTO_INCREMENT=5 DEFAULT CHARSET=utf8mb4;

--
-- Dumping data for table `about_questions`
--

INSERT INTO `about_questions` (`about_id`, `about_question`, `about_answer`) VALUES
(3, 'Our Story', 'We run this bussiness in 1988.. & end at mmdadad vishal\r\nLorem ipsum dolor sit amet, consectetuer adipiscing elit. Aenean commodo ligula eget dolor. Aenean massa. Cum sociis natoque penatibus et magnis dis parturient montes, nascetur ridiculus mus. Donec quam felis, ultricies nec, pellentesque eu, pretium quis, sem. Nulla consequat massa quis enim. Donec pede justo, fringilla vel, aliquet nec, vulputate eget, arcu.'),
(4, 'Where are we located?', 'We are base in Sunway Setia jaya, \r\nLorem ipsum dolor sit amet, consectetuer adipiscing elit. Aenean commodo ligula eget dolor. Aenean massa. Cum sociis natoque penatibus et magnis dis parturient montes, nascetur ridiculus mus. Donec quam felis, ultricies nec, pellentesque eu, pretium quis, sem. Nulla consequat massa quis enim. Donec pede justo, fringilla vel, aliquet nec, vulputate eget, arcu.');

-- --------------------------------------------------------

--
-- Table structure for table `brands`
--

DROP TABLE IF EXISTS `brands`;
CREATE TABLE IF NOT EXISTS `brands` (
  `brand_id` int(10) NOT NULL AUTO_INCREMENT,
  `brand_title` text NOT NULL,
  `brand_address` varchar(255) NOT NULL,
  `brand_email` varchar(100) NOT NULL,
  `brand_pass` varchar(100) NOT NULL,
  PRIMARY KEY (`brand_id`)
) ENGINE=InnoDB AUTO_INCREMENT=4 DEFAULT CHARSET=utf8mb4;

--
-- Dumping data for table `brands`
--

INSERT INTO `brands` (`brand_id`, `brand_title`, `brand_address`, `brand_email`, `brand_pass`) VALUES
(1, 'WOODEN', 'One south, Malaysia', 'wooden123@gmail.com', 'Wooden123'),
(2, 'SWIFT', 'Cyberjaya, Selangor', 'swift123@gmail.com', 'Swift123'),
(3, 'SAMSUNG', 'Seri Petalling, KL', 'samsung123@gmail.com', 'Samsung123');

-- --------------------------------------------------------

--
-- Table structure for table `cancelled_orders`
--

DROP TABLE IF EXISTS `cancelled_orders`;
CREATE TABLE IF NOT EXISTS `cancelled_orders` (
  `cancelled_order_id` int(11) NOT NULL AUTO_INCREMENT,
  `customer_id` int(11) NOT NULL,
  `customer_order_no` int(11) NOT NULL,
  `hotel_id` int(11) NOT NULL,
  `order_date` varchar(255) NOT NULL,
  `invoice_no` int(25) NOT NULL,
  `order_description` varchar(255) NOT NULL,
  `due_amount` varchar(50) NOT NULL,
  `pickup_order_date` varchar(255) NOT NULL,
  `pickup_order_time` varchar(255) NOT NULL,
  `order_type` text NOT NULL,
  `order_status` varchar(30) NOT NULL,
  PRIMARY KEY (`cancelled_order_id`)
) ENGINE=InnoDB AUTO_INCREMENT=6 DEFAULT CHARSET=utf8mb4;

--
-- Dumping data for table `cancelled_orders`
--

INSERT INTO `cancelled_orders` (`cancelled_order_id`, `customer_id`, `customer_order_no`, `hotel_id`, `order_date`, `invoice_no`, `order_description`, `due_amount`, `pickup_order_date`, `pickup_order_time`, `order_type`, `order_status`) VALUES
(3, 1, 1, 2, '2020-05-16 17:03:40', 1125297583, '(1) Sambal Udang Petai * 35 = RM35<br>', '37.1', '16-05-2020', '12:00PM to 12:30PM', 'FOOD-PICKUP', 'Cancelled'),
(4, 24, 2, 1, '2020-05-17 14:40:09', 235682998, '(1) Garlic Parmesan Chicken Wings * 55 = RM55<br>(1) Curry Braised Chicken * 45 = RM45<br>(1) Asian Brown Rice Salad * 28 = RM28<br>', '135.68', '20-05-2020', '11:00AM to 11:30AM', 'FOOD-PICKUP', 'Cancelled'),
(5, 28, 1, 1, '2020-05-19 18:34:21', 709461208, '(1) bunny * 55 = RM55<br>', '58.3', '16-05-2020', '01:30PM to 02:00PM', 'FOOD-PICKUP', 'Cancelled');

-- --------------------------------------------------------

--
-- Table structure for table `categories`
--

DROP TABLE IF EXISTS `categories`;
CREATE TABLE IF NOT EXISTS `categories` (
  `cat_id` int(10) NOT NULL AUTO_INCREMENT,
  `cat_title` text NOT NULL,
  `cat_img` text NOT NULL,
  PRIMARY KEY (`cat_id`)
) ENGINE=InnoDB AUTO_INCREMENT=12 DEFAULT CHARSET=utf8mb4;

--
-- Dumping data for table `categories`
--

INSERT INTO `categories` (`cat_id`, `cat_title`, `cat_img`) VALUES
(1, 'SOFA AND CHAIRS', 'id-9-cat-1.jpg'),
(2, 'FURNITURE AND DECOR', 'id-9-cat-2.jpg'),
(3, 'LAMP AND LIGHTING', 'id-9-cat-3.jpg'),
(4, 'SOUND AND LIFE', 'id-9-cat-4.jpg'),
(5, 'APPLIANCE', 'id-9-cat-5.jpg'),
(11, 'CHAIR', 'id-9-cat-1.jpg');

-- --------------------------------------------------------

--
-- Table structure for table `completed_orders`
--

DROP TABLE IF EXISTS `completed_orders`;
CREATE TABLE IF NOT EXISTS `completed_orders` (
  `completed_order_id` int(11) NOT NULL AUTO_INCREMENT,
  `customer_id` int(11) NOT NULL,
  `customer_order_no` int(11) NOT NULL,
  `hotel_id` int(11) NOT NULL,
  `order_date` varchar(255) NOT NULL,
  `invoice_no` int(25) NOT NULL,
  `order_description` varchar(255) NOT NULL,
  `due_amount` varchar(50) NOT NULL,
  `pickup_order_date` varchar(255) NOT NULL,
  `pickup_order_time` varchar(255) NOT NULL,
  `order_status` text NOT NULL,
  `order_type` varchar(30) NOT NULL,
  PRIMARY KEY (`completed_order_id`)
) ENGINE=InnoDB AUTO_INCREMENT=12 DEFAULT CHARSET=utf8mb4;

--
-- Dumping data for table `completed_orders`
--

INSERT INTO `completed_orders` (`completed_order_id`, `customer_id`, `customer_order_no`, `hotel_id`, `order_date`, `invoice_no`, `order_description`, `due_amount`, `pickup_order_date`, `pickup_order_time`, `order_status`, `order_type`) VALUES
(9, 24, 1, 1, '2020-05-17 14:13:31', 1864741449, '(2) Classic Mojito * 23 = RM46<br>(1) Roasted Chicken with Apples and Celery * 70 = RM70<br>(1) Handheld Chicken Pie * 35 = RM35<br>', '160.06', '17-05-2020', '04:30PM to 05:00PM', 'Completed with Payment', 'FOOD-PICKUP'),
(10, 28, 1, 1, '2020-05-19 18:25:59', 1197079272, '(1) Roasted Chicken with Apples and Celery * 70 = RM70<br>(1) Handheld Chicken Pie * 35 = RM35<br>', '111.3', '16-05-2020', '12:30PM to 01:00PM', 'Completed with Payment', 'FOOD-PICKUP'),
(11, 24, 1, 1, '2020-05-20 11:23:52', 577902758, '(2) Garlic Parmesan Chicken Wings * 55 = RM110<br>(3) Barbeque Pulled Sliders * 32 = RM96<br>(1) Roasted Chicken with Apples and Celery * 70 = RM70<br>', '292.56', '21-05-2020', '11:00AM to 11:30AM', 'Completed with Payment', 'FOOD-PICKUP');

-- --------------------------------------------------------

--
-- Table structure for table `contact_page`
--

DROP TABLE IF EXISTS `contact_page`;
CREATE TABLE IF NOT EXISTS `contact_page` (
  `contactid` int(11) NOT NULL AUTO_INCREMENT,
  `contact_link` varchar(255) NOT NULL,
  `contact_address` varchar(255) NOT NULL,
  `contact_phone` varchar(255) NOT NULL,
  `contact_description` varchar(255) NOT NULL,
  PRIMARY KEY (`contactid`)
) ENGINE=InnoDB AUTO_INCREMENT=2 DEFAULT CHARSET=utf8mb4;

--
-- Dumping data for table `contact_page`
--

INSERT INTO `contact_page` (`contactid`, `contact_link`, `contact_address`, `contact_phone`, `contact_description`) VALUES
(1, '3.041398, 101.369672', 'Swift Commerce Sdn Bhd Lot 23 , Lebuh Sultan Mohamed 1 42000 Klang ', 'Tel: 03-3169 6700 / Fax: 03-3176 1271 ', 'Company Bio-data ... \r\nOr write something about company here \r\nKMKMKSMD');

-- --------------------------------------------------------

--
-- Table structure for table `contact_query`
--

DROP TABLE IF EXISTS `contact_query`;
CREATE TABLE IF NOT EXISTS `contact_query` (
  `contact_id` int(11) NOT NULL AUTO_INCREMENT,
  `contact_customer_name` varchar(100) NOT NULL,
  `contact_customer_email` varchar(100) NOT NULL,
  `contact_customer_enquiry` varchar(255) NOT NULL,
  PRIMARY KEY (`contact_id`)
) ENGINE=InnoDB AUTO_INCREMENT=14 DEFAULT CHARSET=utf8mb4;

--
-- Dumping data for table `contact_query`
--

INSERT INTO `contact_query` (`contact_id`, `contact_customer_name`, `contact_customer_email`, `contact_customer_enquiry`) VALUES
(12, 'Mathan', 'mathankumar67@outlook.com', 'I have some issues which are relates with order no # 99.. '),
(13, 'Leo Leong', 'Leoleong123@gmail.com', 'May I know how i return back my parcel. ');

-- --------------------------------------------------------

--
-- Table structure for table `customers`
--

DROP TABLE IF EXISTS `customers`;
CREATE TABLE IF NOT EXISTS `customers` (
  `customer_id` int(10) NOT NULL AUTO_INCREMENT,
  `customer_name` varchar(100) NOT NULL,
  `customer_email` varchar(100) NOT NULL,
  `customer_pass` varchar(100) NOT NULL,
  `customer_country` text NOT NULL,
  `customer_city` text NOT NULL,
  `customer_contact` int(30) NOT NULL,
  `customer_address` text NOT NULL,
  `customer_image` text NOT NULL,
  PRIMARY KEY (`customer_id`)
) ENGINE=InnoDB AUTO_INCREMENT=30 DEFAULT CHARSET=utf8mb4;

--
-- Dumping data for table `customers`
--

INSERT INTO `customers` (`customer_id`, `customer_name`, `customer_email`, `customer_pass`, `customer_country`, `customer_city`, `customer_contact`, `customer_address`, `customer_image`) VALUES
(1, 'Mathan', 'Mathankumar67@outlook.com', 'Mathan123', 'Malaysia', 'Kuala lumpur', 1123108545, '3 elements, Seri Kambangan', ''),
(5, 'Bunny Mangrani', 'bunny.mangrani45@gmail.com', 'Bunny123', 'Malaysia', 'karachi', 2147483647, 'Kehkashan Apartment', 'Boss.jpg'),
(9, 'Eryn gogo', 'eryn123@gmail.com', 'Eryn123', 'Malaysia', 'Kuala lumpur', 1123108545, 'One south Apartment, Cyberjaya', 'Boss.jpg'),
(10, 'Leo Leong', 'Leoleong123@gmail.com', 'Leo123', 'Malaysia', 'Kuala lumpur', 2147483647, 'One south Apartment, Cyberjaya', 'Boss.jpg'),
(21, 'Eryn  Gogo empire', 'eryn345@gmail.com', 'Eryn123', 'Malaysia', 'Kuala lumpur', 1123108545, 'One south', 'E9F08599-2261-43AB-B318-BDEC22F29992.jpeg'),
(23, 'Vishal Vidhani', 'Vishalvidhani123@gmail.com', 'VIshal123', 'Malaysia', 'karachi', 1123244, 'adad', 'bhjeesham.jpg'),
(24, 'Eryn L', 'erynazrin@live.com', '123456', 'Malaysia', 'Pantai Dalam', 176844365, 'B3-4-2, Pantai Hillpark Phase 1', 'A&E Doctor.png'),
(25, 'Mohit Chandwani', 'mohitchandwani123@gmail.com', 'Mohit123', 'Malaysia', 'karachi', 898042342, 'Kehkashan Apartment', 'bhjeesham.jpg'),
(26, 'lucky kumar', 'lucky123@gmail.com', '123456', 'Malaysia', 'karachi', 5656858, 'Kehkashan Apartment', 'bhjeesham.jpg'),
(27, 'Leo Leong L', 'leoleong@test.com', '123456', 'Malaysia', 'Pantai Dalam', 172005180, 'B3-4-2, Pantai Hillpark Phase 1', 'A&E Doctor.png'),
(28, 'vishal vidhani', 'Vishalvidhani125@gmail.com', '123456', 'Malaysia', 'Kuala lumpur', 434234444, 'Kehkashan Apartment', 'tushal.jpg'),
(29, 'Dinesh kumar', 'dinesh890@gmail.com', 'Dinesh123', 'Malaysia', 'karachi', 4343245, 'Kehkashan Apartment', 'vishal.jpg');

-- --------------------------------------------------------

--
-- Table structure for table `customer_foodcart`
--

DROP TABLE IF EXISTS `customer_foodcart`;
CREATE TABLE IF NOT EXISTS `customer_foodcart` (
  `foodcart_id` int(11) NOT NULL AUTO_INCREMENT,
  `fooditem_id` int(11) NOT NULL,
  `foodhotel_id` int(11) NOT NULL,
  `item_name` text NOT NULL,
  `customer_id` int(11) NOT NULL,
  `quantity` int(11) NOT NULL,
  `total_price` int(20) NOT NULL,
  `unit_price` int(20) NOT NULL,
  PRIMARY KEY (`foodcart_id`)
) ENGINE=InnoDB AUTO_INCREMENT=415 DEFAULT CHARSET=utf8mb4;

--
-- Dumping data for table `customer_foodcart`
--

INSERT INTO `customer_foodcart` (`foodcart_id`, `fooditem_id`, `foodhotel_id`, `item_name`, `customer_id`, `quantity`, `total_price`, `unit_price`) VALUES
(395, 78, 23, 'Canterbury Stone Grey Chair', 29, 1, 329, 329),
(396, 85, 23, 'Soft Luxury Sofa', 29, 1, 188, 188),
(411, 99, 1, 'Smoked Salmon', 24, 1, 34, 34),
(414, 43, 1, 'Roasted Chickens with Apples and Celery', 27, 1, 70, 70);

-- --------------------------------------------------------

--
-- Table structure for table `customer_pickuporder`
--

DROP TABLE IF EXISTS `customer_pickuporder`;
CREATE TABLE IF NOT EXISTS `customer_pickuporder` (
  `porder_id` int(11) NOT NULL AUTO_INCREMENT,
  `customer_id` int(11) NOT NULL,
  `customer_order_no` int(10) NOT NULL,
  `hotel_id` int(11) NOT NULL,
  `order_date` timestamp NOT NULL DEFAULT CURRENT_TIMESTAMP ON UPDATE CURRENT_TIMESTAMP,
  `invoiceno` int(10) NOT NULL,
  `order_description` varchar(255) NOT NULL,
  `due_amount` varchar(30) NOT NULL,
  `pickup_order_date` varchar(40) NOT NULL,
  `pickup_order_time` varchar(40) NOT NULL,
  `order_status` varchar(40) NOT NULL,
  PRIMARY KEY (`porder_id`)
) ENGINE=InnoDB AUTO_INCREMENT=82 DEFAULT CHARSET=utf8mb4;

--
-- Dumping data for table `customer_pickuporder`
--

INSERT INTO `customer_pickuporder` (`porder_id`, `customer_id`, `customer_order_no`, `hotel_id`, `order_date`, `invoiceno`, `order_description`, `due_amount`, `pickup_order_date`, `pickup_order_time`, `order_status`) VALUES
(42, 1, 1, 2, '2020-05-17 09:04:28', 1613989681, '(1) Chilli Crab * 78 = RM78<br>', '82.68', '09-05-2020', '01:00PM to 01:30PM', 'Confirm'),
(45, 1, 2, 24, '2020-05-19 10:47:58', 530336988, '(1) 4005 Office Chair  * 44 = RM44<br>', '46.64', '21-05-2020', '12:00PM to 12:30PM', 'Confirm'),
(46, 1, 3, 2, '2020-05-19 19:39:01', 542876312, '(1) Squid in Seafood Sauce * 48 = RM48<br>', '50.88', '09-05-2020', '01:00PM to 01:30PM', 'Confirm'),
(47, 1, 4, 2, '2020-05-19 19:59:05', 490576701, '(1) Vegetable Spring Roll * 18 = RM18<br>', '19.08', '09-05-2020', '01:00PM to 01:30PM', 'Confirm'),
(49, 24, 2, 1, '2020-05-20 03:51:11', 1781321613, '(3) Garlic Parmesan Chicken Wings * 55 = RM165<br>', '174.9', '21-05-2020', '12:30PM to 01:00PM', 'Confirm'),
(50, 24, 3, 1, '2020-05-20 04:09:09', 1716505499, '(1) Roasted Chicken with Apples and Celery * 70 = RM70<br>', '74.2', '23-05-2020', '11:00AM to 11:30AM', 'Confirm'),
(51, 24, 4, 1, '2020-05-20 08:49:34', 632168647, '(2) Roasted Chicken with Apples and Celery * 70 = RM140<br>', '148.4', '22-05-2020', '02:30PM to 03:00PM', 'Confirm'),
(52, 24, 5, 1, '2020-05-20 13:29:18', 1778836366, '(2) Roasted Chicken with Apples and Celery * 70 = RM140<br>', '148.4', '22-05-2020', '01:00PM to 01:30PM', 'Confirm'),
(53, 24, 6, 1, '2020-05-20 15:05:06', 438739534, '(2) Hot Fudge Sundae * 15 = RM30<br>(1) Vegetable Fajitas * 35 = RM35<br>', '68.9', '23-05-2020', '11:00AM to 11:30AM', 'Confirm'),
(54, 24, 7, 1, '2020-05-20 15:47:15', 1437019519, '(1) Garlic Parmesan Chicken Wings * 55 = RM55<br>(2) Roasted Chicken with Apples and Celery * 70 = RM140<br>', '206.7', '26-05-2020', '02:30PM to 03:00PM', 'Confirm'),
(55, 1, 5, 1, '2020-05-20 19:10:09', 615123435, '(1) Garlic Parmesan Chicken Wings * 55 = RM55<br>', '58.3', '02-05-2020', '01:30PM to 02:00PM', 'Confirm'),
(56, 1, 6, 23, '2020-05-20 19:10:44', 1681046501, '(1) Framed Canvas * 399 = RM399<br>', '422.94', '09-05-2020', '11:00AM to 11:30AM', 'Confirm'),
(57, 24, 8, 1, '2020-05-21 02:17:12', 562184887, '(1) Handheld Chicken Pie * 35 = RM35<br>(3) Roasted Chicken with Apples and Celery * 70 = RM210<br>', '259.7', '01-06-2020', '12:00PM to 12:30PM', 'Confirm'),
(58, 24, 9, 1, '2020-05-21 03:08:56', 1624447956, '(1) Roasted Chicken with Apples and Celery * 70 = RM70<br>(1) Barbeque Pulled Sliders * 32 = RM32<br>(2) Asian Brown Rice Salad * 28 = RM56<br>', '167.48', '01-06-2020', '02:00PM to 02:30PM', 'Confirm'),
(59, 24, 10, 1, '2020-05-21 04:25:19', 1193816680, '(1) Roasted Chicken with Apples and Celery * 70 = RM70<br>(2) Vegetable Fajitas * 35 = RM70<br>', '148.4', '01-06-2020', '03:00PM to 03:30PM', 'Confirm'),
(60, 24, 11, 1, '2020-05-21 07:08:44', 357134890, '(1) Roasted Chicken with Apples and Celery * 70 = RM70<br>(2) Stir Fried Chicken Rice * 20 = RM40<br>', '116.6', '27-05-2020', '02:30PM to 03:00PM', 'Confirm'),
(61, 24, 12, 1, '2020-05-21 08:00:07', 320241928, '(1) Vegetable Fajitas * 35 = RM35<br>(3) Roasted Chicken with Apples and Celery * 70 = RM210<br>(1) Garlic Parmesan Chicken Wings * 55 = RM55<br>', '318', '26-05-2020', '02:30PM to 03:00PM', 'Confirm'),
(62, 24, 13, 1, '2020-05-21 13:19:32', 1916162682, '(1) Roasted Chicken with Apples and Celery * 70 = RM70<br>(2) Brocolli Salad with Rice Noodles * 25 = RM50<br>', '127.2', '26-05-2020', '02:00PM to 02:30PM', 'Confirm'),
(63, 27, 1, 1, '2020-05-23 09:20:08', 942334499, '(1) Roasted Chicken with Apples and Celery * 70 = RM70<br>(1) Garlic Parmesan Chicken Wings * 55 = RM55<br>', '132.5', '28-05-2020', '11:00AM to 11:30AM', 'Confirm'),
(64, 1, 7, 23, '2020-05-25 05:26:38', 71906285, '(1) Canterbury Stone Grey Chair * 329 = RM329<br>', '348.74', '11-04-2020', '12:30PM to 01:00PM', 'Confirm'),
(65, 27, 2, 1, '2020-05-25 09:27:27', 1288370976, '(1) Roasted Chickens with Apples and Celery * 70 = RM70<br>', '74.2', '29-05-2020', '11:00AM to 11:30AM', 'Confirm'),
(66, 27, 3, 1, '2020-05-26 06:11:17', 734150943, '(1) Roasted Chickens with Apples and Celery * 70 = RM70<br>', '74.2', '29-05-2020', '11:00AM to 11:30AM', 'Confirm'),
(67, 27, 4, 1, '2020-05-26 06:19:32', 45812670, '(1) Roasted Chickens with Apples and Celery * 70 = RM70<br>', '74.2', '30-06-2020', '11:00AM to 11:30AM', 'Confirm'),
(68, 29, 1, 1, '2020-05-26 19:20:32', 1076693844, '(1) Roasted Chickens with Apples and Celery * 70 = RM70<br>', '74.2', '18-04-2020', '12:00PM to 12:30PM', 'Confirm'),
(69, 29, 2, 23, '2020-05-26 19:21:26', 742611898, '(1) Canterbury Stone Grey Chair * 329 = RM329<br>(1) Soft Luxury Sofa * 188 = RM188<br>', '548.02', '16-05-2020', '01:00PM to 01:30PM', 'Confirm'),
(70, 24, 14, 1, '2020-05-27 01:49:51', 1667551737, '(1) Sambal Udang Petai * 75 = RM75<br>', '79.5', '01-06-2020', '12:30PM to 01:00PM', 'Confirm'),
(71, 24, 15, 1, '2020-05-27 02:03:36', 1574699836, '(1) Roasted Chickens with Apples and Celery * 70 = RM70<br>(2) Handheld Chicken Pie * 35 = RM70<br>', '148.4', '28-05-2020', '01:00PM to 01:30PM', 'Confirm'),
(72, 24, 16, 1, '2020-05-27 02:03:53', 1895660167, '', '0', '28-05-2020', '01:00PM to 01:30PM', 'Confirm'),
(73, 27, 5, 1, '2020-05-27 08:06:14', 270160763, '(1) Roasted Chickens with Apples and Celery * 70 = RM70<br>', '74.2', '21-05-2020', '11:00AM to 11:30AM', 'Confirm'),
(74, 24, 17, 1, '2020-05-27 09:07:58', 562144182, '(1) Roasted Chickens with Apples and Celery * 70 = RM70<br>', '74.2', '02-06-202', '11:00AM to 11:30AM', 'Confirm'),
(75, 27, 6, 1, '2020-05-27 09:50:58', 162827696, '(1) Roasted Chickens with Apples and Celery * 70 = RM70<br>', '74.2', '30-05-2020', '01:30PM to 02:00PM', 'Confirm'),
(76, 1, 8, 2, '2020-05-28 04:40:16', 1245450665, '(1) Vegetable Spring Roll * 18 = RM18<br>', '19.08', '16-05-2020', '12:30PM to 01:00PM', 'Confirm'),
(77, 1, 9, 1, '2020-05-28 10:06:28', 966383020, '(1) Garlic Parmesan Chicken Wings * 55 = RM55<br>', '58.3', '15-05-2020', '12:00PM to 12:30PM', 'Confirm'),
(78, 24, 18, 1, '2020-05-29 02:48:50', 1031117514, '(1) Roasted Chickens with Apples and Celery * 70 = RM70<br>(1) Garlic Parmesan Chicken Wings * 55 = RM55<br>(2) Handheld Chicken Pie * 35 = RM70<br>', '206.7', '31-05-2020', '01:00PM to 01:30PM', 'Confirm'),
(79, 27, 7, 1, '2020-05-29 09:11:02', 1174764249, '(1) Roasted Chickens with Apples and Celery * 70 = RM70<br>', '74.2', '30-05-2020', '01:00PM to 01:30PM', 'Confirm'),
(80, 27, 8, 1, '2020-05-31 07:24:03', 133527887, '(1) Roasted Chickens with Apples and Celery * 70 = RM70<br>(1) Garlic Parmesan Chicken Wings * 55 = RM55<br>', '132.5', '15-05-2020', '11:00AM to 11:30AM', 'Confirm'),
(81, 27, 9, 2, '2020-06-01 11:35:54', 959550220, '(1) Royal Golden Fried Rice * 32 = RM32<br>(1) Squid in Seafood Sauce * 48 = RM48<br>', '84.8', '19-06-2020', '11:00AM to 11:30AM', 'Confirm');

-- --------------------------------------------------------

--
-- Table structure for table `food_category`
--

DROP TABLE IF EXISTS `food_category`;
CREATE TABLE IF NOT EXISTS `food_category` (
  `fcat_id` int(10) NOT NULL AUTO_INCREMENT,
  `food_cat` varchar(30) NOT NULL,
  `cat_image` varchar(233) NOT NULL,
  `cat_type` varchar(40) NOT NULL,
  PRIMARY KEY (`fcat_id`)
) ENGINE=InnoDB AUTO_INCREMENT=29 DEFAULT CHARSET=utf8mb4;

--
-- Dumping data for table `food_category`
--

INSERT INTO `food_category` (`fcat_id`, `food_cat`, `cat_image`, `cat_type`) VALUES
(1, 'Family Meal', '', 'food'),
(3, 'Starter', '', 'food'),
(4, 'Main Course', '', 'food'),
(5, 'Rice', '', 'food'),
(6, 'Noodles', '', 'food'),
(7, 'Vegetables', '', 'food'),
(8, 'Dessert', '', 'food'),
(9, 'Beverage', '', 'food'),
(12, 'SOFA AND CHAIRS', 'id-9-cat-1.jpg', 'product'),
(13, 'DECORATION', 'id-9-cat-2.jpg', 'product'),
(14, 'LAMP AND LIGHTING', 'id-9-cat-3.jpg', 'product'),
(15, 'SOUND AND LIFE', 'id-9-cat-4.jpg', 'product'),
(16, 'APPLIANCES', 'id-9-cat-5.jpg', 'product'),
(17, 'BEDROOM', 'id-9-cat-1.jpg', 'product'),
(21, 'KITCHEN', 'id-9-banner-1.jpg', 'gogo'),
(22, 'HANDMADE', 'id-9-banner-2.jpg', 'gogo'),
(23, 'WATCHES', 'id-9-banner-3.jpg', 'gogo'),
(24, 'LUXURY SOFA', 'id-9-banner-4.jpg', 'gogo'),
(28, 'Dim Sum', '', 'food');

-- --------------------------------------------------------

--
-- Table structure for table `food_company`
--

DROP TABLE IF EXISTS `food_company`;
CREATE TABLE IF NOT EXISTS `food_company` (
  `fcompany_id` int(10) NOT NULL AUTO_INCREMENT,
  `Company_name` varchar(40) NOT NULL,
  `company_address` varchar(255) NOT NULL,
  `company_openinghours` varchar(40) NOT NULL,
  `company_email` varchar(100) NOT NULL,
  `company_pass` varchar(100) NOT NULL,
  `company_type` varchar(30) NOT NULL,
  `company_banner_img` varchar(255) NOT NULL,
  PRIMARY KEY (`fcompany_id`)
) ENGINE=InnoDB AUTO_INCREMENT=34 DEFAULT CHARSET=utf8mb4;

--
-- Dumping data for table `food_company`
--

INSERT INTO `food_company` (`fcompany_id`, `Company_name`, `company_address`, `company_openinghours`, `company_email`, `company_pass`, `company_type`, `company_banner_img`) VALUES
(1, 'Hotel A', '21, Jalan Berangan, Bukit Bintang, 50200 Kuala Lumpur, Wilayah Persekutuan Kuala Lumpur, Malaysia', '', 'hotela@gmail.com', '123456', 'food', 'item-2.jpg'),
(2, 'Hotel B', '43300 Seri Kembangan, Selangor, Malaysia', '', 'hotelb@gmail.com', '123456', 'food', ''),
(21, 'Eryn', 'XYZ', '', 'eryngogo123@admin.com', 'Eryn1234', 'admin', ''),
(23, 'Pelikan Outlet', '', '', 'pelikan@swiftcommerce.my', '123456', 'product', 'item-3.jpg'),
(32, 'GOGO EMPIRE', 'XYZ', '', 'gogoempire123@gmail.com', 'gogo1234', 'gogo', 'item-1.jpg'),
(33, 'Swift Commerce', 'XYZ', '', 'admin@swiftcommerce.my', '123456', 'admin', '');

-- --------------------------------------------------------

--
-- Table structure for table `food_items`
--

DROP TABLE IF EXISTS `food_items`;
CREATE TABLE IF NOT EXISTS `food_items` (
  `item_id` int(10) NOT NULL AUTO_INCREMENT,
  `fcat_id` int(10) NOT NULL,
  `fcompany_id` int(10) NOT NULL,
  `date` timestamp NOT NULL DEFAULT CURRENT_TIMESTAMP ON UPDATE CURRENT_TIMESTAMP,
  `item_title` text NOT NULL,
  `item_img` text NOT NULL,
  `item_price` int(15) NOT NULL,
  `item_desc` text NOT NULL,
  `youtube_link` varchar(244) NOT NULL,
  `status` text NOT NULL,
  `item_discounted_price` varchar(11) NOT NULL,
  `item_avalability` varchar(30) NOT NULL,
  `item_type` varchar(40) NOT NULL,
  `top_product` text NOT NULL,
  `trending` varchar(20) NOT NULL,
  PRIMARY KEY (`item_id`)
) ENGINE=InnoDB AUTO_INCREMENT=100 DEFAULT CHARSET=utf8mb4;

--
-- Dumping data for table `food_items`
--

INSERT INTO `food_items` (`item_id`, `fcat_id`, `fcompany_id`, `date`, `item_title`, `item_img`, `item_price`, `item_desc`, `youtube_link`, `status`, `item_discounted_price`, `item_avalability`, `item_type`, `top_product`, `trending`) VALUES
(43, 1, 1, '2020-05-25 08:15:16', 'Roasted Chickens with Apples and Celery', 'partridge-in-cider-with-apples-celery.jpg', 70, 'A classic holiday recipe, good any time of the year. Stuff chicken with fresh carrots, celery, red potatoes, onions and thyme for a hearty meal the whole family will love. And, for a festive presentation, serve with dirty rice on a platter.', 'https://www.youtube.com/watch?v=3V2R4PO2Ygk', 'on', '1', 'NEW', 'food', '', ''),
(44, 1, 1, '2020-05-19 10:08:43', 'Garlic Parmesan Chicken Wings', 'Recipes_GarlicParmesanWings_Hero_709x758.png', 55, 'Savory wings with a one-two punch of garlic and parmesan. Perfect for a tailgate, dinner party, or a weeknight treat.', 'https://www.youtube.com/watch?v=fZtpZc2RDh0', 'on', 'NO', 'NEW', 'food', '', ''),
(45, 3, 1, '2020-05-19 10:08:45', 'Handheld Chicken Pie', 'Thumb_420x420_HandheldPotPies.png', 35, 'With a serving size of 12, this dish could be a party appetizer or a weeknight meal with the family. This savory pocket of tender chicken, potatoes, and spices will have everyone coming back for seconds...maybe even thirds.', 'https://www.youtube.com/watch?v=t8E89lrV36A', 'on', 'NO', 'PROMO', 'food', '', ''),
(46, 3, 1, '2020-05-19 10:08:48', 'Barbeque Pulled Sliders', 'Hero_709x758_BBQ_Sliders-1.png', 32, 'These family friendly sandwiches may be small but their flavor is huge. Ready in just 40 minutes and feeding 6 people, they are the perfect dinner for the whole family.', 'https://www.youtube.com/watch?v=WkxbteTZnlg', 'on', 'NO', 'NEW', 'food', '', ''),
(47, 4, 1, '2020-05-19 10:08:55', 'Vegetable Fajitas', 'Recipes_VegFajitaRice_Thumb_420x420-1.png', 35, 'Using a baking sheet and parchment paper for easy cleanup, this recipe is the perfect weeknight dinner. Making this recipe for one? No problem. This spicy, Mexican inspired recipe is great for meal prep, too!', 'https://www.youtube.com/watch?v=HWvK6bjrxM8', 'on', 'NO', 'NEW', 'food', '', ''),
(48, 4, 1, '2020-05-19 10:09:00', 'Curry Braised Chicken', 'PanangCurryBraisedChicken_Thumbnail.jpg', 45, 'Mixing curry paste, coconut milk and ginger with our delicious chicken thighs creates the perfect meal. Both exotic and comforting, this dish is perfect for a dinner party or eating with family and friends.', 'https://www.youtube.com/watch?v=aPbs8zimtl4', 'on', 'NO', 'NEW', 'food', '', ''),
(49, 5, 1, '2020-05-19 10:09:03', 'Asian Brown Rice Salad', 'Recipes_Asian_Chicken_and_Brown_Rice_Thumb_420x420.jpg', 28, 'Vibrant and colorful, this healthy salad is perfect as a side or main dish. Itâ€™s also a great way to use rice to create a light, yet filling meal.', '', 'on', 'NO', 'NEW', 'food', '', ''),
(50, 5, 1, '2020-05-19 10:09:06', 'Stir Fried Chicken Rice', 'Recipes_Chicken_Stir_Fry_Thumb_420x420.jpg', 20, 'Stir-fry is a great way to put a complete meal and more veggies on the table. Contains chicken, spices, healthy vegetables and cooked rice. ', '', 'on', 'NO', 'PROMO', 'food', '', ''),
(51, 6, 1, '2020-05-19 10:09:08', 'Brocolli Salad with Rice Noodles', 'Recipes_Chicken_and_Broccoli_Salad_with_Rice_Noodles_Thumb_420x420.jpg', 25, 'Introducing a flavorful Asian take on the classic pairing of chicken and broccoli. Itâ€™s also low in fat and calories, as well as a great source of protein.', '', 'on', 'NO', 'NEW', 'food', '', ''),
(52, 6, 1, '2020-05-19 10:09:11', 'Garlic Chicken Farfelle with Roasted Vegetables', 'Recipes_Garlic_Chicken_Farfalle_with_Broccoli_Thumb_420x420.jpg', 26, 'Mediterranean meal that is both delicious and nutritious.', '', 'on', 'NO', 'PROMO', 'food', '', ''),
(53, 7, 1, '2020-05-19 10:09:13', ' Roasted Eggplant and Brown Rice Bowl with Turmeric Tahini', 'k_archive_58eb6552e9c56c10d93f60630b92f86801b48fa7.jpg', 22, 'This vegan, gluten-free menu features caramelized and tender eggplant, juicy pomegranate seeds, chickpeas, fresh herbs, and nutty tahini sauce all in one happy bowl.', '', 'on', 'NO', 'NEW', 'food', '', ''),
(54, 7, 1, '2020-05-19 10:09:15', 'Kung Pao Cauliflower', 'k_archive_ebd8c555cc45b5316be20db27af085423ee3b8f8.jpg', 19, 'Give your takeout favorite a meatless spin by swapping out the chicken for cauliflower â€” we guarantee that itâ€™s no less satisfying.', '', 'on', 'NO', 'NEW', 'food', '', ''),
(55, 8, 1, '2020-05-19 10:09:19', 'Hot Fudge Sundae', 'CCF_HotFudgeSundae.jpg', 15, 'The Best Hot Fudge Anywhere. Topped with Whipped Cream and Almonds.', '', 'on', 'NO', 'NEW', 'food', '', ''),
(56, 8, 1, '2020-05-19 10:09:22', 'Godiva Chocolate Brownie Sundae', 'CCF_GodivaChocBrownieSundae.jpg', 32, 'Our own Fabulous Godiva Chocolate Brownie, Vanilla Ice Cream, Hot Fudge, Whipped Cream and Toasted Almonds.', '', 'on', 'NO', 'NEW', 'food', '', ''),
(57, 9, 1, '2020-05-19 10:09:24', 'Long Island Strawberry Tea', 'Ling-island-strawberry-tea-Long-island-mango-tea.jpg', 13, 'Orange juice, strawberry purÃ©e, house-made sweet & sour and Coke and a garnish of lemon wedge.\r\n\r\n', '', 'on', 'NO', 'PROMO', 'food', '', ''),
(58, 9, 1, '2020-05-19 10:09:30', 'Classic Mojito', 'Mojito.jpg', 23, 'BACARDIÂ® Rum with lime juice, fresh mint leaves and MoninÂ® Mojito Syrup topped with SpriteÂ®.', '', 'on', 'NO', 'NEW', 'food', '', ''),
(59, 9, 1, '2020-05-19 10:09:34', 'Perrier Sparkling Mineral Water', 'download (7).jpg', 13, 'Perrier is a French brand of natural bottled mineral water captured at the source in VergÃ¨ze, located in the Gard dÃ©partement. Perrier is best known for its naturally occurring carbonation, distinctive green bottle, and higher levels of carbonation than its peers.', '', 'on', 'NO', 'PROMO', 'food', '', ''),
(60, 3, 2, '2020-05-19 10:09:36', 'Har Gao Dumpling', 'download (8).jpg', 15, 'Classic Prawn Har Gao Dumpling', '', 'on', 'NO', 'NEW', 'food', '', ''),
(61, 1, 2, '2020-05-28 18:12:07', 'Squid in Seafood Sauce', 'dad.jpg', 48, 'Freshly sliced squid in seafood sauce.', '', 'on', 'NO', 'NEW', 'food', '', ''),
(62, 1, 2, '2020-05-19 10:09:41', 'Chilli Crab', 'download (9).jpg', 78, 'Spicy Chill Crab with Mantao Buns perfect for family sharing.', '', 'on', 'NO', 'NEW', 'food', '', ''),
(63, 3, 2, '2020-05-19 10:09:44', 'Vegetable Spring Roll', 'download (10).jpg', 18, 'Filled with finely sliced cabbage, mushroom and carrots.', '', 'on', 'NO', 'NEW', 'food', '', ''),
(64, 4, 2, '2020-05-19 10:46:02', 'Scallop Cheong Fun', 'download (11).jpg', 20, 'Steamed scallop cheong fun with vegetables and shrimp paste.', '', 'on', 'NO', 'NEW', 'food', '', ''),
(65, 4, 2, '2020-05-19 10:46:05', 'Royal Golden Fried Rice', 'download (12).jpg', 32, 'Fried rice with prawn and ebiko.', '', 'on', 'NO', 'NEW', 'food', '', ''),
(66, 6, 2, '2020-05-19 10:46:08', 'Braised Seafood Yee Mee', 'images (1).jpg', 28, 'Braised with prawn and red snapper in superior broth.', '', 'on', 'NO', 'NEW', 'food', '', ''),
(67, 6, 2, '2020-05-28 18:09:33', 'Hong Kong Crispy Noodle', 'dad.jpg', 25, 'Deep Fried Shang Mee with seafood toppings.', '', 'on', 'NO', 'NEW', 'food', '', ''),
(68, 7, 2, '2020-05-19 10:46:16', 'Stir Fried Hong Kong Kai Lan', 'download (14).jpg', 25, 'Stir Fried Gailan with sliced mushroom.', '', 'on', 'NO', 'NEW', 'food', '', ''),
(69, 7, 2, '2020-05-19 10:46:19', 'Stir Fried Siew Pak Choy', 'download (15).jpg', 23, 'Bok Choy with garlic and oyster sauce.', '', 'on', 'NO', 'NEW', 'food', '', ''),
(70, 8, 2, '2020-05-19 10:46:21', 'Mango Pomelo Delight', 'download (16).jpg', 18, 'Fresh chilled mango stopped with plucked pomelo.', '', 'on', 'NO', 'PROMO', 'food', '', ''),
(71, 8, 2, '2020-05-19 10:46:25', 'Ice Sea Coconut ', 'download (17).jpg', 15, 'Sea coconut, lychee, in lime infusion,', '', 'on', 'NO', 'NEW', 'food', '', ''),
(72, 8, 2, '2020-05-19 10:46:33', 'Snowskin Durian Mochi', 'download (18).jpg', 20, 'Delicate snowskin mochi filled with premium durian.', '', 'on', 'NO', 'NEW', 'food', '', ''),
(77, 13, 23, '2020-05-25 07:49:25', 'Framed Canvas bunny', 'Framed-Canvas-100x140cm-PinkBlue-front-768x768.jpg', 398, 'Dimensions	\r\n100W x 140H cm\r\n\r\nFinish	\r\nClassic Printing, 30% Oil Painting\r\n\r\nFrame Color	\r\nWhite', '', 'on', '1', 'IN STOCK', 'product', 'yes', 'New Arrival'),
(78, 12, 23, '2020-05-25 04:31:06', 'Canterbury Stone Grey Chair', 'canterbury-stone-grey-front-500x500.jpg', 329, 'Width (cm)	\r\n56\r\n\r\nDepth (cm)	\r\n51\r\n\r\nHeight (cm)	\r\n90\r\n\r\nSeat Height (cm)	\r\n48\r\n\r\nLegs Materials	\r\nSolid Wood\r\n\r\nFabric Composition	\r\nPremium Polyester\r\n\r\nAssembly	\r\nLegs to be fitted', 'https://www.youtube.com/watch?v=7B5a2uYFAjM', 'on', '', 'NEW', 'product', 'yes', 'New Arrival'),
(79, 14, 23, '2020-05-25 04:31:09', 'Macaw Brass Pear Shape Pendant Lamp', 'MACAW-BRASS-PEAR-SHAPE-PENDANT-LAMP.jpg', 229, 'Diameter (cm)	\r\n19\r\n\r\nHeight (cm)	\r\n30\r\n\r\nMaterials	\r\nMetal\r\n\r\nColor	\r\nBrass\r\n\r\nBulb Base Type	\r\nE27\r\n\r\nBulb Wattage (w)	\r\n60\r\n\r\nBulb Quantity	\r\n1\r\n\r\nBulb included?	\r\nNo', 'https://www.youtube.com/watch?v=VSz8w1hm6Vk', 'on', '', 'IN STOCK', 'product', 'yes', 'New Arrival'),
(80, 15, 23, '2020-05-25 07:43:34', 'Clara Tv Cabinet', 'clara-tv-cabinet-front-3-768x768.jpg', 1899, 'Width (cm)	\r\n200\r\n\r\nDepth (cm)	\r\n40\r\n\r\nHeight (cm)	\r\n50\r\n\r\nMaterials	\r\nAmerican Ash Veneer, Medium-density Fibreboard (MDF)\r\n\r\nFrame Materials	\r\nSolid Wood\r\n\r\nLegs Materials	\r\nEpoxy Powder Coated Metal\r\n\r\n', 'https://www.youtube.com/watch?v=-uVAGurv07w', 'on', '', 'IN STOCK', 'product', 'yes', 'Daily Discover'),
(81, 16, 23, '2020-05-25 07:51:26', 'Severin Expresso Maker', 'images (2).jpg', 3200, 'Be your own barista - with the espresso maker Espresa from SEVERIN.\r\n\r\nYour coffee moment already starts with the right preparation? Then Espresa is exactly the right coffee partner for you. With the sieve carrier machine you prepare single or double espresso like a professional barista. Thanks to the integrated milk foam nozzle, you can also conjure up all kinds of coffee classics - from cappuccino to latte macchiato to flat white.\r\n\r\nSince the Espresa Plus espresso maker comes with a high-quality barista starter set, nothing stands in the way of your coffee experiments.\r\n', 'https://www.youtube.com/watch?v=ecmaHcuxkdY', 'on', '', 'IN STOCK', 'product', '', ''),
(82, 17, 23, '2020-05-28 18:39:25', 'Brunell King Bed', 'bed.jpg', 1400, 'Width (cm)	\r\n205\r\n\r\nDepth (cm)	\r\n201\r\n\r\nHeight (cm)	\r\n99\r\n\r\nMattress Size (cm)	\r\nFit for King Size: 182 X 190\r\n\r\nFrame Materials	\r\nSolid Wood, Plywood\r\n\r\nLegs Materials	\r\nSolid Wood\r\n\r\nSlats Material	\r\nPlywood\r\n\r\nFabric Composition	\r\nPolyester Fabric\r\n\r\nWarranty	\r\n1 Year on Frame & Bed Slat*\r\n\r\nAssembly	\r\nFull assembly required\r\n\r\nNote	\r\nPrice Excludes Mattress\r\n\r\n', '', 'on', '', 'IN STOCK', 'product', 'yes', 'Daily Discover'),
(85, 12, 23, '2020-05-25 05:25:16', 'Soft Luxury Sofa', 'luxury_sofa.jpg', 188, 'Comfortable and luxury Sofa', '', 'on', '', 'NEW', 'product', 'yes', 'Trending Search'),
(86, 12, 23, '2020-05-25 05:23:01', '4005 Office Chair ', 'officechair.jpg', 99, 'metal chair', '', 'on', '', 'IN STOCK', 'product', '', 'Trending Search'),
(87, 12, 23, '2020-05-28 18:15:06', 'Chair', 'buuu.jpg', 44, 'Casual Chair', '', 'on', '', 'IN STOCK', 'product', '', 'Trending Search'),
(90, 21, 32, '2020-05-25 06:54:27', 'Kitchen Metal Stand', 'kitchen1.jpg', 99, 'kitchen Metal Stand', '', 'on', '1', 'NEW', 'gogo', '', ''),
(91, 23, 32, '2020-05-25 06:04:49', 'Man Watch', 'watche.jpg', 44, 'Man watch', '', 'on', '', 'NEW', 'gogo', '', ''),
(93, 14, 23, '2020-05-25 07:51:32', '4005 Office Chair ', 'row.jpg', 99, 'dadad', '', 'on', '', 'NEW', 'product', '', 'Daily Discover'),
(95, 1, 1, '2020-05-26 19:43:30', 'Sambal Udang Petai', 'mmmjj.jpg', 75, 'dad', 'https://www.youtube.com/watch?v=ZFSj0rAze2s', 'on', '10', 'NEW', 'food', '', ''),
(97, 4, 2, '2020-05-28 20:35:14', 'Mixed Stray', 'nnn.jpg', 32, 'smndaoncoadnnf', '', 'on', '0', 'NEW', 'food', '', ''),
(98, 3, 1, '2020-05-28 20:44:49', 'Bunny', 'lll.jpg', 35, 'oandoads tasty and healthy', '', 'on', '0', 'IN STOCK', 'food', '', ''),
(99, 3, 1, '2020-05-29 06:09:24', 'Smoked Salmon', 'download.jfif', 34, 'Avocado Salad, Grapefruit, etc.', '', 'on', '1', 'IN STOCK', 'food', '', '');

-- --------------------------------------------------------

--
-- Table structure for table `otp_expiry`
--

DROP TABLE IF EXISTS `otp_expiry`;
CREATE TABLE IF NOT EXISTS `otp_expiry` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `otp` int(11) NOT NULL,
  `is_expired` int(11) NOT NULL,
  `create_at` timestamp NOT NULL DEFAULT CURRENT_TIMESTAMP ON UPDATE CURRENT_TIMESTAMP,
  PRIMARY KEY (`id`)
) ENGINE=MyISAM DEFAULT CHARSET=latin1;

-- --------------------------------------------------------

--
-- Table structure for table `products`
--

DROP TABLE IF EXISTS `products`;
CREATE TABLE IF NOT EXISTS `products` (
  `product_id` int(10) NOT NULL AUTO_INCREMENT,
  `cat_id` int(10) NOT NULL,
  `brand_id` int(10) NOT NULL,
  `date` timestamp NOT NULL DEFAULT CURRENT_TIMESTAMP ON UPDATE CURRENT_TIMESTAMP,
  `product_title` text NOT NULL,
  `product_img1` text NOT NULL,
  `product_img2` text NOT NULL,
  `product_img3` text NOT NULL,
  `product_price` int(15) NOT NULL,
  `product_desc` text NOT NULL,
  `status` text NOT NULL,
  `product_youtubelink` varchar(600) NOT NULL,
  `product_discounted_percentage` int(11) NOT NULL,
  `product_availability` varchar(30) NOT NULL,
  `product_rating` decimal(30,0) NOT NULL,
  PRIMARY KEY (`product_id`)
) ENGINE=InnoDB AUTO_INCREMENT=24 DEFAULT CHARSET=utf8mb4;

--
-- Dumping data for table `products`
--

INSERT INTO `products` (`product_id`, `cat_id`, `brand_id`, `date`, `product_title`, `product_img1`, `product_img2`, `product_img3`, `product_price`, `product_desc`, `status`, `product_youtubelink`, `product_discounted_percentage`, `product_availability`, `product_rating`) VALUES
(18, 1, 2, '2020-04-05 02:43:51', 'Shaped modular sofa ', '1_11.jpg', '1_11.jpg', '1_11.jpg', 230, '<p>L-Shaped Modular Sofa</p>\r\n<p>The Sofa covers the combination of 2 pieces</p>', 'on', 'https://www.youtube.com/watch?v=ZFSj0rAze2s', 20, 'IN STOCK', '4'),
(19, 1, 1, '2020-04-05 02:45:55', 'Elastic Armchair Office ', '6_19.jpg', '6_19.jpg', '6_19.jpg', 210, '<p>Computer Chair Slip Cover</p>\r\n<p>Comfortable Office chair</p>', 'on', 'https://www.youtube.com/watch?v=ZFSj0rAze2s', 10, 'TREND', '4'),
(20, 2, 1, '2020-04-05 02:47:38', 'Mesh Office Chair With Metal Leg', '3_20.jpg', '3_20.jpg', '3_20.jpg', 80, '<p>Adjustable height</p>\r\n<p>Breathable and Comfotable Mash</p>', 'on', 'https://www.youtube.com/watch?v=ZFSj0rAze2s', 10, 'NEW', '4'),
(21, 3, 3, '2020-04-05 22:58:48', 'Waterproof Up Down Wall Light', '12_16.jpg', '12_16.jpg', '12_16.jpg', 80, '<p>Suitable for outdoor and indoors</p>\r\n<p>suitable for stair coridoor</p>', 'on', 'https://www.youtube.com/watch?v=ZFSj0rAze2s', 10, 'IN STOCK', '4'),
(22, 3, 3, '2020-04-06 00:31:41', 'LED Glare Flashlight', '10_22.jpg', '10_22.jpg', '10_22.jpg', 99, '<p>Easy to Use&nbsp;</p>\r\n<p>Reliable</p>', 'on', 'https://www.youtube.com/watch?v=ZFSj0rAze2s', 10, 'Choose', '4'),
(23, 2, 2, '2020-04-06 01:21:11', 'Bunny Mangrani', '3_20.jpg', '3_20.jpg', '3_20.jpg', 44, '<p>dad</p>', 'on', 'https://www.youtube.com/watch?v=ZFSj0rAze2s', 20, 'NEW', '4');

-- --------------------------------------------------------

--
-- Table structure for table `registered_users`
--

DROP TABLE IF EXISTS `registered_users`;
CREATE TABLE IF NOT EXISTS `registered_users` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `email` varchar(30) NOT NULL,
  PRIMARY KEY (`id`)
) ENGINE=MyISAM AUTO_INCREMENT=7 DEFAULT CHARSET=latin1;

--
-- Dumping data for table `registered_users`
--

INSERT INTO `registered_users` (`id`, `email`) VALUES
(6, 'bunny.mangrani45@gmail.com');

-- --------------------------------------------------------

--
-- Table structure for table `reviews`
--

DROP TABLE IF EXISTS `reviews`;
CREATE TABLE IF NOT EXISTS `reviews` (
  `rate` varchar(40) NOT NULL,
  `comments` varchar(400) NOT NULL,
  `curr_time` varchar(50) NOT NULL,
  `email` varchar(50) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4;

--
-- Dumping data for table `reviews`
--

INSERT INTO `reviews` (`rate`, `comments`, `curr_time`, `email`) VALUES
('4 and a half', 'The Webiste is Good', '19-12-05 11:22:19', 'Mathankumar199@gmail.com'),
('3 and a half', 'Your Work is pretty Good, Good Work Mathan. ', '19-12-05 11:41:08', 'Leoleong123@gmail.com'),
('3 and a half', 'dasdasd', '19-12-07 02:10:28', 'Mathan15@gmail.com'),
('2 and a half', 'dadad', '19-12-14 11:17:29', 'Leoleong123@gmail.com'),
('2 and a half', 'dasdasd', '19-12-24 09:58:07', 'jimmy@gmail.com'),
('3', 'MMMM', '19-12-27 07:57:21', 'Eryn@gmail.com'),
('2 and a half', 'adasd', '20-01-30 01:07:15', 'jaisy@gmail.com');
COMMIT;

/*!40101 SET CHARACTER_SET_CLIENT=@OLD_CHARACTER_SET_CLIENT */;
/*!40101 SET CHARACTER_SET_RESULTS=@OLD_CHARACTER_SET_RESULTS */;
/*!40101 SET COLLATION_CONNECTION=@OLD_COLLATION_CONNECTION */;
