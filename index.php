<?php
session_start();
include("includes/db.php");
include("functions/functions.php");

?>

<!DOCTYPE html>
<html lang="en">
  <head>
	<link rel="icon" href="/uploads/2s/website/001/550/731/1550731994_5c6e4ada36b0a.jpg">
    <meta charset="UTF-8" />
    <meta name="viewport" content="width=device-width, initial-scale=1.0" />
    <meta http-equiv="X-UA-Compatible" content="ie=edge" />
    <title>Swift Commerce</title>



    

    <!-- Bootstrap CDN -->
    <link
      rel="stylesheet"
      href="https://stackpath.bootstrapcdn.com/bootstrap/4.3.1/css/bootstrap.min.css"
      integrity="sha384-ggOyR0iXCbMQv3Xipma34MD+dH/1fQ784/j6cY/iJTQUOhcWr7x9JvoRxT2MZw1T"
      crossorigin="anonymous"
    />

    <!--  Font Awesome CDN -->
    <script src="https://kit.fontawesome.com/23412c6a8d.js"></script>

    <!-- Slick Slider -->
    <link
      rel="stylesheet"
      type="text/css"
      href="//cdn.jsdelivr.net/npm/slick-carousel@1.8.1/slick/slick.css"
    />

    <!-- Custom Stylesheet -->
    <link rel="stylesheet" href="./css/style.css" />
    <link rel="stylesheet" href="./css/viewproduct.css" />
  </head>
  <body>
    <!-- header -->

    <header>
      <div class="container">
        <div class="row">
          <div class="col-md-4 col-sm-12 col-12">
            <div class="btn-group">
              <button
                class="btn border dropdown-toggle my-md-4 my-2"
                data-toggle="dropdown"
                aria-haspopup="true"
                aria-expanded="false"
              >
                ENG
              </button>
               <div class="dropdown-menu">
                <a href="#" class="dropdown-item">Bhasa - Malaysia</a>
              </div> 
            </div>
          </div>
          <div class="col-md-4 col-12 text-center">
            <!--<h2 class="my-md-3 site-title text-white">Gogo Empire </h2>-->
            <div class="logo"><a href=""><img src="image/catalog/CBMC-logo-800x132.png" height="60" width= "300" title="Your Store"
                                        alt="Your Store" /></div>
          </div>
          <div class="col-md-4 col-12 text-right">
            <p class="my-md-4 header-links">
              <?php
                if(!isset($_SESSION['customer_email'])){
                echo"";
                }else{
                  
                  echo"<a href='' class='px-1'> $_SESSION[customer_email] </a>";
                  echo"<a class='px-2' style='color:white;'>|</a>";
                }
              ?>
              <?php
                if(!isset($_SESSION['customer_email'])){
                echo"<a href='checkout.php' class='px-2'>login</a>";
                echo"<a class='px-2' style='color:black;'>|</a>";
                echo"<a href='register.php' class='px-2'>Create an Account</a>";
                }else{
                  echo"<a href='logout.php' class='px-2'>logout</a>";
                }
              ?>
            </p>
          </div>
        </div>
      </div>

      <div class="container-fluid p-0 blue-bg">
        <nav class="navbar navbar-expand-lg navbar-light">
          <button
            class="navbar-toggler"
            type="button"
            data-toggle="collapse"
            data-target="#navbarNav"
            aria-controls="navbarNav"
            aria-expanded="false"
            aria-label="Toggle navigation"
          >
            <span class="navbar-toggler-icon"></span>
          </button>
          <div class="collapse navbar-collapse" id="navbarNav">
            <ul class="navbar-nav">
              <li class="nav-item active">
                <a href="index.php" class="nav-link" >HOME <span class="sr-only">(current)</span></a>
              </li>
              <li class="nav-item">
                <a href="hotels.php" class="nav-link" > COLLECTION</a>
              </li>
              <li class="nav-item">
                <a href="hotels.php" class="nav-link" > STORES</a>
              </li>
              <li class="nav-item">
                <a href="aboutus.php" class="nav-link" > ABOUT US</a>
              </li>
              <li class="nav-item">
              <a href="contact.php" class="nav-link" >CONTACT US</a>
              </li>
            </ul>
          </div>
          <!-- <div class="navbar-nav">
            <form class="form-inline my-2 my-lg-0" method="get" action="results.php" enctype="multipart/form-data">
                  <input class="form-control mr-sm-2" type="search" placeholder="Search a Product" aria-label="Search" name="user_query"/>
                  <button class="btn btn-outline-info my-2 my-sm-0" type="submit">Search</button>
                  </form>
          </div> -->
        </nav>
      </div>
    </header>

    <!-- /header -->

    <!-- Main Section   -->

    <main>
      <!--- First Slider -->
      <div class="container-fluid p-0">
        <div class="site-slider">
          <div class="slider-one">

            <?php 

            $query = "Select * from food_company where company_banner_img != ''";
            $run_query = mysqli_query($con, $query);
            

            while($row_query=mysqli_fetch_array($run_query)){

              $company_id = $row_query['fcompany_id'];
              $company_banner_img = $row_query['company_banner_img'];
              $company_name = $row_query['Company_name'];

              echo "<div class='slide'>
              <a href= 'food.php?foodcompany=$company_id' ><img src='./admin_area/homepage_images/$company_banner_img' class='img-fluid' alt='$company_name'/> </a>
            </div>";
            }

            ?>
          </div>
          <div class="slider-btn">
            <span class="prev position-top"
              ><i class="fas fa-chevron-left"></i
            ></span>
            <span class="next position-top right-0"
              ><i class="fas fa-chevron-right"></i
            ></span>
          </div>
        </div>
      </div>
      <!--- /First Slider -->

      <!-- Extra -->

      
      <div class="container-fluid">
        <div class="site-slider-two px-md-4">
          <div class="row slider-two text-center">

          <?php

           $get_cats = "select * from food_category where cat_type = 'product'";
           $run_cats = mysqli_query($con, $get_cats);

           while($row_cats=mysqli_fetch_array($run_cats)){
             $cat_id = $row_cats['fcat_id'];
             $cat_title = $row_cats['food_cat'];
             $cat_img = $row_cats['cat_image'];

             echo "<div class='col-md-2 product pt-md-5 pt-4'>
             <img src='./admin_area/homepage_images/$cat_img' alt='' />
            
            <form action='displayproductsbycat.php' method='post'>
             <input type='hidden' name='category_id' value='$cat_id'>
             <button  type='submit'  name='viewallproducts_btn' class='border site-btntwo btn-spantwo'>$cat_title</button>
             </form>
             
           </div>";

             ?>
            <!-- <div class="col-md-2 product pt-md-5 pt-4">
              <img src="./admin_area/homepage_images/<?php echo"$cat_img"?>" alt="" />
             
             <form action="displayproductsbycat.php" method="post">
              <input type="hidden" name="category_id" value="<?php  echo "$cat_id" ?>">
              <button  type="submit" name="viewallproducts_btn" class="border site-btntwo btn-spantwo"><?php echo"$cat_title"?></button>
              </form>
              
            </div> -->
            <?php
           }
          ?>
          </div>
          <div class="slider-btn">
            <span class="prev position-top"
              ><i class="fas fa-chevron-left fa-2x text-secondary"></i
            ></span>
            <span class="next position-top right-0"
              ><i class="fas fa-chevron-right fa-2x text-secondary"></i
            ></span>
          </div>
        </div>
      </div>

      <!-- /Extra -->
      
      <hr class="hr" />

      <!-- Features Section -->

      <div class="container text-center">
        <div class="features">
          <h1>Our Top PRODUCTS</h1>
          <p class="text-secondary">
          Here below are top Products of Gogo Empire Website
          </p>
        </div>
      </div>

      <!-- Features third Slider -->
      <div class="container-fluid">
        <div class="site-slider-three px-md-4">
          <div class="slider-three row text-center px-4">

            <?php

            $query_top_product = "Select * from food_items where top_product = 'yes' AND item_type = 'product'";
            $run_top_product = mysqli_query($con, $query_top_product);

            while($row_top_product=mysqli_fetch_array($run_top_product)){

              $top_product_id = $row_top_product['item_id'];
              $top_product_img = $row_top_product['item_img'];
              $top_product_name = $row_top_product['item_title'];
              $top_product_price = $row_top_product['item_price'];

              echo "<div class='col-md-2 product pt-md-5'>
              <img src='./admin_area/fooditem_images/$top_product_img' class='img-fluid' alt='Image 2' />
              <div class='cart-details'>
                <h6 class='pro-title p-0'>$top_product_name</h6>
                <div class='pro-price py-2'>
                  <h5>
                  
                    <span>RM $top_product_price</span>
                  </h5>
                </div>
                <div class='cart mt-4'>

                  <a style='text-decoration:none;' href='singleitems.php?itemid=$top_product_id' type='button' class='border site-btn btn-span'>PURCHASE</a>
                </div>
              </div>
            </div>";
            }
            ?>
          </div>
          
          <div class="slider-btn">
            <span class="prev position-top"
              ><i class="fas fa-chevron-left fa-2x text-secondary"></i
            ></span>
            <span class="next position-top right-0"
              ><i class="fas fa-chevron-right fa-2x text-secondary"></i
            ></span>
          </div>
        </div>
      </div>
      <!-- /Features third Slider -->
      <!-- /Features Section -->

      <hr class="hr"></hr>

          <!-- GogoEmpire Mall heading Section -->
      <div class="container text-center">
        <div class="features">
          <h1>GogoEmpire Mall</h1>
        </div>
      </div>

      
      <!-- Mall Section -->
        <div class="container my-5">
            <div class="row">
                <?php
                
                $query = "Select * from food_category where cat_type ='gogo'";
                $run_query = mysqli_query($con, $query);
                while($fetch_query = mysqli_fetch_array($run_query)){

                $category_id = $fetch_query['fcat_id'];
                $category_name = $fetch_query['food_cat'];
                $category_image = $fetch_query['cat_image'];
                

                if($category_name == 'KITCHEN'){

                  $getimage = "Select * from food_category where food_cat = 'KITCHEN'";
                  $runimage = mysqli_query($con,$getimage);
                  $fetchimage = mysqli_fetch_array($runimage);

                  $imageid = $fetchimage['fcat_id'];
                  $categoryimage = $fetchimage['cat_image'];
                  $categoryname = $fetchimage['food_cat'];
             

                 

                  echo " 
                  <div class='col-md-8 col-12'>
                    <form action='displayproductsbycat.php' method='post'>
                      <input type='hidden' name='category_id' value='$imageid'>
                      <button style='border:none;' type='submit' name='viewallproducts_btn'>
                      <img src='./admin_area/homepage_images/$categoryimage' class='img-fluid' alt='$categoryname'>
                      </button>
                    </form>
                  </div>";




                }

                if($category_name == 'HANDMADE'){

                  $getimage = "Select * from food_category where food_cat = 'HANDMADE'";
                  $runimage = mysqli_query($con,$getimage);
                  $fetchimage = mysqli_fetch_array($runimage);

                  $imageid = $fetchimage['fcat_id'];
                  $categoryimage = $fetchimage['cat_image'];
                  $categoryname = $fetchimage['food_cat'];

                 echo " 
                    <div class='col-md-4 col-12'>
                      <form action='displayproductsbycat.php' method='post'>
                        <input type='hidden' name='category_id' value='$imageid'>
                        <button style='border:none;' type='submit' name='viewallproducts_btn'>
                        <img src='./admin_area/homepage_images/$categoryimage' class='img-fluid' alt='$categoryname'>
                        </button>
                      </form>
                    </div>";


                }

              
                }
                ?>
              
            </div>

            <div class="row my-md-3">

                <?php

                  $query = "Select * from food_category where cat_type ='gogo'";
                  $run_query = mysqli_query($con, $query);
                  while($fetch_query = mysqli_fetch_array($run_query)){

                  $category_id = $fetch_query['fcat_id'];
                  $category_name = $fetch_query['food_cat'];
                  $category_image = $fetch_query['cat_image'];

                  if($category_name == 'WATCHES'){

                    $getimage = "Select * from food_category where food_cat = 'WATCHES'";
                    $runimage = mysqli_query($con,$getimage);
                    $fetchimage = mysqli_fetch_array($runimage);
  
                    $imageid = $fetchimage['fcat_id'];
                    $categoryimage = $fetchimage['cat_image'];
                    $categoryname = $fetchimage['food_cat'];

                    echo " 
                    <div class='col-md-4 col-12'>
                      <form action='displayproductsbycat.php' method='post'>
                        <input type='hidden' name='category_id' value='$imageid'>
                        <button style='border:none;' type='submit' name='viewallproducts_btn'>
                        <img src='./admin_area/homepage_images/$categoryimage' class='img-fluid' alt='$categoryname'>
                        </button>
                      </form>
                    </div>";
                  }

                  if($category_name == 'LUXURY SOFA'){

                    $getimage = "Select * from food_category where food_cat = 'LUXURY SOFA'";
                    $runimage = mysqli_query($con,$getimage);
                    $fetchimage = mysqli_fetch_array($runimage);
  
                    $imageid = $fetchimage['fcat_id'];
                    $categoryimage = $fetchimage['cat_image'];
                    $categoryname = $fetchimage['food_cat'];

                    echo " 
                    <div class='col-md-8 col-12'>
                      <form action='displayproductsbycat.php' method='post'>
                        <input type='hidden' name='category_id' value='$imageid'>
                        <button style='border:none;' type='submit' name='viewallproducts_btn'>
                        <img src='./admin_area/homepage_images/$categoryimage' class='img-fluid' alt='$categoryname'>
                        </button>
                      </form>
                    </div>";
  
                  }
                  }



                ?>
            </div>
        </div>
      <!-- /Mall Section -->

      <!-- New, dailydiscover and TopTreanding -->
        <div class="container">
          <div class="newseller">
            <div class="row">

             
              
              <div class="col-md-4 col-sm-6">
                <h3 class="text-secondary">New Arrivals</h3>
                <?php 

                  $trending_query = "Select * from food_items where trending = 'New Arrival'";
                  $trending_run_query = mysqli_query($con, $trending_query);

                  while($trending_fetch_query = mysqli_fetch_array($trending_run_query)){

                    $trending_item_id = $trending_fetch_query['item_id'];
                    $trending_item_cat = $trending_fetch_query['fcat_id'];
                    $trending_item_company = $trending_fetch_query['fcompany_id'];
                    $trending_item_title = $trending_fetch_query['item_title'];
                    $trending_item_price = $trending_fetch_query['item_price'];
                    $trending_item_img = $trending_fetch_query['item_img'];
                    $trending_item_desc = $trending_fetch_query['item_desc'];
                    $trending_item_discounted_price = $trending_fetch_query['item_discounted_price'];
                    $trending_item_availability = $trending_fetch_query['item_avalability'];
                    $trending_item_youtubelink = $trending_fetch_query['youtube_link'];

                    echo "<div class='row py-3'>
                    <div class='col-md-3 p-0'>
                      <div class='items border'>
                        <a href = 'singleitems.php?itemid=$trending_item_id' ><img src='./admin_area/fooditem_images/$trending_item_img' alt='$trending_item_title' class='img-fluid'></a>
                      </div>
                    </div>
                    <div class='col-md-9 p-0 py-4 py-md-0'>
                      <div class='px-4'>
                        <h6>$trending_item_title</h6>
                        <div class='rating pb-2'>
                          <i class='fas fa-star'></i>
                          <i class='fas fa-star'></i>
                          <i class='fas fa-star'></i>
                          <i class='fas fa-star'></i>
                          <i class='fas fa-star-half-alt'></i>
                        </div>
                        <h5>
                          <span class='text-color'>RM$trending_item_price</span>
                        </h5>
                      </div>
                    </div>
                  </div>";

                  }

                ?>  



              </div>
              <div class="col-md-4 col-sm-6">
                <h3 class="text-secondary">Daily Discovers</h3>

                <?php 

                  $trending_query = "Select * from food_items where trending = 'Daily Discover'";
                  $trending_run_query = mysqli_query($con, $trending_query);

                  while($trending_fetch_query = mysqli_fetch_array($trending_run_query)){

                    $trending_item_id = $trending_fetch_query['item_id'];
                    $trending_item_cat = $trending_fetch_query['fcat_id'];
                    $trending_item_company = $trending_fetch_query['fcompany_id'];
                    $trending_item_title = $trending_fetch_query['item_title'];
                    $trending_item_price = $trending_fetch_query['item_price'];
                    $trending_item_img = $trending_fetch_query['item_img'];
                    $trending_item_desc = $trending_fetch_query['item_desc'];
                    $trending_item_discounted_price = $trending_fetch_query['item_discounted_price'];
                    $trending_item_availability = $trending_fetch_query['item_avalability'];
                    $trending_item_youtubelink = $trending_fetch_query['youtube_link'];

                    echo "<div class='row py-3'>
                    <div class='col-md-3 p-0'>
                      <div class='items border'>
                      <a href = 'singleitems.php?itemid=$trending_item_id' ><img src='./admin_area/fooditem_images/$trending_item_img' alt='$trending_item_title' class='img-fluid'></a>
                      </div>
                    </div>
                    <div class='col-md-9 p-0 py-4 py-md-0'>
                      <div class='px-4'>
                        <h6>$trending_item_title</h6>
                        <div class='rating pb-2'>
                          <i class='fas fa-star'></i>
                          <i class='fas fa-star'></i>
                          <i class='fas fa-star'></i>
                          <i class='fas fa-star'></i>
                          <i class='fas fa-star-half-alt'></i>
                        </div>
                        <h5>
                          <span class='text-color'>RM$trending_item_price</span>
                        </h5>
                      </div>
                    </div>
                  </div>";

                  }

                  ?> 
               
              </div>



              <div class="col-md-4 col-sm-6">
                <h3 class="text-secondary">Trending Searches</h3>

                <?php 

                  $trending_query = "Select * from food_items where trending = 'Trending Search'";
                  $trending_run_query = mysqli_query($con, $trending_query);

                  while($trending_fetch_query = mysqli_fetch_array($trending_run_query)){

                    $trending_item_id = $trending_fetch_query['item_id'];
                    $trending_item_cat = $trending_fetch_query['fcat_id'];
                    $trending_item_company = $trending_fetch_query['fcompany_id'];
                    $trending_item_title = $trending_fetch_query['item_title'];
                    $trending_item_price = $trending_fetch_query['item_price'];
                    $trending_item_img = $trending_fetch_query['item_img'];
                    $trending_item_desc = $trending_fetch_query['item_desc'];
                    $trending_item_discounted_price = $trending_fetch_query['item_discounted_price'];
                    $trending_item_availability = $trending_fetch_query['item_avalability'];
                    $trending_item_youtubelink = $trending_fetch_query['youtube_link'];

                    echo "<div class='row py-3'>
                    <div class='col-md-3 p-0'>
                      <div class='items border'>
                      <a href = 'singleitems.php?itemid=$trending_item_id' ><img src='./admin_area/fooditem_images/$trending_item_img' alt='$trending_item_title' class='img-fluid'></a>
                      </div>
                    </div>
                    <div class='col-md-9 p-0 py-4 py-md-0'>
                      <div class='px-4'>
                        <h6>$trending_item_title</h6>
                        <div class='rating pb-2'>
                          <i class='fas fa-star'></i>
                          <i class='fas fa-star'></i>
                          <i class='fas fa-star'></i>
                          <i class='fas fa-star'></i>
                          <i class='fas fa-star-half-alt'></i>
                        </div>
                        <h5>
                          <span class='text-color'>RM$trending_item_price</span>
                        </h5>
                      </div>
                    </div>
                  </div>";

                  }

                  ?> 
              
                  
                 
              </div>
            </div>
          </div>
        </div>

      <!-- /New, dailydiscover and TopTreanding -->


      

      <!-- Facilities -->
        <div class="container-fluid p-0">
          <div class="site-info">
            <div class="row text-center py-3 bg-primary-color m-0">
              <div class="col-md-4 col-sm-12 my-md-0 my-4">
                <div class="row justify-content-center text-light">
                  <i class="fas fa-rocket fa-4x px-4"></i>
                  <div class="py-2 font-roboto text-left">
                    <h6 class="m-0">Free Shipping & Return</h6>
                    <small>Free Shipping on order over RM49</small>
                  </div>
                </div>
              </div>
              <div class="col-md-4 col-sm-12 my-md-0 my-4">
                  <div class="row justify-content-center text-light">
                      <i class="fas fa-hand-holding-usd fa-4x px-4"></i>
                      <div class="py-2 font-roboto text-left">
                        <h6 class="m-0">Money Guarantee</h6>
                        <small>30 days money back guarantee</small>
                      </div>
                    </div>
              </div>
              <div class="col-md-4 col-sm-12 my-md-0 my-4">
                  <div class="row justify-content-center text-light">
                      <i class="fas fa-headphones-alt fa-4x px-4"></i>
                      <div class="py-2 font-roboto text-left">
                        <h6 class="m-0">Online Support</h6>
                        <small>We support online 24 hours a day</small>
                      </div>
                    </div>
              </div>
            </div>
          </div>
        </div>
      <!-- /Facilities -->

      

    </main>

    <!-- /Main Section   -->


    <!-- Footer -->

    <footer>
        <div class="container-fluid px-5">
          <div class="row">
            <div class="col-md-4 col-sm-12">
              <h4>Contact Us</h4>
              <div class="row pl-md-1 text-secondary">
                <div class="col-12">
                  <i class="fa fa-home px-md-2"></i>
                  <small>Swift Commerce Sdn Bhd Lot 23 , Lebuh Sultan Mohamed 1 42000 Klang</small>
                </div>
              </div>
              <div class="row pl-md-1 text-secondary py-4">
                  <div class="col-12">
                      <i class="fa fa-paper-plane px-md-2"></i>
                      <small>askme@swiftcommerce.my </small>
                    </div>
              </div>
               <div class="row pl-md-1 text-secondary">
                  <div class="col-12">
                      <i class="fa fa-phone-volume px-md-2"></i>
                      <small>Tel: 03-3169 6700 / Fax: 03-3176 1271</small>
                    </div>
               </div>

              <div class="row social text-secondary">
                <div class="col-12 py-3">
                  <i class="fab fa-twitter"></i>
                  <i class="fab fa-facebook-f"></i>
                  <i class="fab fa-google-plus-g"></i>
                  <i class="fab fa-skype"></i>
                  <i class="fab fa-pinterest-p"></i>
                  <i class="fab fa-youtube"></i>
                  <i class="fab fa-linkedin-in"></i>
                </div>
              </div>
            </div>
            <div class="col-md-2 col-sm-12">
              <h4>Our Services</h4>
              <div class="d-flex flex-column pl-md-3">
                <small class="pt-0">Kuala Lumpur</small>
                <small>Selangor</small>
                <small>KL Centeral</small>
              </div>
            </div>
            <div class="col-md-2 col-sm-12">
              <h4>Extras</h4>
              <div class="d-flex flex-column pl-md-3">
              <a href="aboutus.php" style="text-decoration:none;"><small>About Us</small></a>
                  <small>Collection</small>
                  <a href="contact.php" style="text-decoration:none;"><small>Contact Us</small></a>
                </div>
            </div>
            <div class="col-md-4 follow-us col-sm-12">
              <h4>Follow Instagram</h4>
              <div class="d-flex flex-row">
                <a href="https://www.instagram.com/the_saujana_hotel_kuala_lumpur/" target="_blank"><img src="./assets/Saujana1.png" alt="Instagram 1" width="128" height="128"></a>
                <a href="https://www.instagram.com/the_saujana_hotel_kuala_lumpur/" target="_blank"><img src="./assets/Saujana2.png" alt="Instagram 2" width="128" height="128"></a>
                <a href="https://www.instagram.com/the_saujana_hotel_kuala_lumpur/" target="_blank"><img src="./assets/Saujana3.png" alt="Instagram 3" width="128" height="128"></a>
              </div>
              <div class="d-flex flex-row">
                  <a href="https://www.instagram.com/the_saujana_hotel_kuala_lumpur/" target="_blank"><img src="./assets/Saujana4.png" alt="Instagram 1" width="128" height="128"></a>
                  <a href="https://www.instagram.com/the_saujana_hotel_kuala_lumpur/" target="_blank"><img src="./assets/Saujana5.png" alt="Instagram 2" width="128" height="128"></a>
                  <a href="https://www.instagram.com/the_saujana_hotel_kuala_lumpur/" target="_blank"><img src="./assets/Saujana6.png" alt="Instagram 3" width="128" height="128"></a>
                </div>
            </div>
          </div>
        </div>

        <div class="container-fluid news pt-5">
          <div class="row">
            <div class="col-md-6 col-12 pl-5">
              <h4 class="primary-color font-roboto m-0 p-0">
                Need Help? Call Our Award-Warning
              </h4>
              <p class="m-0 p-0 text-secondary">
                Support Team 24/7 At (+60) 176844365
              </p>
            </div>
            <!-- <div class="col-md-4 col-12 my-md-0 my-3 pl-md-0 pl-5">
              <input type="text" class="form-control border-0 bg-light" placeholder="Enter Your email Address">
            </div>
            <div class="col-md-2 col-12 my-md-0 my-3 pl-md-0 pl-5">
              <button class="btn bg-primary-color text-white">Subscribe</button>
            </div> -->
          </div>
        </div>

        <div class="container text-center">
          <p class="pt-5">
            <img src="./assets/payment.png" alt="payment image" class="img-fluid">
          </p>
          <small class="text-secondary py-4">GogoEmpire © 2020 All Rights Reserved.</small>
        </div>

    </footer>

    <!-- /Footer -->


    <script
      src="https://code.jquery.com/jquery-3.3.1.slim.min.js"
      integrity="sha384-q8i/X+965DzO0rT7abK41JStQIAqVgRVzpbzo5smXKp4YfRvH+8abtTE1Pi6jizo"
      crossorigin="anonymous"
    ></script>
    <script
      src="https://cdnjs.cloudflare.com/ajax/libs/popper.js/1.14.7/umd/popper.min.js"
      integrity="sha384-UO2eT0CpHqdSJQ6hJty5KVphtPhzWj9WO1clHTMGa3JDZwrnQq4sF86dIHNDz0W1"
      crossorigin="anonymous"
    ></script>
    <script
      src="https://stackpath.bootstrapcdn.com/bootstrap/4.3.1/js/bootstrap.min.js"
      integrity="sha384-JjSmVgyd0p3pXB1rRibZUAYoIIy6OrQ6VrjIEaFf/nJGzIxFDsf4x0xIM+B07jRM"
      crossorigin="anonymous"
    ></script>
    <script
      type="text/javascript"
      src="//cdn.jsdelivr.net/npm/slick-carousel@1.8.1/slick/slick.min.js"
    ></script>
    <script src="./js/main.js"></script>
  </body>
</html>

