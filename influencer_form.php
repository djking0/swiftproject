<?php
session_start();
include("includes/db.php");
include("functions/functions.php");

?>

<!DOCTYPE html>
<html lang="en">

<head>

    <!-- Basic page needs
    ============================================ -->
    <title>Swift Commerce</title>
    <meta charset="utf-8">
    <meta name="keywords"
        content="Gogo Empire Sdn Bhd" />
    <meta name="description"
        content="Gogo Empire Sdn Bhd" />
    <meta name="author" content="Gogo Empire">
    <meta name="robots" content="index, follow" />

    <!-- Mobile specific metas
    ============================================ -->
    <meta name="viewport" content="width=device-width, initial-scale=1.0, maximum-scale=1.0, user-scalable=no">

    <!-- Favicon
    ============================================ -->

    <link rel="shortcut icon" type="image/png" href="ico/favicon-16x16.png" />


    <!-- Libs CSS
    ============================================ -->
    <link rel="stylesheet" href="css/bootstrap/css/bootstrap.min.css">
    <link href="css/font-awesome/css/font-awesome.min.css" rel="stylesheet">
    <link href="js/datetimepicker/bootstrap-datetimepicker.min.css" rel="stylesheet">
    <link href="js/owl-carousel/owl.carousel.css" rel="stylesheet">
    <link href="css/themecss/lib.css" rel="stylesheet">
    <link href="js/jquery-ui/jquery-ui.min.css" rel="stylesheet">
    <link href="js/minicolors/miniColors.css" rel="stylesheet">

    <!-- Theme CSS
    ============================================ -->
    <link href="css/themecss/so_searchpro.css" rel="stylesheet">
    <link href="css/themecss/so_megamenu.css" rel="stylesheet">
    <link href="css/themecss/so-categories.css" rel="stylesheet">
    <link href="css/themecss/so-listing-tabs.css" rel="stylesheet">
    <link href="css/themecss/so-newletter-popup.css" rel="stylesheet">

    <link href="css/footer/footer1.css" rel="stylesheet">
    <link href="css/header/header1.css" rel="stylesheet">
    <link id="color_scheme" href="css/theme.css" rel="stylesheet">
    <link href="css/responsive.css" rel="stylesheet">

    <!-- Google web fonts
    ============================================ -->
    <link href='https://fonts.googleapis.com/css?family=Roboto:300,400,400i,500,500i,700' rel='stylesheet'
        type='text/css'>
    <style type="text/css">
        body {
            font-family: 'Roboto', sans-serif
        }
    </style>

</head>

<body class="res layout-1 layout-subpage">

    <div id="wrapper" class="wrapper-fluid banners-effect-5">


        <!-- Header Container  -->
        <header id="header" class=" typeheader-1">
            <!-- Header Top -->
            <div class="header-top hidden-compact">
                <div class="container">
                    <div class="row">
                        <div class="header-top-left col-lg-7 col-md-8 col-sm-6 col-xs-4">
                            <div class="hidden-sm hidden-xs welcome-msg"><b>Welcome to GogoFood Store !</b> ! Wrap new offers /
                                gift every single day on Weekends </div>
                            <ul class="top-link list-inline hidden-lg hidden-md">
                                <li class="account" id="my_account">
                                    
                                    
                                </li>
                            </ul>
                        </div>
                        <div class="header-top-right collapsed-block col-lg-5 col-md-4 col-sm-6 col-xs-8">
                            <ul class="top-link list-inline lang-curr">
                                <li class="language">
                                    <div class="btn-group languages-block ">
                                        <form action="http://demo.smartaddons.com/templates/html/emarket/index.html"
                                            method="post" enctype="multipart/form-data" id="bt-language">
                                            <a class="btn btn-link dropdown-toggle" data-toggle="dropdown">
                                                <span class="">English</span>
                                                <span class="fa fa-angle-down"></span>
                                            </a>
                                            <ul class="dropdown-menu">
                                                <li><a href="index.html"> English </a></li>
                                                <li> <a href="index.html">  Bhasa Malayu </a> </li>
                                            </ul>
                                        </form>
                                    </div>

                                </li>
                            </ul>



                        </div>
                    </div>
                </div>
            </div>
            <!-- //Header Top -->

            <!-- Header center -->
            <div class="header-middle">
                <div class="container">
                    <div class="row">
                        <!-- Logo -->
                        <div class="navbar-logo col-lg-2 col-md-2 col-sm-12 col-xs-12">
                            <div class="logo"><a href="index.php">
                                <!--<h1 style="color: white;"> GOGO EMPIRE</h1>-->
                                <img src="image/catalog/CBMC-logo-800x132.png" title="Your Store"
                                        alt="Your Store" />
                                </a></div>
                        </div>
                        <!-- //end Logo -->

                        <!-- Main menu -->
                        <div class="main-menu col-lg-6 col-md-7 ">
                            <div class="responsive so-megamenu megamenu-style-dev">
                                <nav class="navbar-default">
                                    <div class=" container-megamenu  horizontal open ">
                                        <div class="navbar-header">
                                            <button type="button" id="show-megamenu" data-toggle="collapse"
                                                class="navbar-toggle">
                                                <span class="icon-bar"></span>
                                                <span class="icon-bar"></span>
                                                <span class="icon-bar"></span>
                                            </button>
                                        </div>

                                        <div class="megamenu-wrapper">
                                            <span id="remove-megamenu" class="fa fa-times"></span>
                                            <div class="megamenu-pattern">
                                                <div class="container-mega">
                                                <ul class="megamenu" 
                                                        data-animationtime="250">
                                                        <li class="home hover">
                                                            <a href="index.php">HOME </a>
                                                        </li>
                                                        <li class="home hover">
                                                        <a href="hotels.php">STORES </a>
                                                        </li>
                                                        <li class="home hover">
                                                            <a href="">COLLECTION </a>
                                                        </li>
                                                        <li class="home hover">
                                                            <a href="aboutus.php">ABOUT US </a>
                                                        </li>
                                                        <li class="home hover">
                                                            <a href="contact.php">CONTACT US </a>
                                                        </li>
                                                    </ul>
                                                </div>
                                            </div>
                                        </div>
                                    </div>
                                </nav>
                            </div>
                        </div>
                        <!-- //end Main menu -->

                        <div class="middle-right col-lg-4 col-md-3 col-sm-6 col-xs-8">
                            <div class="signin-w hidden-sm hidden-xs">
                                <ul class="signin-link blank">
                                
                                </ul>
                            </div>
                            <div class="signin-w hidden-sm hidden-xs">
                                <ul class="signin-link blank">
                               
                                </ul>
                            </div>

                        </div>

                    </div>

                </div>
            </div>
            <!-- //Header center -->

            <!-- Header Bottom -->
            <div class="header-bottom hidden-compact">
                <div class="container">
                    <div class="row">

                        

                       

                       
                       

                    </div>
                </div>

            </div>
        </header>
        <!-- //Header Container  -->


        <!-- Main Container  -->
        <div class="main-container container">
            <ul class="breadcrumb">
                <li><a href="index.php"><i class="fa fa-home"></i></a></li>
                <li><a href="">INFLUENCER</a></li>

            </ul>

            <div class="row">
                <div id="content" class="col-sm-12">
                    <h2 class="title">Register Account</h2>
                    <p>Fill the all below fields</p>
                    <form action="register.php" method="post" enctype="multipart/form-data"
                        class="form-horizontal account-register clearfix">
                        <fieldset id="account">
                            <legend>Your Personal Details</legend>
                            <div class="form-group required" style="display: none;">
                                <label class="col-sm-2 control-label">Customer Group</label>
                                <div class="col-sm-10">
                                    <div class="radio">
                                        <label>
                                            <input type="radio" name="customer_group_id" value="1" checked="checked">
                                            Default
                                        </label>
                                    </div>
                                </div>
                            </div>
                            <div class="form-group required">
                                <label class="col-sm-2 control-label" for="input-firstname">First Name</label>
                                <div class="col-sm-10">
                                    <input type="text" name="c_fname" value="" placeholder="First Name"
                                        id="input-firstname" class="form-control">
                                </div>
                            </div>
                            <div class="form-group required">
                                <label class="col-sm-2 control-label" for="input-lastname">Last Name</label>
                                <div class="col-sm-10">
                                    <input type="text" name="c_lname" value="" placeholder="Last Name"
                                        id="input-lastname" class="form-control">
                                </div>
                            </div>
                            <div class="form-group required">
                                <label class="col-sm-2 control-label" for="input-email">E-Mail</label>
                                <div class="col-sm-10">
                                    <input type="email" name="c_email" value="" placeholder="E-Mail" id="input-email"
                                        class="form-control">
                                </div>
                            </div>
                            <div class="form-group required">
                                <label class="col-sm-2 control-label" for="input-telephone">Telephone</label>
                                <div class="col-sm-10">
                                    <input type="tel" name="c_number" value="" placeholder="Telephone"
                                        id="input-telephone" class="form-control">
                                </div>
                            </div>
                            <div class="form-group required">
                                <label class="col-sm-2 control-label" for="input-gender">Gender</label>
                                <div class=" col-md-10">
                                    <select name="food_availability" class="form-control" required="required" >
                                                <option>Male</option>
                                                <option>FEMALE</option>
                                            </select>
                                </div>
                            </div>
                        </fieldset>
                        <fieldset id="address">
                            <legend>Your Page</legend>
                            <div class="form-group required">
                                <label class="col-sm-2 control-label" for="input-country">Your Page Type</label>
                                <div class="col-sm-10">
                                    <select name="c_country" class="form-control">
                                    <option selected="">Youtube</option>
                                    <option>Tiktok</option>
                                    <option>Facebook</option>
                                    </select>
                                </div>
                            </div>
                            <div class="form-group required">
                                <label class="col-sm-2 control-label" for="input-city">Your Page Name</label>
                                <div class="col-sm-10">
                                    <input type="text" name="c_city" value="" placeholder="Example: BM TECH" id="input-city"
                                        class="form-control">
                                </div>
                            </div>
                            
                            <div class="form-group required">
                                <label class="col-sm-2 control-label" for="input-city">Your Page Link</label>
                                <div class="col-sm-10">
                                    <input type="text" id="origin-input" name="c_city" value="" placeholder="Example: https://www.youtube.com/channel/UCkNADfI4jFhIRqB6S3" id="input-city"
                                        class="form-control">
                                </div>
                            </div>

                            <div class="form-group required">
                                <label class="col-sm-2 control-label" for="input-city">Your Page Followers</label>
                                <div class="col-sm-10">
                                    <input type="text" name="c_city" value="" placeholder="Example: 1 Million in Youtube" id="input-city"
                                        class="form-control">
                                </div>
                            </div>
                        </fieldset>
                        <fieldset id="address">
                            <legend>Your Address</legend>
                            <div class="form-group required">
                                <label class="col-sm-2 control-label" for="input-address-1">Address</label>
                                <div class="col-sm-10">
                                    <input type="text" name="c_address" value="" placeholder="Address"
                                        id="input-address-1" class="form-control">
                                </div>
                            </div>
                            <div class="form-group required">
                                <label class="col-sm-2 control-label" for="input-city">City</label>
                                <div class="col-sm-10">
                                    <input type="text" name="c_city" value="" placeholder="City" id="input-city"
                                        class="form-control">
                                </div>
                            </div>
                            <div class="form-group required">
                                <label class="col-sm-2 control-label" for="input-country">Country</label>
                                <div class="col-sm-10">
                                    <select name="c_country" class="form-control">
                                    <option> Choose...</option>
					                <option>Singapore</option>
					                <option selected="">Malaysia</option>
                                    </select>
                                </div>
                            </div>
                        </fieldset>
                        <fieldset>
                            <legend>Your Password</legend>
                            <div class="form-group required">
                                <label class="col-sm-2 control-label" for="input-password">Password</label>
                                <div class="col-sm-10">
                                    <input type="password" name="c_pass" value="" placeholder="Password"
                                        id="input-password" class="form-control">
                                </div>
                            </div>
                            <div class="form-group required">
                                <label class="col-sm-2 control-label" for="input-confirm">Password Confirm</label>
                                <div class="col-sm-10">
                                    <input type="password" name="confirm" value="" placeholder="Password Confirm"
                                        id="input-confirm" class="form-control">
                                </div>
                            </div>
                        </fieldset>
                        <div class="buttons">
                            
                            <div class="pull-right">I have read and agree to the <a href="#" class="agree"><b>Pricing
                                        Tables</b></a>
                                <input class="box-checkbox" type="checkbox" name="agree" value="1"> &nbsp;
                                <button type="submit" name="register" class="btn btn-primary">Register</button>
                            </div>
                        </div>
                    </form>
                </div>
            </div>
        </div>
        <!-- //Main Container -->


    <!-- Footer Container -->
      <footer class="footer-container typefooter-1">

        <hr>

        <div class="footer-middle ">
            <div class="container">
                <div class="row">
                    <div class="col-lg-3 col-md-3 col-sm-12 col-xs-12 col-style">
                        <div class="infos-footer">
                            <h3 class="modtitle">Contact Us</h3>
                            <ul class="menu">
                                <li class="adres">
                                    Swift Commerce Sdn Bhd Lot 23 , Lebuh Sultan Mohamed 1 42000 Klang
                                </li>
                                <li class="phone">
                                    Tel: 03-3169 6700 / Fax: 03-3176 1271
                                </li>
                                <li class="mail">
                                    askme@swiftcommerce.my 
                                </li>
                                <li class="time">
                                    Open time: 07:00AM - 5:30PM
                                </li>
                            </ul>
                        </div>


                    </div>
                    <div class="col-lg-2 col-md-2 col-sm-4 col-xs-12 col-style">
                        <div class="box-information box-footer">
                            <div class="module clearfix">
                                <h3 class="modtitle">Information</h3>
                                <div class="modcontent">
                                    <ul class="menu">
                                        <li><a href="aboutus.php">ABOUT US </a></li>
                                        <li><a href="#">FAQ</a></li>
                                        <li><a href="#">Warranty And Services</a></li>
                                        <li><a href="#">Support 24/7 page</a></li>
                                        <li><a href="#">Product Registration</a></li>
                                        <li><a href="#">Product Support</a></li>
                                    </ul>
                                </div>
                            </div>
                        </div>

                    </div>
                    <div class="col-lg-2 col-md-2 col-sm-4 col-xs-12 col-style">
                        <div class="box-account box-footer">
                            <div class="module clearfix">
                                <h3 class="modtitle">Extras</h3>
                                <div class="modcontent">
                                    <ul class="menu">
                                        <li> <a href="aboutus.php"> Gogo Empire </a></li>
                                        <li><a href="#">Collection</a></li>
                                        <li><a href="contact.php">Contact Us</a></li>
                                    </ul>
                                </div>
                            </div>
                        </div>


                    </div>
                    <div class="col-lg-2 col-md-2 col-sm-4 col-xs-12 col-style">
                        <div class="box-service box-footer">
                            <div class="module clearfix">
                                <h3 class="modtitle">Services</h3>
                                <div class="modcontent">
                                    <ul class="menu">
                                        <li><a href="#">Kuala Lumpur</a></li>
                                        <li><a href="#">Selangor</a></li>
                                        <li><a href="#">KL Central</a></li>
                                    </ul>
                                </div>
                            </div>
                        </div>

                    </div>
                    <div class="col-lg-3 col-md-3 col-sm-12 col-xs-12 col-style">


                    </div>

                    <div class="col-lg-12 col-xs-12 text-center">
                        <img src="image/catalog/demo/payment/payment.png" alt="imgpayment">
                    </div>
                </div>
            </div>
        </div>

        <!-- Footer Bottom Container -->
        <div class="footer-bottom ">
            <div class="container">
                <div class="copyright">
                    gogoempire © 2020. All Rights Reserved. 
                </div>
            </div>
        </div>
        <!-- /Footer Bottom Container -->


        <!--Back To Top-->
        <div class="back-to-top"><i class="fa fa-angle-up"></i></div>
        </footer>
    <!-- //end Footer Container -->

    </div>

    <!-- Include Libs & Plugins
	============================================ -->
    <!-- Placed at the end of the document so the pages load faster -->
    <script type="text/javascript" src="js/jquery-2.2.4.min.js"></script>
    <script type="text/javascript" src="js/bootstrap.min.js"></script>
    <script type="text/javascript" src="js/owl-carousel/owl.carousel.js"></script>
    <script type="text/javascript" src="js/themejs/libs.js"></script>
    <script type="text/javascript" src="js/unveil/jquery.unveil.js"></script>
    <script type="text/javascript" src="js/countdown/jquery.countdown.min.js"></script>
    <script type="text/javascript" src="js/dcjqaccordion/jquery.dcjqaccordion.2.8.min.js"></script>
    <script type="text/javascript" src="js/datetimepicker/moment.js"></script>
    <script type="text/javascript" src="js/datetimepicker/bootstrap-datetimepicker.min.js"></script>
    <script type="text/javascript" src="js/jquery-ui/jquery-ui.min.js"></script>


    <!-- Theme files
	============================================ -->


    <script type="text/javascript" src="js/themejs/so_megamenu.js"></script>
    <script type="text/javascript" src="js/themejs/addtocart.js"></script>
    <script type="text/javascript" src="js/themejs/application.js"></script>



</body>


</html>


