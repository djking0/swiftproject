<?php
include("includes/db.php");
include("functions/functions.php");
session_start();


?>
<!DOCTYPE html>
<html lang="en">
  <head>
    <meta charset="UTF-8" />
    <meta name="viewport" content="width=device-width, initial-scale=1.0" />
    <meta http-equiv="X-UA-Compatible" content="ie=edge" />
    <title>Gogo Empire Store</title>

    <!-- Bootstrap CDN -->
    <link
      rel="stylesheet"
      href="https://stackpath.bootstrapcdn.com/bootstrap/4.3.1/css/bootstrap.min.css"
      integrity="sha384-ggOyR0iXCbMQv3Xipma34MD+dH/1fQ784/j6cY/iJTQUOhcWr7x9JvoRxT2MZw1T"
      crossorigin="anonymous"
    />

    <!--  Font Awesome CDN -->
    <script src="https://kit.fontawesome.com/23412c6a8d.js"></script>

    <!-- Slick Slider -->
    <link
      rel="stylesheet"
      type="text/css"
      href="//cdn.jsdelivr.net/npm/slick-carousel@1.8.1/slick/slick.css"
    />

    
    <!-- Custom Stylesheet -->
    <link rel="stylesheet" href="./css/style.css" />
    <link rel="stylesheet" href="./css/viewproduct.css" />

  </head>

  <body>
    <!-- header -->

    <header>
      <div class="container">
        <div class="row">
          <div class="col-md-4 col-sm-12 col-12">
            <div class="btn-group">
              <button
                class="btn border dropdown-toggle my-md-4 my-2 text-white"
                data-toggle="dropdown"
                aria-haspopup="true"
                aria-expanded="false"
              >
                ENG
              </button>
              <div class="dropdown-menu">
                <a href="#" class="dropdown-item">Bhasa - Malaysia</a>
              </div>
            </div>
          </div>
          <div class="col-md-4 col-12 text-center">
            <h2 class="my-md-3 site-title text-white">Gogo Empire </h2>
          </div>
          <div class="col-md-4 col-12 text-right">
            <p class="my-md-4 header-links">
              <?php
                if(!isset($_SESSION['customer_email'])){
                echo"";
                }else{
                  
                  echo"<a href='' class='px-1'> $_SESSION[customer_email] </a>";
                  echo"<a class='px-2' style='color:white;'>|</a>";
                }
              ?>
              <?php
                if(!isset($_SESSION['customer_email'])){
                echo"<a href='checkout.php' class='px-2'>login</a>";
                echo"<a class='px-2' style='color:white;'>|</a>";
                echo"<a href='register.php' class='px-2'>Create an Account</a>";
                }else{
                  echo"<a href='logout.php' class='px-2'>logout</a>";
                }
              ?>
            </p>
          </div>
        </div>  
      </div>

      <div class="container-fluid p-0">
        <nav class="navbar navbar-expand-lg navbar-light bg-white">
          <button
            class="navbar-toggler"
            type="button"
            data-toggle="collapse"
            data-target="#navbarNav"
            aria-controls="navbarNav"
            aria-expanded="false"
            aria-label="Toggle navigation"
          >
            <span class="navbar-toggler-icon"></span>
          </button>
          <div class="collapse navbar-collapse" id="navbarNav">
            <ul class="navbar-nav">
              <li class="nav-item active">
                <a href="index.php" class="nav-link" >HOME <span class="sr-only">(current)</span></a>
              </li>
              <li class="nav-item">
                <a href="index.php" class="nav-link" > FEATURES </a>
              </li>
              <li class="nav-item">
                <a href="index.php" class="nav-link" > COLLECTION</a>
              </li>
              <li class="nav-item">
                <a href="index.php" class="nav-link" > SHOP</a>
              </li>
              <li class="nav-item">
                <a href="index.php" class="nav-link" > ABOUT US</a>
              </li>
              <li class="nav-item">
                <a href="cart.php" class="nav-link" > Your Cart <b class="badge badge-pill badge-light float-right"> <?php total_items();?></b></a>
              </li>

              <?php
                        $ip = getIp();

                  ?>

                  <?php
                          cart();
                  ?>
            </ul>
          </div>
          <div class="navbar-nav">
            <form class="form-inline my-2 my-lg-0" method="get" action="results.php" enctype="multipart/form-data">
                  <input class="form-control mr-sm-2" type="search" placeholder="Search a Product" aria-label="Search" name="user_query"/>
                  <button class="btn btn-outline-info my-2 my-sm-0" type="submit">Search</button>
                  </form>
            <!-- <li class="nav-items rounded-circle mx-2 basket-icon">
              <i class="fas fa-shopping-basket p-2"></i>
            </li> -->
          </div>
        </nav>
      </div>

        <!-- BreadCrumbs -->
        <div class="container-fluid p-0">
          <nav class="navbar navbar-expand-lg navbar-light bg-white">
            <ul class="breadcrumbs">
              <li class="breadcrumbs__item">
                <a href="#" class="breadcrumbs__link">Home</a>
              </li>
              <li class="breadcrumbs__item">
                <a href="#" class="breadcrumbs__link breadcrumbs__link--active"> Category <br></a>
              </li>
          </nav>
            </ul>
        </div>
      <!-- /BreadCrumbs -->

    </header>

    <!-- /header -->

    <!-- Main Section   -->

    <main>
      <div class="d-flex justify-content-between flex-wrap flex-md-nowrap align-items-center pt-3 pb-2 mb-3 border-bottom">

        <?php

        if(isset($_GET['cat'])){

          $cat_id = $_GET['cat'];

          $get_cat_data = "select * from categories where cat_id='$cat_id'";

          $run_products = mysqli_query($con, $get_cat_data);

          $row_cat_pro=mysqli_fetch_array($run_products);

          
          $pro_cat_title = $row_cat_pro['cat_title'];


          // echo"<h1> $pro_cat_title</h1>";

          ?>

          <h1 class="h2"><?php echo"$pro_cat_title"?></h1>
          <div class="btn-toolbar mb-2 mb-md-0">
            <div class="btn-group mr-2">
              <button type="button" class="btn btn-sm btn-outline-secondary">Latest</button>
              <button type="button" class="btn btn-sm btn-outline-secondary">Top Sales</button>
            </div>

            <div class="dropdown">
              <button type="button" class="btn btn-sm btn-outline-secondary dropdown-toggle" >
                <span data-feather="calendar"></span>
                Price
              </button>
             </div>

            <!-- --- -->

            <!-- <div class="dropdown">
              <button class="btn btn-sm btn-outline-secondary dropdown-toggle" type="button" id="dropdownMenuButton" data-toggle="dropdown" aria-haspopup="true" aria-expanded="false">
                Price
              </button>
              <div class="dropdown-menu" aria-labelledby="dropdownMenuButton">
                <a class="dropdown-item" href="#">Low to High</a>
                <a class="dropdown-item" href="#">High to Low</a>
              </div>
            </div> -->



            <!-- --- -->



          </div>
          






          <?php
        }
        ?>
      </div>



      <!-- Product -->
      <br>

      <div class="container">
        <div class="row">

            <?php

            if(isset($_GET['cat'])){

                $cat_id = $_GET['cat'];

                $get_cat_pro = "select * from products where cat_id='$cat_id'";

                $run_products = mysqli_query($con, $get_cat_pro);

                while($row_cat_pro=mysqli_fetch_array($run_products)){

                  $pro_id = $row_cat_pro['product_id'];
                  $pro_title = $row_cat_pro['product_title'];
                  $pro_cat = $row_cat_pro['cat_id'];
                  $pro_brand = $row_cat_pro['brand_id'];
                  $pro_desc = $row_cat_pro['product_desc'];
                  $pro_price = $row_cat_pro['product_price'];
                  $pro_imageone = $row_cat_pro['product_img1'];
                  $pro_imagetwo = $row_cat_pro['product_img2'];
                  $pro_discounted_percentage = $row_cat_pro['product_discounted_percentage'];
                  $pro_availability = $row_cat_pro['product_availability'];
                  $pro_youtubelink = $row_cat_pro['product_youtubelink'];
                  ?>

                  <!-- Items -->
                  <div class="col-md-3 col-sm-6">
                      <div class="product-grid">
                          <div class="product-image">
                              <a href="#">
                                  <img class="pic-1" src="./admin_area/product_images/<?php echo"$pro_imageone"?>">
                                  <img class="pic-2" src="./admin_area/product_images/<?php echo"$pro_imagetwo"?>">
                              </a>
                              <span class="product-trend-label"><?php echo"$pro_availability"?></span>
                              <span class="product-discount-label">-<?php echo"$pro_discounted_percentage"?>%</span>
                              <ul class="social">
                                  <li><a href="viewproduct.php?add_cart=<?php echo"$pro_id"?>" data-tip="Add to Cart"><i class="fa fa-shopping-cart"></i></a></li>
                                  <li><a href="<?php echo"$pro_youtubelink"?>" data-tip="Youtube link"><i class="fa fa-youtube-play"></i></a></li>
                                  <li><a href="" data-tip="Compare"><i class="fa fa-random"></i></a></li>
                                  <li><a href="viewproduct.php?pro_id=<?php echo"$pro_id"?>" data-tip="Quick View"><i class="fa fa-search"></i></a></li>
                              </ul>
                          </div>
                          <div class="product-content">
                              <h3 class="title"><a href="#"><?php echo"$pro_title"?></a></h3>
                              <div class="price">RM<?php echo"$pro_price"?></div>
                          </div>
                      </div>
                  </div>
                <!-- /Items -->

                <?php
                  

                }

              }
                
            ?>
        </div>

      </div>
      <!-- /Product -->
      

      <canvas class="my-4 w-100" id="myChart" width="900" height="180"></canvas>

      
    </main>

    <!-- /Main Section   -->

   
    <hr> </hr>
    <!-- Footer -->

    <footer>
        <div class="container-fluid px-5">
          <div class="row">
            <div class="col-md-4 col-sm-12">
              <h4>Contact Us</h4>
              <div class="row pl-md-1 text-secondary">
                <div class="col-12">
                  <i class="fa fa-home px-md-2"></i>
                  <small>S0-7-01, Menara 1, No 3, Jalan Bangsar, KL Eco City</small>
                </div>
              </div>
              <div class="row pl-md-1 text-secondary py-4">
                  <div class="col-12">
                      <i class="fa fa-paper-plane px-md-2"></i>
                      <small>www.gogoempire.com</small>
                    </div>
              </div>
               <div class="row pl-md-1 text-secondary">
                  <div class="col-12">
                      <i class="fa fa-phone-volume px-md-2"></i>
                      <small>(+60) 123817908</small>
                    </div>
               </div>

              <div class="row social text-secondary">
                <div class="col-12 py-3">
                  <i class="fab fa-twitter"></i>
                  <i class="fab fa-facebook-f"></i>
                  <i class="fab fa-google-plus-g"></i>
                  <i class="fab fa-skype"></i>
                  <i class="fab fa-pinterest-p"></i>
                  <i class="fab fa-youtube"></i>
                  <i class="fab fa-linkedin-in"></i>
                </div>
              </div>
            </div>
            <div class="col-md-2 col-sm-12">
              <h4>Our Services</h4>
              <div class="d-flex flex-column pl-md-3">
                <small class="pt-0">Kuala Lumpur</small>
                <small>Selangor</small>
                <small>KL Centeral</small>
              </div>
            </div>
            <div class="col-md-2 col-sm-12">
              <h4>Extras</h4>
              <div class="d-flex flex-column pl-md-3">
                  <small class="pt-0">About GogoEmpire</small>
                  <small>Collection</small>
                  <small>Contact Us</small>
                </div>
            </div>
            <div class="col-md-4 follow-us col-sm-12">
              <h4>Follow Instagram</h4>
              <div class="d-flex flex-row">
                <img src="./assets/256_n.jpg" alt="Instagram 1" class="img-fluid">
                <img src="./assets/792_n.jpg" alt="Instagram 2" class="img-fluid">
                <img src="./assets/392_n.jpg" alt="Instagram 3" class="img-fluid">
              </div>
              <div class="d-flex flex-row">
                  <img src="./assets/664_n.jpg" alt="Instagram 1" class="img-fluid">
                  <img src="./assets/088_n.jpg" alt="Instagram 2" class="img-fluid">
                  <img src="./assets/896_n.jpg" alt="Instagram 3" class="img-fluid">
                </div>
            </div>
          </div>
        </div>

        <div class="container-fluid news pt-5">
          <div class="row">
            <div class="col-md-6 col-12 pl-5">
              <h4 class="primary-color font-roboto m-0 p-0">
                Need Help? Call Our Award-Warning
              </h4>
              <p class="m-0 p-0 text-secondary">
                Support Team 24/7 At (+60) 176844365
              </p>
            </div>
            <div class="col-md-4 col-12 my-md-0 my-3 pl-md-0 pl-5">
              <input type="text" class="form-control border-0 bg-light" placeholder="Enter Your email Address">
            </div>
            <div class="col-md-2 col-12 my-md-0 my-3 pl-md-0 pl-5">
              <button class="btn bg-primary-color text-white">Subscribe</button>
            </div>
          </div>
        </div>

        <div class="container text-center">
          <p class="pt-5">
            <img src="./assets/payment.png" alt="payment image" class="img-fluid">
          </p>
          <small class="text-secondary py-4">GogoEmpire © 2020 All Rights Reserved.</small>
        </div>

    </footer>

    <!-- /Footer -->


    <script
      src="https://code.jquery.com/jquery-3.3.1.slim.min.js"
      integrity="sha384-q8i/X+965DzO0rT7abK41JStQIAqVgRVzpbzo5smXKp4YfRvH+8abtTE1Pi6jizo"
      crossorigin="anonymous"
    ></script>
    <script
      src="https://cdnjs.cloudflare.com/ajax/libs/popper.js/1.14.7/umd/popper.min.js"
      integrity="sha384-UO2eT0CpHqdSJQ6hJty5KVphtPhzWj9WO1clHTMGa3JDZwrnQq4sF86dIHNDz0W1"
      crossorigin="anonymous"
    ></script>
    <script
      src="https://stackpath.bootstrapcdn.com/bootstrap/4.3.1/js/bootstrap.min.js"
      integrity="sha384-JjSmVgyd0p3pXB1rRibZUAYoIIy6OrQ6VrjIEaFf/nJGzIxFDsf4x0xIM+B07jRM"
      crossorigin="anonymous"
    ></script>
    <script
      type="text/javascript"
      src="//cdn.jsdelivr.net/npm/slick-carousel@1.8.1/slick/slick.min.js"
    ></script>
    <script src="./js/main.js"></script>
  </body>



<script src="https://code.jquery.com/jquery-3.4.1.slim.min.js" integrity="sha384-J6qa4849blE2+poT4WnyKhv5vZF5SrPo0iEjwBvKU7imGFAV0wwj1yYfoRSJoZ+n" crossorigin="anonymous"></script>
      <script>window.jQuery || document.write('<script src="/docs/4.4/assets/js/vendor/jquery.slim.min.js"><\/script>')</script><script src="/docs/4.4/dist/js/bootstrap.bundle.min.js" integrity="sha384-6khuMg9gaYr5AxOqhkVIODVIvm9ynTT5J4V1cfthmT+emCG6yVmEZsRHdxlotUnm" crossorigin="anonymous"></script>
        <script src="https://cdnjs.cloudflare.com/ajax/libs/feather-icons/4.9.0/feather.min.js"></script>
        <script src="https://cdnjs.cloudflare.com/ajax/libs/Chart.js/2.7.3/Chart.min.js"></script>
        <script src="dashboard.js"></script></body>
  
</html>